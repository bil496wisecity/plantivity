package com.wisecity.bil496project.util;

public interface ServerCallback {
    void onSuccess(String result);
}
