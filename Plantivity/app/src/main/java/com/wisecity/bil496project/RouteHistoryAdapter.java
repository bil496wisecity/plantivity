package com.wisecity.bil496project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RouteHistoryAdapter extends RecyclerView.Adapter<RouteHistoryAdapter.RouteHistoryViewHolder> {

    private ArrayList<RouteHistoryCardItem> routeHistoryCardItems;

    private OnItemClickListener routeHistoryOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        routeHistoryOnItemClickListener = listener;
    }

    public static class RouteHistoryViewHolder extends RecyclerView.ViewHolder {

        public ImageView iVRouteHistoryCardItem;
        public TextView tVRouteHistoryCardItemName;
        public TextView tVRouteHistoryCardItemDescription;
        public ImageView iVDeleteRouteHistoryCardItem;

        public RouteHistoryViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            iVRouteHistoryCardItem = itemView.findViewById(R.id.iVRouteHistoryCardItem);
            tVRouteHistoryCardItemName = itemView.findViewById(R.id.tVRouteHistoryName);
            tVRouteHistoryCardItemDescription = itemView.findViewById(R.id.tVRouteHistoryDescription);
            iVDeleteRouteHistoryCardItem = itemView.findViewById(R.id.iVDeleteRouteHistoryCardItem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            iVDeleteRouteHistoryCardItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public RouteHistoryAdapter(ArrayList<RouteHistoryCardItem> routeHistoryCardItems) {
        this.routeHistoryCardItems = routeHistoryCardItems;
    }

    @NonNull
    @Override
    public RouteHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.route_history_card_item, parent, false);
        RouteHistoryViewHolder rHVH = new RouteHistoryViewHolder(v, routeHistoryOnItemClickListener);
        return  rHVH;
    }

    @Override
    public void onBindViewHolder(@NonNull RouteHistoryViewHolder holder, int position) {
        // At first item position is 0
        RouteHistoryCardItem  currentRouteHistoryCardItem = routeHistoryCardItems.get(position);

        holder.iVRouteHistoryCardItem.setImageResource(currentRouteHistoryCardItem.getRouteHistoryCardItemImageResource());
        holder.tVRouteHistoryCardItemName.setText(currentRouteHistoryCardItem.getRouteHistoryCardItemName());
        holder.tVRouteHistoryCardItemDescription.setText(currentRouteHistoryCardItem.getRouteHistoryCardItemDescription());

    }

    @Override
    public int getItemCount() {
        return routeHistoryCardItems.size();
    }

}
