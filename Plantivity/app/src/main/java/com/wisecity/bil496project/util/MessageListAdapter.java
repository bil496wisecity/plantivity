package com.wisecity.bil496project.util;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wisecity.bil496project.R;

import java.util.ArrayList;
import java.util.List;

public class MessageListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Message> mMessageList;

    List<Message> messages = new ArrayList<Message>();
    Context context;

    public MessageListAdapter(Context context) {
        this.context = context;
    }

    public void add(Message message) {
        this.messages.add(message);
        notifyDataSetChanged(); // to render the list we need to notify
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    // This is the backbone of the class, it handles the creation of single ListView row (chat bubble)
    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MessageViewHolder holder = new MessageViewHolder();
        LayoutInflater messageInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Message message = messages.get(i);

        if (message.getMessageBelongToCurrentUser()) { // this message was sent by us so let's create a basic chat bubble on the right
            convertView = messageInflater.inflate(R.layout.item_message_sent, null);
            holder.text_message_body = (TextView) convertView.findViewById(R.id.text_message_body);
            holder.text_message_time = (TextView) convertView.findViewById(R.id.text_message_time);
            convertView.setTag(holder);
            String[] splittedMessage = message.getMessage().split("\\s+");
            String mainText = "";
            for(int k = 0; k < splittedMessage.length; k++){
                if(splittedMessage[k].charAt(0) == '@'){
                    mainText += "<font color='#0000ff'>" + splittedMessage[k] + "</font>" + " " ;
                    continue;
                }
                mainText += splittedMessage[k] + " ";
            }
            holder.text_message_body.setText(Html.fromHtml(mainText));
            holder.text_message_time.setText(message.getCreatedAt());
        } else { // this message was sent by someone else so let's create an advanced chat bubble on the left
            convertView = messageInflater.inflate(R.layout.item_message_received, null);
            //holder.image_message_profile = (View) convertView.findViewById(R.id.image_message_profile);
            holder.text_message_name = (TextView) convertView.findViewById(R.id.text_message_name);
            holder.text_message_body = (TextView) convertView.findViewById(R.id.text_message_body);
            holder.text_message_time = (TextView) convertView.findViewById(R.id.text_message_time);
            convertView.setTag(holder);

            String[] splittedMessage = message.getMessage().split("\\s+");
            String mainText = "";
            for(int k = 0; k < splittedMessage.length; k++){
                if(splittedMessage[k].charAt(0) == '@'){
                    mainText += "<font color='#0000ff'>" + splittedMessage[k] + "</font>" + " " ;
                    continue;
                }
                mainText += splittedMessage[k] + " ";
            }

            holder.text_message_name.setText(message.getUserEmail());
            holder.text_message_body.setText(Html.fromHtml(mainText));
            holder.text_message_time.setText(message.getCreatedAt());
            //GradientDrawable drawable = (GradientDrawable) holder.avatar.getBackground();
            //drawable.setColor(Color.parseColor(message.getMemberData().getColor()));
        }

        return convertView;
    }

}

class MessageViewHolder {
    //public View image_message_profile;
    public TextView text_message_name;
    public TextView text_message_body;
    public TextView text_message_time;
}

