package com.wisecity.bil496project;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.wisecity.bil496project.optimal_route.PlantivityScore;
import com.wisecity.bil496project.util.DiscoverPlaces.SwipeCard;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;

import java.util.ArrayList;

public class DiscoverPlacesActivity extends AppCompatActivity {

    public static ArrayList<Place> localChosenPlaces = new ArrayList<>();
    private SwipePlaceHolderView mSwipeView;
    FloatingActionButton fab;
    private Context mContext;
    UserData userData;
    User onlineUser;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_places);

        mSwipeView = findViewById(R.id.swipeView);
        mContext = getApplicationContext();
        localChosenPlaces.clear();
        // Online User provided from login activity via main activity
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");
        // User data provided from main activity
        userData = UserData.getInstance();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        addItemsAndFunctionalityToDrawer();
        userData.getChosenPlaces().clear(); // In every entry to discover places restart the process
        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.swipe_out_msg_view));

        if(userData.getNearByPlaces() != null){
            for(Place place: userData.getNearByPlaces()){
                Log.d("TEST:", place.getName());
                String photoURL = getPhotoURL(place, 500);
                mSwipeView.addView(new SwipeCard(mContext, place, photoURL, mSwipeView, false));
            }
        }

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast fabToast = Toast.makeText(getApplicationContext(), "Chosen Places Has Been Initialized", Toast.LENGTH_SHORT);
                fabToast.show();
                if(localChosenPlaces.size() != 0){
                    userData.setChosenPlaces(localChosenPlaces);
                }
                Toast fabToast2 = Toast.makeText(getApplicationContext(), "Size:" + userData.getChosenPlaces().size(), Toast.LENGTH_SHORT);
                fabToast2.show();
                switchToMainActivity();
//                Snackbar.make(view, "User Activities Has Been Initialized", Snackbar.LENGTH_LONG);
//                        .setAction("Action", null).show();
            }
        });

        findViewById(R.id.rejectBtn).setOnClickListener(v -> mSwipeView.doSwipe(false));
        findViewById(R.id.acceptBtn).setOnClickListener(v -> mSwipeView.doSwipe(true));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    /*
    @Override
    public void onAttachedToWindow() {
        this.openOptionsMenu();
    }
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dicover_places_order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.oRate:
                if(userData.getNearByPlaces().size() == 0) return  false;
                localChosenPlaces.clear();
                userData.getChosenPlaces().clear();
                PlantivityScore.orderByRating(userData.getNearByPlaces());
                mSwipeView.removeAllViews();
                for (Place p:
                     userData.getNearByPlaces()) {
                    String photoURL = getPhotoURL(p, 500);
                    mSwipeView.addView(new SwipeCard(mContext, p, photoURL, mSwipeView, false));
                }
                //Toast.makeText(this, "oRate selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.oCloseness:
                if(userData.getNearByPlaces().size() == 0) return  false;
                localChosenPlaces.clear();
                userData.getChosenPlaces().clear();
                PlantivityScore.orderByDistance(userData.getNearByPlaces(), userData.getCurrentLocation());
                mSwipeView.removeAllViews();
                for (Place p:
                        userData.getNearByPlaces()) {
                    String photoURL = getPhotoURL(p, 500);
                    mSwipeView.addView(new SwipeCard(mContext, p, photoURL, mSwipeView, false));
                }
                //Toast.makeText(this, "oCloseness selected", Toast.LENGTH_SHORT).show();
                return true;
            /*
            case R.id.subitem1:
                Toast.makeText(this, "Sub Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.subitem2:
                Toast.makeText(this, "Sub Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

             */
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public String getPhotoURL(Place place, int maxwidth){
        //TODO temporary
        if(place.getImageReference().size() == 0){
            return "https://source.unsplash.com/NYyCqdBOKwc/600x800";
        }
        StringBuilder photoUrl;
        photoUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/photo?");
        photoUrl.append("maxwidth=" + maxwidth);
        photoUrl.append("&photoreference=" + place.getImageReference().get(0));
        photoUrl.append("&key=" + getString(R.string.google_maps_api));
        Log.d("PHOTOURL", photoUrl.toString());
        return  photoUrl.toString();

    }

    public static void fillChosenPlaces(Place place){
        /*
        if(userData.getChosenPlaces().size() != 0){
            for (Place p:
                 userData.getChosenPlaces()) {
                if(!p.getName().equalsIgnoreCase(place.getName()))
                    continue;
                localChosenPlaces.add(place);
            }
        }
        else localChosenPlaces.add(place);
        */
        localChosenPlaces.add(place);
    }

    private void addItemsAndFunctionalityToDrawer() {

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.material_drawer_dark_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("Berk Utku Yenisey").withEmail("buy@gmail.com").withIcon(getResources().getDrawable(R.drawable.background_profile_routes))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_all_activities);
        PrimaryDrawerItem itemProfileActivity = new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_profile_activity);
        PrimaryDrawerItem itemUserChoicesActivity = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_user_choices_activity);
        PrimaryDrawerItem itemMainActivity = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_main_Activity);
        PrimaryDrawerItem itemPlacePopularityActivity = new PrimaryDrawerItem().withIdentifier(5).withName(R.string.drawer_item_place_populartiy_activity);

        PrimaryDrawerItem itemLogout = new PrimaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_logout);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        itemProfileActivity,
                        itemUserChoicesActivity,
                        itemMainActivity,
                        itemPlacePopularityActivity,

                        new DividerDrawerItem(),
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        switch ((int) drawerItem.getIdentifier()) {
                            case 2:
                                Toast.makeText(getApplicationContext(), "PROFILE ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToProfileActivity();
                                break;
                            case 3:
                                Toast.makeText(getApplicationContext(), "USER CHOICES ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToUserChoicesActivity();
                                break;
                            case 4:
                                Toast.makeText(getApplicationContext(), "MAIN ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToMainActivity();
                                break;
                            case 5:
                                Toast.makeText(getApplicationContext(), "PLACE POPULARITY ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToPlacePopularityActivity();
                                break;
                            case 6:
                                Toast.makeText(getApplicationContext(), "LOGGED OUT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                logout();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true).withAccountHeader(headerResult).build();
    }

    private void switchToProfileActivity() {
        Intent profileActivityIntent = new Intent(getApplicationContext(), ProfileActivity.class);
        profileActivityIntent.putExtra("OnlineUser", onlineUser);
        profileActivityIntent.putExtra("UserData", userData);
        startActivity(profileActivityIntent);
    }

    private void switchToUserChoicesActivity() {
        Intent userChoicesActivityIntent = new Intent(getApplicationContext(), UserChoicesActivity.class);
        userChoicesActivityIntent.putExtra("OnlineUser", onlineUser);
        userChoicesActivityIntent.putExtra("UserData", userData);
        startActivity(userChoicesActivityIntent);
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        mainActivityIntent.putExtra("UserData", userData);
        startActivity(mainActivityIntent);
    }

    private void switchToPlacePopularityActivity() {
        Intent placePopularityActivityIntent = new Intent(getApplicationContext(), PlacePopularityActivity.class);
        placePopularityActivityIntent.putExtra("OnlineUser", onlineUser);
        placePopularityActivityIntent.putExtra("UserData", userData);
        startActivity(placePopularityActivityIntent);
    }

    private void logout() {
        // TO DO Switch To Login Activity and logout
    }
}
