package com.wisecity.bil496project.util;

import android.util.Log;

import com.wisecity.bil496project.optimal_route.PlantivityRanker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class DownloadUrl
{
    public List<Place> getAllNearbyPlaces(String apiCallUrl, List<UserInterest> interestedTypes) throws Exception{

        JSONObject dataJson;
        String dataJsonString;
        String nextPageToken = null;
        String urlExtended = apiCallUrl;
        List<Place> allNearbyPlaces = new ArrayList<>();
        List<Place> nearbyPlaces;
        JSONArray jsonArray;

        PlantivityRanker plantivityRanker = PlantivityRanker.getInstance();

        for (UserInterest userInterest : interestedTypes) {
                urlExtended = apiCallUrl + "&type=" + userInterest.type.name();
                Log.d("url", urlExtended);
                dataJsonString = urlToJsonString(urlExtended);
                dataJson = stringToJson(dataJsonString);
                jsonArray = dataJson.getJSONArray("results");
                nearbyPlaces = jsonToPlaces(jsonArray, userInterest.type.name());
                allNearbyPlaces = appendLists(allNearbyPlaces, nearbyPlaces);
                plantivityRanker.addSubRanker(userInterest, nearbyPlaces);
            }

        return allNearbyPlaces;
    }


    private List<Place> appendLists(List<Place> allNearbyPlaces, List<Place> nearbyPlaces){
        for (int i = 0; i < nearbyPlaces.size(); i++) {
            allNearbyPlaces.add(nearbyPlaces.get(i));
        }
        return allNearbyPlaces;
    }

    private JSONObject stringToJson(String jsonData){
        JSONObject jsonObject = null;

        try
        {
            jsonObject = new JSONObject(jsonData);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return jsonObject;
    }


    public String urlToJsonString(String apiCallUrl) throws IOException {
        String Data = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;

        try
        {
            URL url = new URL(apiCallUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer stringBuffer = new StringBuffer();

            String line = "";

            while ( (line = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(line);
            }

            Data = stringBuffer.toString();
            bufferedReader.close();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            inputStream.close();
            httpURLConnection.disconnect();
        }


        return Data;
    }





    private Place fillPlaceObject(JSONObject googlePlaceJSON)
    {
        Place place = new Place();

        try
        {
            if (!googlePlaceJSON.isNull("place_id")) place.setPlaceId(googlePlaceJSON.getString("place_id"));
            if (!googlePlaceJSON.isNull("name")) place.setName(googlePlaceJSON.getString("name"));
            if (!googlePlaceJSON.isNull("vicinity")) place.setVicinity(googlePlaceJSON.getString("vicinity"));
            if (!googlePlaceJSON.isNull("rating")) place.setRating(googlePlaceJSON.getDouble("rating")); else place.setRating(0);
            place.setLatitude(googlePlaceJSON.getJSONObject("geometry").getJSONObject("location").getString("lat"));
            place.setLongitude(googlePlaceJSON.getJSONObject("geometry").getJSONObject("location").getString("lng"));
            place.setReference(googlePlaceJSON.getString("reference"));

            if (!googlePlaceJSON.isNull("price_level")) place.setPriceLevel(googlePlaceJSON.getInt("price_level"));

            // getting types
            if (!googlePlaceJSON.isNull("types")){
                JSONArray typesJson = googlePlaceJSON.getJSONArray("types");
                ArrayList<String> typesString = new ArrayList<>();
                for(int j = 0; j < typesJson.length(); j++){
                    typesString.add(typesJson.getString(j));
                }
                place.setGroups(typesString);
            }


            if (!googlePlaceJSON.isNull("user_ratings_total")) place.setUserRatingsTotal(googlePlaceJSON.getInt("user_ratings_total"));
            if (!googlePlaceJSON.isNull("plus_code"))
                if(! googlePlaceJSON.getJSONObject("plus_code").isNull("compound_code"))
                    place.setCompoundCode(googlePlaceJSON.getJSONObject("plus_code").getString("compound_code"));

            if (!googlePlaceJSON.isNull("photos")) {
                JSONArray photos = googlePlaceJSON.getJSONArray("photos");
                for (int i = 0; i < photos.length(); i++) {
                    JSONObject photoInfo = photos.getJSONObject(i);
                    place.addImgReference(photoInfo.getString("photo_reference"));
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return place;
    }



    private List<Place> jsonToPlaces(JSONArray jsonArray, String type)
    {
        int counter = jsonArray.length();
        List<Place> NearbyPlacesList = new ArrayList<>();

        Place place = null;

        for (int i=0; i<counter; i++)
        {
            try
            {
                place = fillPlaceObject( (JSONObject) jsonArray.get(i) );
                place.setType(type);
                NearbyPlacesList.add(place);

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return NearbyPlacesList;
    }

}