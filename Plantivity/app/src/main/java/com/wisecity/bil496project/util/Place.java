package com.wisecity.bil496project.util;

import org.json.JSONObject;

import com.google.maps.model.TravelMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Place implements Serializable, Comparable<Place> {
    private String placeId; //TODO Unnecessary
    private String name;
    private String vicinity;
    private double distance;
    private String latitude;
    private String longitude;
    private String reference;
    private String type;
    private ArrayList<String> imgReferences;
    private double rating;
    private int currentPopularity;
    private ArrayList<JSONObject> popularHours;
    private String routeId;
    private Date arrivalTime;
    private TravelMode travelMode;

    private ArrayList<UserInterest.Group> groups = new ArrayList<>();
    private int priceLevel;
    private int userRatingsTotal;
    private String compoundCode;
    private boolean isBlank; // if its true, it will be predicted.

    public Place(){
        this.placeId = "-NA-";
        this.name = "-NA-";
        this.vicinity = "-NA-";
        this.latitude = "";
        this.longitude = "";
        this.reference = "";
        this.type = "";
        this.travelMode = TravelMode.DRIVING;
        this.imgReferences = new ArrayList<>();
        this.distance = 0.0;
        this.rating = (int)(Math.random() * 5) + 1;
        //this.rating = 0.0;
        this.routeId = "";
        this.currentPopularity = -1; // Impossible to be negative, It is negative for Null Check Reasons (At PlacePopularityActivity.java).
        this.popularHours = new ArrayList<>();

        groups = new ArrayList<>();
        priceLevel = 0;
        userRatingsTotal = 0;
        compoundCode = "";
        isBlank = true;
    }


    public void setPlaceId(String placeId){ this.placeId = placeId;}
    public void setName(String name){ this.name = name;}
    public void setVicinity(String vicinity){ this.vicinity = vicinity;}
    public void setLatitude(String latitude){ this.latitude = latitude;}
    public void setLongitude(String longtitude){ this.longitude = longtitude;}
    public void setReference(String reference){ this.reference = reference;}
    public void addImgReference(String imgReference){ this.imgReferences.add(imgReference);}
    public void setType(String type) { this.type = type;}
    public void setRating(double score){ this.rating = rating;}
    public void setTravelMode(TravelMode travelMode) { this.travelMode = travelMode; }
    public void setArrivalTime(Date arrivalTime) { this.arrivalTime = arrivalTime; }
    public void setDistance(double distance){ this.distance = distance;}
    public void setCurrentPopularity(int currentPopularity) { this.currentPopularity = currentPopularity; }
    public void setPopularHours(ArrayList popularHours){ this.popularHours = popularHours;}
    public void setRouteId(String routeId){ this.routeId = routeId;}
    public void setIsBlank(boolean isBlank){this.isBlank = isBlank;}

    public void setGroups(ArrayList<String> types){
        this.groups = new ArrayList<>();
        for(int i = 0; i < types.size(); i++){
            try {
                UserInterest.Type type = UserInterest.Type.valueOf(types.get(i));
                UserInterest.Group group = type.getGroup();
                this.groups.add(group); // does it check if item is unique in list?
            } catch (IllegalArgumentException e) {
                //e.printStackTrace(); // java.lang.IllegalArgumentException: No enum constant
            }
        }
    }
    public void setPriceLevel(int priceLevel){this.priceLevel = priceLevel;}
    public void setUserRatingsTotal(int userRatingsTotal){this.userRatingsTotal = userRatingsTotal;}
    public void setCompoundCode(String compoundCode){this.compoundCode = compoundCode;}

    public String getPlaceId(){ return this.placeId;}
    public String getName(){ return this.name;}
    public String getVicinity(){ return this.vicinity;}
    public String getLatitude(){ return this.latitude;}
    public String getLongitude(){ return this.longitude;}
    public String getReference(){ return this.reference;}
    public String getType() { return this.type; }
    public ArrayList<String> getImageReference(){ return this.imgReferences;}
    public double getRating(){ return this.rating;}
    public double getDistance(){ return this.distance;}
    public int getCurrentPopularity() { return currentPopularity; }
    public ArrayList<JSONObject> getPopularHours(){ return popularHours;}
    public String getRouteId(){ return this.routeId;}
    public TravelMode getTravelMode() { return travelMode; }
    public Date getArrivalTime() { return arrivalTime; }

    public ArrayList<UserInterest.Group> getGroups(){return this.groups;}
    public int getPriceLevel(){return this.priceLevel;}
    public int getUserRatingsTotal(){return this.userRatingsTotal;}
    public String getCompoundCode(){return this.compoundCode;}
    public boolean getIsBlank(){return this.isBlank;}


    public boolean equals(Place place) {
        return placeId.equals(place.placeId);
    }

    @Override
    public int compareTo(Place place) {
        return Double.compare(rating, place.rating);
    }
}
