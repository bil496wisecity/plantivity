package com.wisecity.bil496project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.Route;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class FollowingsProfileActivity extends AppCompatActivity {

    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Toolbar toolbar;

    // a static variable to get a reference of our application context
    public static Context contextOfApplication;
    public static Context getContextOfApplication()
    {
        return contextOfApplication;
    }

    private ArrayList<String> routesNamesList;
    private ArrayList<Route> routesList;
    User following;
    User onlineUser;
    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followings_profile);

        routesList = new ArrayList<>();
        // Online user provided from the login activity
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");

        // Follower user provided from the login activity
        following = (User) getIntent().getSerializableExtra("Following");

        // User data provided from main the activity
        userData = (UserData) getIntent().getSerializableExtra("UserData");
        if(userData.getUserInterests() != null) {
            userData.getUserInterests();
            Toast fabToast = Toast.makeText(getApplicationContext(), "User Activities Has Been Initialized", Toast.LENGTH_SHORT);
            fabToast.show();
        }

        WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                for (User user : listOfAllRegisteredUsers) {
                    if ((user.getEmail().equals(following.getEmail()))) {
                        initializeFollowingUser(user.getEmail(), user.getPassword(), user.getFollowers(), user.getFollowings());
                        extractRouteIdsOfFollowingUser();
                    } else {
                        //System.out.println(listOfAllRegisteredUsers.size()); // For debug purposes
                    }
                }
            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void addItemsAndFunctionalityToDrawer() {

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.material_drawer_dark_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("Berk Utku Yenisey").withEmail("buy@gmail.com").withIcon(getResources().getDrawable(R.drawable.background_profile_routes))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_all_activities);
        SecondaryDrawerItem itemMainActivity = new SecondaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_main_Activity);

        PrimaryDrawerItem itemAddFriends = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_follow_users);

        PrimaryDrawerItem itemLogout = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_logout);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        itemMainActivity,
                        new DividerDrawerItem(),
                        itemAddFriends,
                        new DividerDrawerItem(),
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        switch ((int) drawerItem.getIdentifier()) {
                            case 2:
                                Toast.makeText(getApplicationContext(), "MAIN ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToMainActivity();
                                break;
                            case 3:
                                followUser();
                                Toast.makeText(getApplicationContext(), "USER FOLLOWED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                break;
                            case 4:
                                Toast.makeText(getApplicationContext(), "LOGGED OUT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                logout();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true).withAccountHeader(headerResult).build();
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        mainActivityIntent.putExtra("UserData", userData);
        startActivity(mainActivityIntent);
    }

    private void followUser() {
        openFollowUserDialog();
        // TO DO Add Friend
    }

    private void openFollowUserDialog() {
        FollowUserDialog followUserDialog = new FollowUserDialog(onlineUser, getApplicationContext());
        followUserDialog.show(getSupportFragmentManager(), "Follow User Dialog");
    }

    private void logout() {
        // TO DO Switch To Login Activity and logout
    }

    private void initializeFollowingUser(String emailOfFollowingUser, String passwordOfFollowingUser, ArrayList<String> followersOfFollowingUser, ArrayList<String>followingsOfFollowingUser) {

        following.setEmail(emailOfFollowingUser);
        following.setPassword(passwordOfFollowingUser);


        for(String followerOfFollowing : followersOfFollowingUser) {
            following.addFollower(followerOfFollowing);
        }

        for(String followingOfFollowing : followingsOfFollowingUser) {
            following.addFollowing(followingOfFollowing);
        }
    }

    private void extractRouteIdsOfFollowingUser() {
        WebServerManager.getAllRoutes(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<Route> listOfAllRoutes = new ArrayList<>(JsonOperations.JsontoAllRoutes(result));

                for (Route route : listOfAllRoutes) {

                    if (following.getEmail().equals(route.getUserEmail())) {
                        following.addRouteId(route.getID());
                    }
                }


                addItemsAndFunctionalityToDrawer();

                contextOfApplication = getApplicationContext();

                toolbar = findViewById(R.id.followingsProfileToolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().setTitle(getString(R.string.app_name));
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                // Tabbed Activity
                tabLayout = findViewById(R.id.followingsProfileTabLayout);
                viewPager = findViewById(R.id.followingsProfileViewPager);
                viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                routesNamesList = new ArrayList<>(following.getRouteIds());
                WebServerManager.getAllRoutes(getApplicationContext(), new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        ArrayList<Route> listOfAllRoutes = new ArrayList<>(JsonOperations.JsontoAllRoutes(result));
                        for (String routeId : routesNamesList) {

                            for (Route route : listOfAllRoutes) {

                                if (routeId.equals(route.getID())) {
                                    routesList.add(route);
                                }
                            }
                        }

                        // viewPagerAdapter
                        viewPagerAdapter.addFragment(new RouteHistory(onlineUser, routesList, true, following), "");
//                      viewPagerAdapter.addFragment(new UserInfo(), "");
//                      viewPagerAdapter.addFragment(new Followers(), "");
                        viewPager.setAdapter(viewPagerAdapter);
                        tabLayout.setupWithViewPager(viewPager);

                        tabLayout.getTabAt(0).setIcon(R.drawable.ic_map_black_24dp);
//                      tabLayout.getTabAt(1).setIcon(R.drawable.ic_person_black_24dp);
//                      tabLayout.getTabAt(2).setIcon(R.drawable.ic_people_black_24dp);
                    }
                }, new ServerCallback() {
                    @Override
                    public void onSuccess(String result) {

                    }
                });


            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }
}

