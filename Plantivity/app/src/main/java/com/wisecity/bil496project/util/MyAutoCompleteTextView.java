package com.wisecity.bil496project.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatTextView;

import com.wisecity.bil496project.ChatActivity;

public class MyAutoCompleteTextView extends AutoCompleteTextView {

    public static  boolean isClickedToAutoReplace = false;

    public MyAutoCompleteTextView(Context context) {
        super(context);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void replaceText(CharSequence text) {
        super.replaceText(text);
        ChatActivity.changeText(text);
    }
}
