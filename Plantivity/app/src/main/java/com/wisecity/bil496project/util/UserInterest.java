package com.wisecity.bil496project.util;

import com.wisecity.bil496project.optimal_route.ScoreConfigurations;

import java.io.Serializable;

public class UserInterest implements Serializable {

    public enum Type {
        amusement_park(ScoreConfigurations.AMUSEMENT_PARK_BASE_SCORE, ScoreConfigurations.AMUSEMENT_PARK_DECREASE_FACTOR, Group.entertainment),
        aquarium(ScoreConfigurations.AQUARIUM_PARK_BASE_SCORE, ScoreConfigurations.AQUARIUM_PARK_DECREASE_FACTOR, Group.entertainment),
        art_gallery(ScoreConfigurations.ART_GALLERY_BASE_SCORE, ScoreConfigurations.ART_GALLERY_DECREASE_FACTOR, Group.culture),
        bakery(ScoreConfigurations.BAKERY_BASE_SCORE, ScoreConfigurations.BAKERY_DECREASE_FACTOR, Group.food),
        bar(ScoreConfigurations.BAR_BASE_SCORE, ScoreConfigurations.BAR_DECREASE_FACTOR, Group.clubs),
        beauty_salon(ScoreConfigurations.BEAUTY_SALON_BASE_SCORE, ScoreConfigurations.BEAUTY_SALON_DECREASE_FACTOR, Group.personal_care),
        book_store(ScoreConfigurations.BOOK_STORE_BASE_SCORE, ScoreConfigurations.BOOK_STORE_DECREASE_FACTOR, Group.single),
        bowling_alley(ScoreConfigurations.BOWLING_ALLEY_BASE_SCORE, ScoreConfigurations.BOWLING_ALLEY_DECREASE_FACTOR, Group.entertainment),
        cafe(ScoreConfigurations.CAFE_BASE_SCORE, ScoreConfigurations.CAFE_DECREASE_FACTOR, Group.food),
        campground(ScoreConfigurations.CAMPGROUND_BASE_SCORE, ScoreConfigurations.CAMPGROUND_DECREASE_FACTOR, Group.single),
        casino(ScoreConfigurations.CASINO_BASE_SCORE, ScoreConfigurations.CASINO_DECREASE_FACTOR, Group.clubs),
        church(ScoreConfigurations.CHURCH_BASE_SCORE, ScoreConfigurations.CHURCH_DECREASE_FACTOR, Group.religious_places),
        clothing_store(ScoreConfigurations.CLOTHING_SCORE_BASE_SCORE, ScoreConfigurations.CLOTHING_SCORE_DECREASE_FACTOR, Group.fashion),
        florist(ScoreConfigurations.FLORIST_BASE_SCORE, ScoreConfigurations.FLORIST_DECREASE_FACTOR, Group.single),
        furniture_store(ScoreConfigurations.FURNITURE_STORE_BASE_SCORE, ScoreConfigurations.FURNITURE_STORE_DECREASE_FACTOR, Group.home_store),
        hair_care(ScoreConfigurations.HAIR_CARE_BASE_SCORE, ScoreConfigurations.HAIR_CARE_DECREASE_FACTOR, Group.personal_care),
        hardware_store(ScoreConfigurations.HARDWARE_BASE_SCORE, ScoreConfigurations.HARDWARE_DECREASE_FACTOR, Group.technology_score),
        home_goods_store(ScoreConfigurations.HOME_GOODS_BASE_SCORE, ScoreConfigurations.HOME_GOODS_DECREASE_FACTOR, Group.home_store),
        jewelry_store(ScoreConfigurations.JEWELRY_BASE_SCORE, ScoreConfigurations.JEWELRY_DECREASE_FACTOR, Group.fashion),
        library(ScoreConfigurations.LIBRARY_BASE_SCORE, ScoreConfigurations.LIBRARY_DECREASE_FACTOR, Group.culture),
        liquor_store(ScoreConfigurations.LIQUOR_STORE_BASE_SCORE, ScoreConfigurations.LIQUOR_STORE_DECREASE_FACTOR, Group.single),
        meal_takeaway(ScoreConfigurations.MEAL_TAKEAWAY_BASE_SCORE, ScoreConfigurations.MEAL_TAKEAWAY_DECREASE_FACTOR, Group.food),
        mosque(ScoreConfigurations.MOSQUE_BASE_SCORE, ScoreConfigurations.MOSQUE_DECREASE_FACTOR, Group.religious_places),
        movie_theater(ScoreConfigurations.MOVIE_THEATER_BASE_SCORE, ScoreConfigurations.MOVIE_THEATER_DECREASE_FACTOR, Group.entertainment),
        museum(ScoreConfigurations.MUSEUM_BASE_SCORE, ScoreConfigurations.MUSEUM_DECREASE_FACTOR, Group.culture),
        night_club(ScoreConfigurations.NIGHT_CLUB_BASE_SCORE, ScoreConfigurations.NIGHT_CLUB_DECREASE_FACTOR, Group.clubs),
        park(ScoreConfigurations.PARK_BASE_SCORE, ScoreConfigurations.PARK_DECREASE_FACTOR, Group.nature),
        restaurant(ScoreConfigurations.RESTAURANT_BASE_SCORE, ScoreConfigurations.RESTAURANT_DECREASE_FACTOR, Group.food),
        shoe_store(ScoreConfigurations.SHOE_STORE_BASE_SCORE, ScoreConfigurations.SHOE_STORE_DECREASE_FACTOR, Group.fashion),
        spa(ScoreConfigurations.SPA_BASE_SCORE, ScoreConfigurations.SPA_DECREASE_FACTOR, Group.single),
        synagogue(ScoreConfigurations.SYNAGOGUE_BASE_SCORE, ScoreConfigurations.SYNAGOGUE_DECREASE_FACTOR, Group.religious_places),
        tourist_attraction(ScoreConfigurations.TOURIST_ATTRACTION_BASE_SCORE, ScoreConfigurations.TOURIST_ATTRACTION_DECREASE_FACTOR, Group.single),
        zoo(ScoreConfigurations.ZOO_BASE_SCORE, ScoreConfigurations.ZOO_DECREASE_FACTOR, Group.nature);

        private final double baseScore, importanceDecreaseFactor;
        private final Group group;

        Type(final double baseScore, final double importanceDecreaseFactor, Group group) {
            this.baseScore = baseScore;
            this.importanceDecreaseFactor = importanceDecreaseFactor;
            this.group = group;
        }

        public double baseScore() { return baseScore;}
        public double importanceDecreaseFactor() { return importanceDecreaseFactor;}

        public boolean isInGroup(Group group) {
            return this.group == group;
        }

        public UserInterest.Group getGroup(){return this.group;}

    }

    public Type type;
    public double interestLevel;

    public UserInterest(Type type, double interestLevel) {
        this.type = type;
        this.interestLevel = interestLevel;
    }

    public enum Group {
        entertainment,
        culture,
        clubs,
        food,
        nature,
        personal_care,
        fashion,
        home_store,
        technology_score,
        religious_places,
        single
    }

}
