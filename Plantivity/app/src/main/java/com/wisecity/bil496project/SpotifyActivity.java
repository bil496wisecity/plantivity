package com.wisecity.bil496project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.WebServerManager;


public class SpotifyActivity extends Activity{

    private static final String CLIENT_ID = "ac84cf15405b49fab3ba9addcafd385e";
    private static final String REDIRECT_URI = "http://localhost:8000";
    private static final int REQUEST_CODE = 1337;
    private User onlineUser;
    EditText etSpotifyUsername;
    Button btnLoginSpotify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotify);
        etSpotifyUsername = findViewById(R.id.etSpotifyUsername);
        btnLoginSpotify = findViewById(R.id.btnLoginSpotify);
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");
        if(!onlineUser.getSpotifyUsername().equals(""))
            etSpotifyUsername.setText(onlineUser.getSpotifyUsername());
        btnLoginSpotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newSpotifyUsername =  etSpotifyUsername.getText().toString();
                onlineUser.setSpotifyUsername(newSpotifyUsername);

                AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
                builder.setScopes(new String[]{"user-read-private", "streaming"});
                AuthenticationRequest request = builder.build();

                AuthenticationClient.openLoginActivity(SpotifyActivity.this, REQUEST_CODE, request);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            Toast.makeText(getApplicationContext(), response.getAccessToken(), Toast.LENGTH_LONG).show();
            String spotifyToken = response.getAccessToken();
            String spotifyUsername = onlineUser.getSpotifyUsername();
            String userEmail = onlineUser.getEmail();
            WebServerManager.postSpotifyToken(this, spotifyToken, spotifyUsername, userEmail);


        }
    }


}
