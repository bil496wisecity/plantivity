package com.wisecity.bil496project.util;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.maps.model.TravelMode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonOperations {

    public static List<Route> JsontoRoutes(String result){
        List<Route> routes = new ArrayList<>();
        try {
            JSONArray outerArr = new JSONArray(result);

            for(int i = 0; i < outerArr.length(); i++){
                Route route = new Route();
                JSONObject outerObj = outerArr.getJSONObject(i);
                String userEmail = outerObj.isNull("userEmail")?"":outerObj.optString("userEmail");
                String id = outerObj.getJSONObject("_id").getString("$oid");
                double usersRate = outerObj.isNull("usersRate")?1.0:Double.parseDouble(outerObj.optString("usersRate"));
                route.setUsersRate(usersRate);
                route.setID(id);
                route.setUserEmail(userEmail);
                JSONArray innerArr = outerObj.getJSONArray("places");

                for(int j=0; j < innerArr.length(); j++) {
                    Place place = new Place();
                    JSONObject innerObj = innerArr.getJSONObject(j);
                    String name = innerObj.isNull("name")?"":innerObj.optString("name");
                    String latitude = innerObj.isNull("latitude")?"":innerObj.optString("latitude");
                    String longitude = innerObj.isNull("name")?"":innerObj.optString("longitude");
                    double rating = innerObj.isNull("rating")?0:Double.parseDouble(innerObj.optString("rating"));
                    int travelMode = innerObj.isNull("travelMode")?0:Integer.parseInt(innerObj.optString("travelMode"));
                    place.setName(name);
                    place.setLatitude(latitude);
                    place.setLongitude(longitude);
                    place.setRating(rating);
                    place.setTravelMode(TravelMode.values()[travelMode]);
                    route.addPlace(place);
                    Log.d("NAME:" , name);
                }
                routes.add(route);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  routes;
    }

    public static List<Route> JsontoAllRoutes(String result){
        List<Route> routes = new ArrayList<>();
        try {
            JSONArray outerArr = new JSONArray(result);

            for(int i = 0; i < outerArr.length(); i++){
                Route route = new Route();
                JSONObject outerObj = outerArr.getJSONObject(i);
                String userEmail = outerObj.isNull("userEmail")?"":outerObj.optString("userEmail");
                String id = outerObj.getJSONObject("_id").getString("$oid");
                double usersRate = outerObj.isNull("usersRate")?1.0:Double.parseDouble(outerObj.optString("usersRate"));
                route.setUsersRate(usersRate);
                route.setID(id);
                route.setUserEmail(userEmail);
                JSONArray innerArr = outerObj.getJSONArray("places");

                for(int j=0; j < innerArr.length(); j++) {
                    Place place = new Place();
                    JSONObject innerObj = innerArr.getJSONObject(j);
                    String name = innerObj.isNull("name")?"":innerObj.optString("name");
                    String latitude = innerObj.isNull("latitude")?"":innerObj.optString("latitude");
                    String longitude = innerObj.isNull("name")?"":innerObj.optString("longitude");
                    double rating = innerObj.isNull("rating")?0:Double.parseDouble(innerObj.optString("rating"));
                    int travelMode = innerObj.isNull("travelMode")?0:Integer.parseInt(innerObj.optString("travelMode"));
                    place.setName(name);
                    place.setLatitude(latitude);
                    place.setLongitude(longitude);
                    place.setRating(rating);
                    place.setTravelMode(TravelMode.values()[travelMode]);
                    route.addPlace(place);
                    Log.d("NAME:" , name);
                }
                routes.add(route);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  routes;
    }

    public static List<User> JsontoUsers(String result){
        List<User> users = new ArrayList<>();
        try {
            JSONArray arr = new JSONArray(result);

            for(int j=0; j < arr.length(); j++) {
                User user = new User();
                JSONObject obj = arr.getJSONObject(j);
                String email = obj.isNull("email")?"":obj.optString("email");
                String password = obj.isNull("password")?"":obj.optString("password");
                String deviceId = obj.isNull("deviceId")?"":obj.optString("deviceId");

                JSONArray arrFollowingIds = obj.isNull("followings")? new JSONArray():obj.getJSONArray("followings");
                String[] followingIds = new String[arrFollowingIds.length()];
                for(int i = 0; i < arrFollowingIds.length(); i++){
                    followingIds[i] = arrFollowingIds.getString(i);
                    user.addFollowing(followingIds[i]);
                }

                JSONArray arrFollowerIds = obj.isNull("followers")? new JSONArray():obj.getJSONArray("followers");
                String[] followerIds = new String[arrFollowerIds.length()];
                for(int i = 0; i < arrFollowerIds.length(); i++){
                    followerIds[i] = arrFollowerIds.getString(i);
                    user.addFollower(followerIds[i]);
                }

                user.setEmail(email);
                user.setPassword(password);
                user.setDeviceId(deviceId);

                Log.d("EMAIL:" , email);
                users.add(user);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  users;
    }

    public static User JsontoUser(String result){
        User user = new User();
        try {
            JSONArray arr = new JSONArray(result);
            JSONObject obj = arr.getJSONObject(0);
            String email = obj.isNull("email")?"":obj.optString("email");
            String password = obj.isNull("password")?"":obj.optString("password");
            String deviceId = obj.isNull("deviceId")?"":obj.optString("deviceId");

            JSONArray arrFollowingIds = obj.isNull("followings")? new JSONArray():obj.getJSONArray("followings");
            String[] followingIds = new String[arrFollowingIds.length()];
            for(int i = 0; i < arrFollowingIds.length(); i++){
                followingIds[i] = arrFollowingIds.getString(i);
                user.addFollowing(followingIds[i]);
            }

            JSONArray arrFollowerIds = obj.isNull("followers")? new JSONArray():obj.getJSONArray("followers");
            String[] followerIds = new String[arrFollowerIds.length()];
            for(int i = 0; i < arrFollowerIds.length(); i++){
                followerIds[i] = arrFollowerIds.getString(i);
                user.addFollower(followerIds[i]);
            }

            user.setEmail(email);
            user.setPassword(password);
            user.setDeviceId(deviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  user;
    }
}
