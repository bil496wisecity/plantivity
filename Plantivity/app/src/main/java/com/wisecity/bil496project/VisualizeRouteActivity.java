package com.wisecity.bil496project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.ml.vision.FirebaseVision;

import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.WebServerManager;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VisualizeRouteActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnPolylineClickListener
{

    private final String TAG = "alper";
    private UserData userData;
    private User onlineUser;
    private GoogleMap mMap;
    private GeoApiContext mGeoApiContext = null;
    private FusedLocationProviderClient fusedLocationClient;
    DateFormat dateFormat;
    private Marker userMarker;
    private LatLng currentLatLng;
    private LatLng[] places;
    private Marker[] markers;
    private BitmapDescriptor[] markerIcons;
    private BitmapDescriptor defaultMarkerIcon = new MarkerOptions().getIcon();
    private Polyline[] polylines;
    private List<Place> route;
    private Handler handler = new Handler();
    private Runnable runnable;
    private final int locationUpdateDelay = 100; //One second = 1000 milliseconds.
    private View customMarker;
    private int markerCount = 0;
    private Button chooseImageFromGalleryButton;
    private Button takePictureButton;
    private Uri imageUri;

    @Override
    protected void onResume() {
        handler.postDelayed( runnable = new Runnable() {
            public void run() {
                updateUserLocation();
                handler.postDelayed(runnable, locationUpdateDelay);
            }
        }, locationUpdateDelay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customMarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        // Online User provided from login activity via main activity
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");
        // User data provided from another activity
        userData = UserData.getInstance();
        dateFormat = new SimpleDateFormat("HH:mm");
        FirebaseApp.initializeApp(VisualizeRouteActivity.this);
        setContentView(R.layout.activity_visualize_route);
        addItemsAndFunctionalityToDrawer();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        if (mGeoApiContext == null){
            mGeoApiContext = new GeoApiContext.Builder()
                    .apiKey(getString(R.string.google_maps_key))
                    .build();
        }
        chooseImageFromGalleryButton = findViewById(R.id.chooseImageFromGallery);
        takePictureButton = findViewById(R.id.takePicture);
        chooseImageFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery();
            }
        });
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        route = userData.getRoute();
        markers = new Marker[route.size() + 1];
        markerIcons = new BitmapDescriptor[route.size() + 1];
        places = new LatLng[route.size()];

        if (!userData.getSentFromProfileActivity())
            WebServerManager.postRoute(VisualizeRouteActivity.this, route, 0, "a.yildiz@etu.edu.tr");
        else
            Log.d(TAG, "Route came from profile, no push is made to db. ");

        for (int i = 0; i < route.size(); i++) {
            Place place = route.get(i);
            double lat = Double.parseDouble(place.getLatitude());
            double lng = Double.parseDouble(place.getLongitude());
            places[i] = new LatLng(lat, lng);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnPolylineClickListener(this);
        currentLatLng = new LatLng(userData.getCurrentLocation().latitude, userData.getCurrentLocation().longitude);
        polylines = new Polyline[route.size() + 1];
        Polyline polyline = mMap.addPolyline(new PolylineOptions().add(new LatLng(0, 0)));
        for (int i=0; i<route.size() + 1; i++)
            polylines[i] = polyline;
        polyline.remove();
        userMarker = mMap.addMarker(new MarkerOptions()
                .position(currentLatLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                .title("Siz")
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15));

        for (LatLng latLng: places)
            drawMarker(latLng);
        int routeIndex = 0;
        calculateRoute(routeIndex, new com.google.maps.model.LatLng(userData.getCurrentLocation().latitude, userData.getCurrentLocation().longitude), new com.google.maps.model.LatLng(places[0].latitude, places[0].longitude));
        routeIndex++;
        for (int i=0; i<places.length - 1; i++){
            calculateRoute(routeIndex
                    , new com.google.maps.model.LatLng(places[i].latitude, places[i].longitude)
                    , new com.google.maps.model.LatLng(places[i+1].latitude, places[i+1].longitude));
            routeIndex++;
        }
    }

    public void drawMarker(LatLng latLng){
        TextView placeName = customMarker.findViewById(R.id.placeName);
        TextView placeArrivalTime = customMarker.findViewById(R.id.placeArrivalTime);
        TextView timeSpentText = customMarker.findViewById(R.id.timeSpent);
        ImageView placePhoto = customMarker.findViewById(R.id.placePhoto);
        RatingBar ratingBar = customMarker.findViewById(R.id.ratingBar);

        placeName.setText(route.get(markerCount).getName());
        ratingBar.setRating((float)route.get(markerCount).getRating());

        Date arrivalTime = route.get(markerCount).getArrivalTime();

        if (arrivalTime != null) {
            String estimatedArrival = dateFormat.format(arrivalTime);
            String formattedText = String.format("Tahmini varış: %s", estimatedArrival);
            placeArrivalTime.setText(formattedText);
        } else {
            placeArrivalTime.setVisibility(View.INVISIBLE);
        }

        String[] placeTypeAndTime = getPlaceTypeAndTime(route.get(markerCount).getType());
        String placeType = placeTypeAndTime[0];
        String timeSpent = placeTypeAndTime[1];
        if (timeSpent == null)
            timeSpentText.setText(String.format("Bilinmiyor"));
        else
            timeSpentText.setText(String.format("Ortalama: %s dakika", timeSpent));

        if (placeType != null){
            int placeIconID = getResources().getIdentifier("place_icon_" + placeType, "drawable",  getPackageName());
            if (placeIconID > 0)
                placePhoto.setImageResource(placeIconID);
            else
                placePhoto.setImageResource(getResources().getIdentifier("place_icon", "drawable", getPackageName()));
        }
        else
            placePhoto.setImageResource(getResources().getIdentifier("place_icon", "drawable", getPackageName()));

        markerIcons[markerCount] = BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, customMarker));
        markers[markerCount] = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(++markerCount + ". mekan")
        );
    }

    private void calculateRoute(int routeIndex, com.google.maps.model.LatLng origin, com.google.maps.model.LatLng destination){
        DirectionsApiRequest direction = new DirectionsApiRequest(mGeoApiContext);
        direction.origin(origin);
        direction.alternatives(true);
        direction.language("tr");
        direction.mode(userData.getTravelMode());
        direction.destination(destination)
                .setCallback(new PendingResult.Callback<DirectionsResult>() {
                    @Override
                    public void onResult(DirectionsResult result) {
                        createPolyLine(routeIndex, result);
                    }

                    @Override
                    public void onFailure(Throwable e) {
                        Toast.makeText(VisualizeRouteActivity.this, "Konumunuzdan 1.mekana rota bulunamadı.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void createPolyLine(int routeIndex, final DirectionsResult result){
        new Handler(Looper.getMainLooper()).post(() -> {
            DirectionsRoute route = result.routes[0];
            List<com.google.maps.model.LatLng> decodedPath = PolylineEncoding.decode(route.overviewPolyline.getEncodedPath());
            List<LatLng> newDecodedPath = new ArrayList<>();

            for(com.google.maps.model.LatLng latLng: decodedPath)
                newDecodedPath.add(new LatLng(
                        latLng.lat,
                        latLng.lng
                ));
            Polyline polyline = mMap.addPolyline(new PolylineOptions()
                    .visible(false)
                    .clickable(true)
                    .width(15)
                    .addAll(newDecodedPath));
            polyline.setTag(route.legs[0].duration);
            if (routeIndex == 0)
                polyline.setVisible(true);
            polylines[routeIndex] = polyline;
        });
    }

    private void updateUserLocation(){
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    if (location != null) {
                        if (Math.abs(currentLatLng.latitude - location.getLatitude()) > 0.0005
                                || Math.abs(currentLatLng.longitude - location.getLongitude()) > 0.0005) {
                            currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                            userMarker.remove();
                            userMarker = mMap.addMarker(new MarkerOptions()
                                    .position(currentLatLng)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                                    .title("Siz")
                            );

                            Polyline closestPolyline = null;
                            double min = Double.MAX_VALUE;
                            for (Polyline polyline : polylines) {
                                double averageLat;
                                double averageLng;
                                double sumLat = 0;
                                double sumLng = 0;
                                for (LatLng latLng : polyline.getPoints()) {
                                    sumLat += latLng.latitude;
                                    sumLng += latLng.longitude;
                                }
                                averageLat = sumLat / polyline.getPoints().size();
                                averageLng = sumLng / polyline.getPoints().size();
                                double latDifference = Math.abs(location.getLatitude() - averageLat);
                                double lngDifference = Math.abs(location.getLongitude() - averageLng);
                                if ((latDifference + lngDifference) / 2 < min) {
                                    min = (latDifference + lngDifference) / 2;
                                    closestPolyline = polyline;
                                }
                            }
                            for (Polyline polyline : polylines)
                                polyline.setVisible(false);
                            closestPolyline.setVisible(true);
                        }
                    }

                });
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        Toast.makeText(VisualizeRouteActivity.this, polyline.getTag().toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTitle().equals("Siz")){
            polylines[0].setVisible(true);
            for (int i=1; i<polylines.length; i++)
                polylines[i].setVisible(false);
        }
        else {
            int markerCount = Integer.parseInt(marker.getTitle().substring(0, marker.getTitle().indexOf("."))) - 1;
            for (int i = 0; i < markerIcons.length; i++) {
                if (i == markerCount)
                    marker.setIcon(markerIcons[markerCount]);
                else {
                    if (markers[i] != null)
                        markers[i].setIcon(defaultMarkerIcon);
                }
            }

            for (Polyline polyline : polylines) {
                polyline.setVisible(false);
            }
            polylines[markerCount + 1].setVisible(true);
        }
        return false;
    }

    public String[] getPlaceTypeAndTime(String type){
        String[] placeTypeAndTime = new String[2];
        placeTypeAndTime[0] = null;
        int stringIdentifier = this.getResources().getIdentifier(type, "string", this.getPackageName());
        if (stringIdentifier > 0) {
            placeTypeAndTime[0] = type;
            placeTypeAndTime[1] = this.getString(stringIdentifier);
        }
        return placeTypeAndTime;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            imageUri = data.getData();
        }
        try {
            detectTextFromImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    private void detectTextFromImage() throws IOException {
        FirebaseVisionImage firebaseVisionImage = FirebaseVisionImage.fromFilePath(VisualizeRouteActivity.this, imageUri);
        FirebaseVisionTextRecognizer firebaseVisionTextRecognizer = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
        firebaseVisionTextRecognizer.processImage(firebaseVisionImage).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                Log.d(TAG, "Algilanan yazi: " + firebaseVisionText.getText());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                imageUri = FileProvider.getUriForFile(this,
                        "com.wisecity.bil496project",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(takePictureIntent, 0);
            }
        }
    }

    private void addItemsAndFunctionalityToDrawer() {

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.material_drawer_dark_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("Berk Utku Yenisey").withEmail("buy@gmail.com").withIcon(getResources().getDrawable(R.drawable.background_profile_routes))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_all_activities);
        PrimaryDrawerItem itemProfileActivity = new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_profile_activity);
        PrimaryDrawerItem itemUserChoicesActivity = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_user_choices_activity);
        PrimaryDrawerItem itemDiscoverPlacesActivity = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_discover_places_activity);
        PrimaryDrawerItem itemPlacePopularityActivity = new PrimaryDrawerItem().withIdentifier(5).withName(R.string.drawer_item_place_populartiy_activity);
        PrimaryDrawerItem itemMainActivity= new PrimaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_main_Activity);
        PrimaryDrawerItem itemLogout = new PrimaryDrawerItem().withIdentifier(7).withName(R.string.drawer_item_logout);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        itemProfileActivity,
                        itemUserChoicesActivity,
                        itemDiscoverPlacesActivity,
                        itemPlacePopularityActivity,
                        itemMainActivity,
                        new DividerDrawerItem(),
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        switch ((int) drawerItem.getIdentifier()) {
                            case 2:
                                Toast.makeText(getApplicationContext(), "PROFILE ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToProfileActivity();
                                break;
                            case 3:
                                Toast.makeText(getApplicationContext(), "USER CHOICES ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToUserChoicesActivity();
                                break;
                            case 4:
                                Toast.makeText(getApplicationContext(), "DISCOVER PLACES ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToDiscoverPlacesActivity();
                                break;
                            case 5:
                                Toast.makeText(getApplicationContext(), "PLACE POPULARITY ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToPlacePopularityActivity();
                                break;
                            case 6:
                                Toast.makeText(getApplicationContext(), "MAIN ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToMainActivity();
                                break;
                            case 7:
                                Toast.makeText(getApplicationContext(), "LOGGED OUT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                logout();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true).withAccountHeader(headerResult).build();
    }

    private void switchToProfileActivity() {
        Intent profileActivityIntent = new Intent(getApplicationContext(), ProfileActivity.class);
        profileActivityIntent.putExtra("OnlineUser", onlineUser);
        profileActivityIntent.putExtra("UserData", userData);
        startActivity(profileActivityIntent);
    }

    private void switchToUserChoicesActivity() {
        Intent userChoicesActivityIntent = new Intent(getApplicationContext(), UserChoicesActivity.class);
        userChoicesActivityIntent.putExtra("OnlineUser", onlineUser);
        userChoicesActivityIntent.putExtra("UserData", userData);
        startActivity(userChoicesActivityIntent);
    }

    private void switchToDiscoverPlacesActivity() {
        Intent discoverPlacesActivityIntent = new Intent(getApplicationContext(), DiscoverPlacesActivity.class);
        discoverPlacesActivityIntent.putExtra("OnlineUser", onlineUser);
        discoverPlacesActivityIntent.putExtra("UserData", userData);
        startActivity(discoverPlacesActivityIntent);
    }

    private void switchToPlacePopularityActivity() {
        Intent placePopularityActivityIntent = new Intent(getApplicationContext(), PlacePopularityActivity.class);
        placePopularityActivityIntent.putExtra("OnlineUser", onlineUser);
        placePopularityActivityIntent.putExtra("UserData", userData);
        startActivity(placePopularityActivityIntent);
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        mainActivityIntent.putExtra("UserData", userData);
        startActivity(mainActivityIntent);
    }

    private void logout() {
        // TO DO Switch To Login Activity and logout
    }
}