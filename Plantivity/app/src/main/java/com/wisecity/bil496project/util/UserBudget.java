package com.wisecity.bil496project.util;

import java.io.Serializable;
import java.util.Currency;
import java.util.Locale;

public class UserBudget implements Serializable {

        public Locale locale;
        public int amount;
        public Currency currency;

        public UserBudget(Locale locale, int amount) {
            this.locale = locale;
            this.amount = amount;
            this.currency = Currency.getInstance(locale);
        }

}
