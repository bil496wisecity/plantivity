package com.wisecity.bil496project.api_calls;

import android.icu.util.Calendar;
import android.os.AsyncTask;

import com.wisecity.bil496project.util.CityEvent;
import com.wisecity.bil496project.util.DownloadUrl;
import com.wisecity.bil496project.util.LatLngSerializable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetNearbyEvents extends AsyncTask {

    @Override
    protected Object doInBackground(Object[] objects) {

        String url = "http://192.168.0.13:8000/nearbyevents/";

        int radius = 20000;

        LatLngSerializable origin = (LatLngSerializable) objects[0];

        url += origin.latitude + "," + origin.longitude;

        url += "/" + radius;

        DownloadUrl downloadUrl = new DownloadUrl();
        ArrayList<CityEvent> events = new ArrayList<>();

        try {

            String response = downloadUrl.urlToJsonString(url);
            JSONArray jsonEvents = new JSONArray(response);

            for (int i = 0; i < jsonEvents.length(); i++) {
                JSONObject jsonEvent = jsonEvents.getJSONObject(i);

                CityEvent cityEvent = new CityEvent();

                cityEvent.title = jsonEvent.getString("event_name");
                cityEvent.setName(cityEvent.title);
                cityEvent.setVicinity(jsonEvent.getString("location"));
                cityEvent.setLatitude(jsonEvent.getString("lat"));
                cityEvent.setLongitude(jsonEvent.getString("lng"));
               // cityEvent.photoURL = jsonEvent.getString("photo_url");

                String dateString = jsonEvent.getString("date");

                String[] dateSplitted = dateString.split(" ");

                int day = Integer.parseInt(dateSplitted[0]);
                int month = encodeMonth(dateSplitted[1]);
                int year = Integer.parseInt(dateSplitted[2]);

                String[] hourSplitted = dateSplitted[4].split(":");

                int hour = Integer.parseInt(hourSplitted[0]);
                int minute = Integer.parseInt(hourSplitted[1]);

                cityEvent.date = Calendar.getInstance();
                cityEvent.date.set(year, month, day, hour, minute);

                events.add(cityEvent);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return events;

    }

    private int encodeMonth(String month) {

        if(month.equals("Oca"))
            return 0;

        if(month.equals("Şub"))
            return 1;

        if (month.equals("Mar"))
            return 2;

        if (month.equals("Nis"))
            return 3;

        if (month.equals("May"))
            return 4;

        if (month.equals("Haz"))
            return 5;

        if (month.equals("Tem"))
            return 6;

        if (month.equals("Ağu"))
            return 7;

        if (month.equals("Eyl"))
            return 8;

        if (month.equals("Eki"))
            return 9;

        if (month.equals("Kas"))
            return 10;

        if (month.equals("Ara"))
            return 11;

        return 0;
    }

}
