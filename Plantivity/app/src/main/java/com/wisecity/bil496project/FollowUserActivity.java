package com.wisecity.bil496project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class FollowUserActivity extends AppCompatActivity implements VolleyCallback {

    private ProgressBar spinner;

    private User onlineUser;
    private UserData userData;

    ArrayList<String> initialFollowingsList;

    EditText eTEmailOfUserToBeFollowed;
    Button btnFollowUser;
    private int initialFollowingCount;
    private int updatedFollowingCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_user);

        initialFollowingCount = 0;
        updatedFollowingCount = 0;

        // Online user provided from the main activity
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");

        initialFollowingsList = onlineUser.getFollowings();
        // User data provided from the main activity
        userData = (UserData) getIntent().getSerializableExtra("UserData");

        spinner = findViewById(R.id.spinnerFollowUser);
        spinner.setVisibility(View.GONE);
        eTEmailOfUserToBeFollowed = findViewById(R.id.eTEmailOfUserToBeFollowed);
        btnFollowUser = findViewById(R.id.btnFollowUser);
        btnFollowUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setVisibility(View.VISIBLE);
                followUser();
            }
        });

    }

    private void switchToMainActivity() {
        spinner.setVisibility(View.GONE);
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        mainActivityIntent.putExtra("UserData", userData);
        startActivity(mainActivityIntent);
    }

    private void followUser() {
        WebServerManager.getAllUsers(getApplicationContext(), this::onSuccessResponse, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }

    @Override
    public void onSuccessResponse(String result) {
        ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
        for(User registeredUser : listOfAllRegisteredUsers) {
            if(eTEmailOfUserToBeFollowed.getText().toString().equals(registeredUser.getEmail())) {
                WebServerManager.putFriendFollow(getApplicationContext(), onlineUser.getEmail(), eTEmailOfUserToBeFollowed.getText().toString());
            }
        }
        Handler handler = new Handler();
        int delay = 1500; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                checkOnlineUserUpdate();
                handler.postDelayed(this, delay);
            }
        }, delay);


    }

    private void updateOnlineUser(String emailOfOnlineUser, String passwordOfOnlineUser, ArrayList<String> followersOfOnlineUser, ArrayList<String>followingsOfOnlineUser) {
        onlineUser.setEmail(emailOfOnlineUser);
        onlineUser.setPassword(passwordOfOnlineUser);
        initialFollowingCount = onlineUser.getFollowings().size();
        onlineUser.getFollowers().clear();
        onlineUser.getFollowings().clear();

        for(String follower : followersOfOnlineUser) {
            onlineUser.addFollower(follower);
        }

        for(String following : followingsOfOnlineUser) {
            onlineUser.addFollowing(following);
        }
        updatedFollowingCount = onlineUser.getFollowings().size();
        if(initialFollowingCount != updatedFollowingCount) {
            switchToMainActivity();
        }
    }

    private void checkOnlineUserUpdate() {
        WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                for (User user : listOfAllRegisteredUsers) {
                    if ((user.getEmail().equals(onlineUser.getEmail())) && (user.getPassword().equals(onlineUser.getPassword()))) {
                        updateOnlineUser(user.getEmail(), user.getPassword(), user.getFollowers(), user.getFollowings());

                    } else {
                        //System.out.println(listOfAllRegisteredUsers.size()); // For debug purposes
                    }
                }

            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }
}
