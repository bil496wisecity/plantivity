package com.wisecity.bil496project;

public class FollowingsCardItem {
    private int followingsCardItemImageResource;
    private String followingsCardItemName;
    private String followingsCardItemDescription;

    public FollowingsCardItem(int followingsCardItemImageResource, String followingsCardItemName, String followingsCardItemDescription) {
        this.followingsCardItemImageResource = followingsCardItemImageResource;
        this.followingsCardItemName = followingsCardItemName;
        this.followingsCardItemDescription = followingsCardItemDescription;
    }

    // SETTERS
    public void setFollowingsCardItemName(String followingsCardItemName) {
        this.followingsCardItemName = followingsCardItemName;
    }

    public void setFollowingsCardItemDescription(String followingsCardItemDescription) {
        this.followingsCardItemDescription = followingsCardItemDescription;
    }

    // GETTERS
    public int getFollowingsCardItemImageResource() {
        return followingsCardItemImageResource;
    }

    public String getFollowingsCardItemName() {
        return followingsCardItemName;
    }

    public String getFollowingsCardItemDescription() {
        return followingsCardItemDescription;
    }
}
