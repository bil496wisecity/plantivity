package com.wisecity.bil496project;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    private String userName;
    private String email;
    private String password;

    EditText eTUsername;
    EditText eTEmail;
    EditText eTPassword;
    EditText eTConfirmPassword;
    Button btnRegister;
    TextView tVLoginHere;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        eTUsername = findViewById(R.id.eTUsername);
        eTEmail = findViewById(R.id.eTEmail);
        eTPassword = findViewById(R.id.eTPassword);
        eTConfirmPassword = findViewById(R.id.eTConfirmPassword);

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = eTUsername.getText().toString();
                email = eTEmail.getText().toString();
                password = eTPassword.getText().toString();
                if(password.equals(eTConfirmPassword.getText().toString())) {
                    sendRegisterData();
                }
                else {
                    Toast.makeText(RegisterActivity.this, "Passwords Doesn't Match, Please Try Again!", Toast.LENGTH_LONG).show();
                }

            }
        });

        tVLoginHere = findViewById(R.id.tVLoginHere);
        tVLoginHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToLoginActivity();
            }
        });
    }

    // Do all the register processes with the server here.
    protected void sendRegisterData() {
        User userToBeRegistered = new User();
        userToBeRegistered.setEmail(email);
        userToBeRegistered.setPassword(password);
        WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                boolean isEmailRegisteredBefore = false;
                for (User user : listOfAllRegisteredUsers) {
                    if (email.equals(user.getEmail())) {
                        isEmailRegisteredBefore = true;
                    }
                }
                if (isEmailRegisteredBefore == false) {
                    try {
                        WebServerManager.postUser(getApplicationContext(), userToBeRegistered);
                        switchToLoginActivity();
                    } catch (Exception registerException) {
                        registerException.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Email Already Registered, Please Try Again With Another Email!", Toast.LENGTH_LONG).show();
                }
            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });

    }

    protected void switchToLoginActivity() {
        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(loginIntent);
    }

}
