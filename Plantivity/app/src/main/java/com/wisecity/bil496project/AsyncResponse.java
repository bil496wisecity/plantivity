package com.wisecity.bil496project;

import com.wisecity.bil496project.util.UserData;

public interface AsyncResponse {
    void processFinish(UserData userData);
}
