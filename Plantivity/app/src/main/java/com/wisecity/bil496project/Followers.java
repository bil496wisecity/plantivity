package com.wisecity.bil496project;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Followers extends Fragment {
    private RecyclerView followersRecyclerView;
    private FollowersAdapter followersRecyclerViewAdapter;
    private RecyclerView.LayoutManager followersRecyclerViewLayoutManager;

    View view;

    User onlineUser;
    ArrayList<User> followersList;

    public Followers(User onlineUser, ArrayList<User> followersList) {
        this.onlineUser = onlineUser;
        this.followersList = followersList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ArrayList<FollowersCardItem> followersCardItems = new ArrayList<>();
        if(followersList.equals(null) == false) {
            if(followersList.size() > 0) {
                for(User follower : followersList) {
                    followersCardItems.add(new FollowersCardItem(R.drawable.background_profile_routes, follower.getEmail(), follower.getPassword()));
                }
            }
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_followers, container, false);

        followersRecyclerView = view.findViewById(R.id.followersRecyclerView);
        followersRecyclerView.setHasFixedSize(true);
        followersRecyclerViewLayoutManager = new LinearLayoutManager(this.getActivity());
        followersRecyclerViewAdapter = new FollowersAdapter(followersCardItems);

        followersRecyclerView.setLayoutManager(followersRecyclerViewLayoutManager);
        followersRecyclerView.setAdapter(followersRecyclerViewAdapter);

        followersRecyclerViewAdapter.setOnItemClickListener(new FollowersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                followersRecyclerViewAdapter.notifyItemChanged(position);
                UserData tempUserData = UserData.getInstance();
                User follower = new User();
                follower.setEmail(followersCardItems.get(position).getFollowersCardItemName());
                Intent followersProfileActivityIntent = new Intent(view.getContext(), FollowersProfileActivity.class);
                followersProfileActivityIntent.putExtra("OnlineUser", onlineUser);
                followersProfileActivityIntent.putExtra("Follower", follower);
                followersProfileActivityIntent.putExtra("UserData", tempUserData);
                view.getContext().startActivity(followersProfileActivityIntent);
            }

            @Override
            public void onDeleteClick(int position) {
                WebServerManager.putFriendUnFollow(view.getContext(), followersList.get(position).getEmail(), onlineUser.getEmail());
                followersCardItems.remove(position);
                followersList.remove(position);
                followersRecyclerViewAdapter.notifyItemRemoved(position);
            }
        });
        return view;
    }

}
