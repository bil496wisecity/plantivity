package com.wisecity.bil496project.optimal_route;

import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.UserInterest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class PlantivityRanker {

    private static PlantivityRanker plantivityRanker = null;

    private PriorityQueue<SubRanker> subRankers;

    private PlantivityRanker()
    {
        Comparator<SubRanker> subRankerComparator = (s1, s2) -> -s1.compareTo(s2);
        subRankers = new PriorityQueue<>(subRankerComparator);
    }

    public static PlantivityRanker getInstance()
    {
        if (plantivityRanker == null)
            plantivityRanker = new PlantivityRanker();

        return plantivityRanker;
    }

    public Place bestPlace() {
        return subRankers.peek().top();
    }

    public Place popBestPlace(boolean decreaseImportance) {

        if (subRankers.isEmpty())
            return null;

        Place top = subRankers.peek().pop();
        SubRanker peek = subRankers.peek();

        if (decreaseImportance)
            decreaseImportance(peek);

        return top;
    }

    public void addSubRanker(UserInterest userInterest, List<Place> places) {

        if (places.size() < 1) {
            return;
        }

        SubRanker subRanker = findSubRankerByType(userInterest.type);

        if (subRanker != null) {
            subRanker.addPlaces(places);
            return;
        }

        subRanker = new SubRanker(userInterest.type, userInterest.type.baseScore(),
                userInterest.interestLevel, userInterest.type.importanceDecreaseFactor());

        Collections.sort(places, (p1, p2) -> p2.getUserRatingsTotal() - p1.getUserRatingsTotal());

        for (Place place : places) {
            subRanker.addPlace(place);
        }

        subRankers.add(subRanker);
    }

    private SubRanker findSubRankerByType(UserInterest.Type type) {

        for (SubRanker subRanker : subRankers) {
            if (subRanker.type.equals(type))
                return subRanker;
        }

        return null;
    }

    public void decreaseImportance(SubRanker subRanker) {
        if (subRanker.type.isInGroup(UserInterest.Group.food)) {
            decreaseImporanceOfGroup(UserInterest.Group.food);
            return;
        }
        subRanker.decreaseImportance();
    }

    private void decreaseImporanceOfGroup(UserInterest.Group group) {
        List<SubRanker> subRankersToBeUpdated = new ArrayList<>();

        for (SubRanker subRanker: subRankers) {
            if (subRanker.type.isInGroup(group)) {
                subRankersToBeUpdated.add(subRanker);
            }
        }

        for (SubRanker subRanker: subRankersToBeUpdated) {
            subRanker.decreaseImportance();
        }
    }

    private boolean updatePriority(SubRanker subRanker) {
        if (subRankers.remove(subRanker))
            return subRankers.add(subRanker);
        else
            return false;
    }

    private class SubRanker implements Comparable<SubRanker> {

        private UserInterest.Type type;
        private double baseScore;
        private double userInterestScore;
        private double importance;
        private double importanceDecreaseFactor;
        private PriorityQueue<Place> places;

        private SubRanker(UserInterest.Type type, double baseScore, double userInterestScore, double importanceDecreaseFactor) {
            this.type = type;
            this.baseScore = baseScore;
            this.userInterestScore = userInterestScore;
            this.importanceDecreaseFactor = importanceDecreaseFactor;
            importance = 0;
            places = new PriorityQueue<>((p1, p2) -> p2.getUserRatingsTotal() - p1.getUserRatingsTotal());
        }

        private void addPlace(Place place) {
            if (contains(place))
                return;
            places.add(place);
        }

        private void addPlaces(List<Place> places) {
            for (Place place : places) {
                addPlace(place);
            }
        }

        private Place top() {
            if (places.size() > 0)
                return places.peek();
            else
                return null;
        }

        private Place pop() {
            if (places.size() > 0)
                return places.remove();
            else
                return null;
        }

        private double getScore() {
            double topPlaceScore = 0;

            if (places.size() > 0) {
                topPlaceScore = places.peek().getRating();
            }
            return importance + baseScore + userInterestScore + topPlaceScore / 20;
        }

        private void decreaseImportance() {
            importance += importanceDecreaseFactor;
            updatePriority(this);
        }

        private boolean contains(Place place){
            String placeId = place.getPlaceId();
            return places.stream().filter(p -> p.getPlaceId().equals(placeId)).findFirst().isPresent();
        }

        @Override
        public int compareTo(SubRanker subRanker) {
            return Double.compare(getScore(), subRanker.getScore());
        }

    }

}
