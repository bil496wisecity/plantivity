package com.wisecity.bil496project.util;

public class Message {
    private String message;
    private String userEmail;
    private String createdAt;
    private boolean messageBelongToCurrentUser;

    public Message(){
        message = "-NaN-";
        userEmail = "-Nan-";
        createdAt = "00:00";
        messageBelongToCurrentUser = false;
    }


    public void setMessage(String message){ this.message = message;}
    public void setUserEmail(String userEmail){ this.userEmail = userEmail;}
    public void setCreatedAt(String createdAt){ this.createdAt = createdAt;}
    public void setMessageBelongToCurrentUser(boolean messageBelongToCurrentUser){ this.messageBelongToCurrentUser = messageBelongToCurrentUser;}

    public String getMessage(){ return this.message;}
    public String getUserEmail(){ return this.userEmail;}
    public String getCreatedAt(){ return this.createdAt;}
    public boolean getMessageBelongToCurrentUser(){ return this.messageBelongToCurrentUser;}

}
