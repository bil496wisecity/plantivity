package com.wisecity.bil496project;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserInfo extends Fragment {

    private int PICK_IMAGE_REQUEST = 1;

    View view;

    private boolean isProfilePictureChangedAtThisSession;

    private TextView tVUsername;
    private ImageView iVUserProfilePicture;
    private Button btnChangeProfilePicture;

    Context applicationContext;

    Bitmap selectedImage;

    public UserInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        isProfilePictureChangedAtThisSession = false;
        view = inflater.inflate(R.layout.fragment_user_info, container, false);
        applicationContext = ProfileActivity.getContextOfApplication();
        tVUsername = view.findViewById(R.id.tVUsername);
        iVUserProfilePicture = view.findViewById(R.id.iVProfilePicture);
        iVUserProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture(v);
                isProfilePictureChangedAtThisSession = true;
            }
        });
        btnChangeProfilePicture = view.findViewById(R.id.btnChangeProfilePicture);
        btnChangeProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isProfilePictureChangedAtThisSession == true) {
                    // Upload The New Profile Image
                    // TO DO
                    Toast.makeText(getActivity(), "Profile Picture Is Changed!", Toast.LENGTH_SHORT).show();
                    // TO DO REFRESH THE ACTIVITY
                }
                else if(isProfilePictureChangedAtThisSession == false){
                    Toast.makeText(getActivity(), "Profile Picture Is Not Changed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    protected void selectPicture(View view) {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) { //Kullanıcı bize resim seçme izini verdi mi?
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, 2); // 2 request kodunu hatirla
        }
        else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 1); // 1 i hatırla
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == 2) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 1); // 1 i hatırla
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == 1) {
            if(resultCode == RESULT_OK && data != null) {
                Uri uri = data.getData();
                try {
                    selectedImage = MediaStore.Images.Media.getBitmap(applicationContext.getContentResolver(), uri);
                    iVUserProfilePicture.setImageBitmap(selectedImage);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addImage(View view) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
    }

}
