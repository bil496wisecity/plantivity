package com.wisecity.bil496project.optimal_route.place_graph;

import com.wisecity.bil496project.util.LatLngSerializable;
import com.wisecity.bil496project.util.UserTimeUnit;
import com.wisecity.bil496project.util.Place;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Node {

    private String id;

    private UserTimeUnit cost;

    private double score;

    private boolean visited;

    private LatLngSerializable location;

    private List<Edge> edges;

    private Place place;

    private Date arrivalTime;

    public Node(String id, UserTimeUnit cost, double score, LatLngSerializable location) {
        this.id = id;
        this.cost = cost;
        this.score = score;
        visited = false;
        this.location = location;
        edges = new ArrayList<>();
    }

    public Node(String id, UserTimeUnit cost, double score, LatLngSerializable location, Place place) {
        this.id = id;
        this.cost = cost;
        this.score = score;
        visited = false;
        this.location = location;
        this.place = place;
        edges = new ArrayList<>();
    }

    public UserTimeUnit getCost() { return cost; }

    public double getScore() { return score; }

    public Date getArrivalTime() { return arrivalTime; }

    public void setArrivalTime(Date arrivalTime) { this.arrivalTime = arrivalTime; }

    public void visit() { visited = true; }

    public void unvisit() { visited = false; }

    public boolean isVisited() {return visited;}

    public LatLngSerializable getLocation(){
        return location;
    }

    public void addEdge(Edge e) {
        edges.add(e);
    }

    public List<Edge> edgeList() {
        return edges;
    }

    public void cutEdges() { edges = new ArrayList<>(); }

    public Place getPlace() { return place; }

    public Node getOrigin(Node root) {

        Node head = root;

        while (true) {

            if (head.edgeList().size() <= 0)
                return null;

            Node destination = head.edgeList().get(0).getDestination();

            if (destination == this)
                return head;

            head = destination;
        }
    }

    public boolean isTailNode() {
        return edgeList().size() < 1;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append("Id: " + id + ", ");
        s.append("Name: " + place.getName() + ", ");
        s.append("Cost: " + cost.amount + ", ");
        s.append("Score: " + score);

        return s.toString();
    }

}
