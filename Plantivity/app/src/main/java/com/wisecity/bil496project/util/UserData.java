package com.wisecity.bil496project.util;

import android.icu.util.Calendar;

import com.google.maps.model.TravelMode;
import com.wisecity.bil496project.optimal_route.DistanceBetweenTwoGeoCoords;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class UserData implements Serializable {

    private String userName; // name of current user
    private String sessionName; // name of current session
    private LatLngSerializable currentLocation;
    private List<Place> nearByPlaces; // nearby places to user's current location
    private UserBudget budget; // amount of money user allocate for that session
    private UserTimeUnit minutes; // how many minutes user have
    private TravelMode travelMode;
    private TransportType transportType; // which transportation user use
    private List<Place> chosenPlaces; // nearby places user want to travel
    private List<CityEvent> nearByEvents; // nearby events to starting location
    private List<CityEvent> chosenEvents; // nearby events to starting location
    private List<Place> route; // route calculated for user
    private List<UserInterest> userInterests;
    private String amountOfBudget;
    private Date travelDate;
    private boolean sentFromProfileActivity;

    private static UserData instance = null;

    public static UserData getInstance()
    {
        if (instance == null)
            instance = new UserData();

        return instance;
    }

    private UserData() {
        userName = "templateUser";
        sessionName = "111111111";
        budget = new UserBudget(Locale.US, 100); // user have 100$
        minutes = new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 75); // user have 75 min.
        transportType = TransportType.BUS;
        travelMode = TravelMode.DRIVING; //Directions API uses travelMode rather than transportType. This should be considered.
        nearByPlaces = new ArrayList<>();
        chosenPlaces = new ArrayList<>();
        route = new ArrayList<>();
        userInterests = new ArrayList<>();
        userInterests.add(new UserInterest(UserInterest.Type.tourist_attraction, 5));
        nearByEvents = new ArrayList<>();
        chosenEvents = new ArrayList<>();
        currentLocation = new LatLngSerializable(39.9272, 32.8182);
        for (Place p : nearByPlaces){
            p.setDistance(DistanceBetweenTwoGeoCoords.calculateDistance(currentLocation.latitude, Double.parseDouble(p.getLatitude()), currentLocation.longitude,Double.parseDouble( p.getLongitude()), 0, 0));
        }
        sentFromProfileActivity = false;
        travelDate = Calendar.getInstance().getTime();
    }

    public void setUserName(String userName) { this.userName = userName; }
    public void setSessionName(String sessionName) { this.sessionName = sessionName; }
    public void setCurrentLocation(LatLngSerializable currentLocation) { this.currentLocation = currentLocation; }
    public void setBudget(UserBudget budget) { this.budget = budget; }
    public void setMinutes(UserTimeUnit minutes) { this.minutes = minutes; }
    public void setTransportType(TransportType transportType) { this.transportType = transportType; }
    public void setNearByPlaces(List<Place> nearByPlaces) { this.nearByPlaces = nearByPlaces; }
    public void setChosenPlaces(List<Place> chosenPlaces) { this.chosenPlaces = chosenPlaces; }
    public void setNearByEvents(List<CityEvent> nearByEvents) { this.nearByEvents = nearByEvents; }
    public void setChosenEvents(List<CityEvent> chosenPlaces) { this.chosenEvents = chosenPlaces; }
    public void setRoute(List<Place> route) { this.route = route; }
    public void setUserInterests(List<UserInterest> userInterests) {this.userInterests = userInterests;}
    public void setAmountOfBudget(String amountOfBudget) {
        this.amountOfBudget = amountOfBudget;
    }
    public void setTravelMode(TravelMode travelMode) { this.travelMode = travelMode; }
    public void setTravelDate(Date date) { this.travelDate = date; }
    public void setSentFromProfileActivity(boolean sentFromProfileActivity) { this.sentFromProfileActivity = sentFromProfileActivity; }

    public String getUserName() { return userName; }
    public String getSessionName() { return sessionName; }
    public LatLngSerializable getCurrentLocation() { return currentLocation; }
    public UserBudget getBudget() { return budget; }
    public UserTimeUnit getMinutes() { return minutes; }
    public TransportType getTransportType() { return transportType; }
    public List<Place> getNearByPlaces() { return nearByPlaces; }
    public List<Place> getChosenPlaces() { return chosenPlaces; }
    public List<CityEvent> getChosenEvents() { return chosenEvents; }
    public List<CityEvent> getNearByEvents() { return nearByEvents;}
    public List<Place> getRoute() { return route; }
    public List<UserInterest> getUserInterests(){ return userInterests;}
    public String getAmountOfBudget() {
        return amountOfBudget;
    }
    public TravelMode getTravelMode() { return travelMode; }
    public Date getTravelDate() { return travelDate; }
    public boolean getSentFromProfileActivity() { return sentFromProfileActivity; }







}
