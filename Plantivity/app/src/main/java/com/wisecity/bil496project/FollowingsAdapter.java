package com.wisecity.bil496project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FollowingsAdapter extends RecyclerView.Adapter<FollowingsAdapter.FollowingsViewHolder> {

    private ArrayList<FollowingsCardItem> followingsCardItems;

    private OnItemClickListener followingsOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        followingsOnItemClickListener = listener;
    }

    public static class FollowingsViewHolder extends RecyclerView.ViewHolder{

        public ImageView iVFollowingsCardItem;
        public TextView tVFollowingsCardItemName;
        public TextView tVFollowingsCardItemDescription;
        public ImageView iVDeleteFollowingsCardItem;

        public FollowingsViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            iVFollowingsCardItem = itemView.findViewById(R.id.iVFollowingsCardItem);
            tVFollowingsCardItemName = itemView.findViewById(R.id.tVFollowingsName);
            tVFollowingsCardItemDescription = itemView.findViewById(R.id.tVFollowingsDescription);
            iVDeleteFollowingsCardItem = itemView.findViewById(R.id.iVDeleteFollowingsCardItem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            iVDeleteFollowingsCardItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public FollowingsAdapter(ArrayList<FollowingsCardItem> followingsCardItems) {
        this.followingsCardItems = followingsCardItems;
    }

    @NonNull
    @Override
    public FollowingsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.followings_card_item, parent, false);
        FollowingsViewHolder fVH = new FollowingsViewHolder(v, followingsOnItemClickListener);
        return  fVH;
    }

    @Override
    public void onBindViewHolder(@NonNull FollowingsViewHolder holder, int position) {
        // At first item position is 0
        FollowingsCardItem  currentFollowingsCardItem = followingsCardItems.get(position);

        holder.iVFollowingsCardItem.setImageResource(currentFollowingsCardItem.getFollowingsCardItemImageResource());
        holder.tVFollowingsCardItemName.setText(currentFollowingsCardItem.getFollowingsCardItemName());
        holder.tVFollowingsCardItemDescription.setText(currentFollowingsCardItem.getFollowingsCardItemDescription());
    }

    @Override
    public int getItemCount() {
        return followingsCardItems.size();
    }
}
