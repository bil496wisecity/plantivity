package com.wisecity.bil496project.optimal_route.place_graph;

import com.wisecity.bil496project.util.TransportType;

public class Edge {

    private Node destination;

    private double cost;

    private TransportType transportType;

    public Edge(Node destination, double cost) {
        this.destination = destination;
        this.cost = cost;
    }

    public Node getDestination() {
        return destination;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) { this.cost = cost; }

    public void setDestination(Node destination) { this.destination = destination; }

    public TransportType getTransportType() { return transportType; }

    public void setTransportType(TransportType transportType) { this.transportType = transportType; }

}
