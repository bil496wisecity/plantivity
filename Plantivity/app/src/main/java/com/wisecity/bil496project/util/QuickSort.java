package com.wisecity.bil496project.util;

import com.wisecity.bil496project.optimal_route.DistanceBetweenTwoGeoCoords;

import java.util.List;

public class QuickSort {

    /* This function takes last element as pivot,
      places the pivot element at its correct
      position in sorted array, and places all
      smaller (smaller than pivot) to left of
      pivot and all greater elements to right
      of pivot */
    public static int partitionByRating(List<Place> nearbyPlaces, int low, int high)
    {
        double pivot = nearbyPlaces.get(high).getRating();
        int i = (low-1); // index of smaller element
        for (int j=low; j<high; j++)
        {
            // If current element is smaller than the pivot
            if (nearbyPlaces.get(j).getRating() > pivot)
            {
                i++;

                Place temp = nearbyPlaces.get(i);
                nearbyPlaces.set(i, nearbyPlaces.get(j));
                nearbyPlaces.set(j, temp);
            }
        }

        Place temp = nearbyPlaces.get(i+1);
        nearbyPlaces.set(i+1, nearbyPlaces.get(high));
        nearbyPlaces.set(high, temp);

        return i+1;
    }

    public static int partitionByDistance(List<Place> nearbyPlaces, LatLngSerializable latlon, int low, int high){
        double pivot = DistanceBetweenTwoGeoCoords.calculateDistance(latlon.latitude, Double.parseDouble(nearbyPlaces.get(high).getLatitude()),
                latlon.longitude, Double.parseDouble(nearbyPlaces.get(high).getLongitude()), 0.0,0.0)  ;


        int i = (low-1); // index of smaller element
        for (int j=low; j<high; j++)
        {
            // If current element is smaller than the pivot
            double currentPlaceDist = DistanceBetweenTwoGeoCoords.calculateDistance(latlon.latitude, Double.parseDouble(nearbyPlaces.get(j).getLatitude()),
                    latlon.longitude, Double.parseDouble(nearbyPlaces.get(j).getLongitude()), 0.0,0.0)  ;
            if (currentPlaceDist < pivot)
            {
                i++;

                Place temp = nearbyPlaces.get(i);
                nearbyPlaces.set(i, nearbyPlaces.get(j));
                nearbyPlaces.set(j, temp);
            }
        }

        Place temp = nearbyPlaces.get(i+1);
        nearbyPlaces.set(i+1, nearbyPlaces.get(high));
        nearbyPlaces.set(high, temp);

        return i+1;
    }

    public static void sortByDistance(List<Place> nearbyPlaces, LatLngSerializable latlon, int low, int high)
    {
        if (low < high)
        {
            /* pi is partitioning index, arr[pi] is
              now at right place */
            int pi = partitionByDistance(nearbyPlaces, latlon, low, high);

            // Recursively sort elements before
            // partition and after partition
            sortByDistance(nearbyPlaces,latlon, low, pi-1);
            sortByDistance(nearbyPlaces,latlon, pi+1, high);
        }
    }



    public static void sortByRating(List<Place> nearbyPlaces, int low, int high)
    {
        if (low < high)
        {
            /* pi is partitioning index, arr[pi] is
              now at right place */
            int pi = partitionByRating(nearbyPlaces, low, high);

            // Recursively sort elements before
            // partition and after partition
            sortByRating(nearbyPlaces, low, pi-1);
            sortByRating(nearbyPlaces, pi+1, high);
        }
    }
}
