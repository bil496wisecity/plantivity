package com.wisecity.bil496project.util;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

    private String email;
    private String password;
    private String deviceId;
    private ArrayList<String> routeIds;
    private ArrayList<String> followers;
    private ArrayList<String> followings;
    private String spotifyUsername;

    public User(){
        this.email = "-NA-";
        this.password = "-NA-";
        this.routeIds = new ArrayList<>();
        this.followers = new ArrayList<>();
        this.followings = new ArrayList<>();
        this.spotifyUsername = "";
        this.deviceId = "";
    }

    public void setEmail(String email){ this.email = email;}
    public void setDeviceId(String deviceId){ this.deviceId = deviceId;}
    public void setPassword(String password){ this.password = password;}
    public void addRouteId(String routeId) {
        this.routeIds.add(routeId);
    }
    public void addFollower(String followerId){ this.followers.add(followerId);}
    public void addFollowing(String followingId){ this.followings.add(followingId);}
    public void setSpotifyUsername(String spotifyUsername){this.spotifyUsername = spotifyUsername;}

    public String getEmail(){ return this.email;}
    public String getDeviceId(){ return this.deviceId;}
    public String getPassword(){ return this.password;}
    public ArrayList<String> getRouteIds() {
        return this.routeIds;
    }
    public ArrayList<String> getFollowers(){ return this.followers;}
    public ArrayList<String> getFollowings(){ return this.followings;}
    public String getSpotifyUsername(){return this.spotifyUsername;}

    public boolean equals(User user) {
        if(this.getEmail().equals(user.getEmail())) {
            boolean followersAndFollowingsAreEqual = true;

            if(this.getFollowers().size() == user.getFollowers().size()) {
                for(int i = 0; i < this.getFollowers().size(); i++) {
                    if(this.getFollowers().get(i).equals(user.getFollowers().get(i))) {
                        followersAndFollowingsAreEqual = true;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                return false;
            }

            if(this.getFollowings().size() == user.getFollowings().size()) {
                for(int i = 0; i < this.getFollowings().size(); i++) {
                    if(this.getFollowings().get(i).equals(user.getFollowings().get(i))) {
                        followersAndFollowingsAreEqual = true;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                return false;
            }

            if(this.password.equals(user.getPassword())) {
                if(followersAndFollowingsAreEqual == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }

        }
        else {
            return false;
        }
    }

}
