package com.wisecity.bil496project.optimal_route.place_graph;

import com.wisecity.bil496project.optimal_route.DistanceBetweenTwoGeoCoords;
import com.wisecity.bil496project.optimal_route.OptimalRoute;
import com.wisecity.bil496project.util.LatLngSerializable;
import com.wisecity.bil496project.util.UserTimeUnit;
import com.wisecity.bil496project.util.Place;

import java.util.ArrayList;
import java.util.List;

public class PlaceGraph {

    private class Pair {
        private Node node;
        private double distance;

        private Pair(Node node, double distance){
            this.node = node;
            this.distance = distance;
        }
    }

    private Node root;
    private OptimalRoute optimalRoute;

    public Node buildPlaceGraph(LatLngSerializable startingLocation, List<Place> places,
                                OptimalRoute optimalRoute) {

        this.optimalRoute = optimalRoute;

        root = new Node("root", new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 0),
                0, startingLocation);

        if (places == null)
            return root;

        Node head = root;

        LatLngSerializable currentPlaceLocation = startingLocation;

        ArrayList<Node> placeNodes = createNodes(places);

        while (placeNodes.size() > 0) {

            Pair nearestPlace = getNearestPlaceToLocation(currentPlaceLocation, placeNodes);

            Edge edgeToNearestPlaceNode = new Edge(nearestPlace.node, nearestPlace.distance);

            head.addEdge(edgeToNearestPlaceNode);

            head = nearestPlace.node;

            currentPlaceLocation = nearestPlace.node.getLocation();

            placeNodes.remove(nearestPlace.node);

        }

        return root;

    }

    public Node getMinCostNodeToPlace(Place place, Node graph) {

        Node nearestPlaceNode = null;

        Node head = graph;

        double distanceToNearest = Double.MAX_VALUE;

        double referenceLat = Double.parseDouble(place.getLatitude());
        double referenceLng = Double.parseDouble(place.getLongitude());

        while (true) {

            double lat = head.getLocation().latitude;
            double lng = head.getLocation().longitude;

            if (lat == referenceLat && lng == referenceLng)
                continue;

            double totalDistance = 0;

            double minCostNodeToPlaceDistance = DistanceBetweenTwoGeoCoords.
                    calculateDistance(referenceLat, lat, referenceLng, lng, 0, 0);

            totalDistance += minCostNodeToPlaceDistance;

            if (!head.isTailNode()) {

                Node adjacentNode = head.edgeList().get(0).getDestination();

                double placeToAdjacentNodeDistance = DistanceBetweenTwoGeoCoords.
                        calculateDistance(referenceLat, adjacentNode.getLocation().latitude,
                                referenceLng, adjacentNode.getLocation().longitude, 0, 0);

                totalDistance += placeToAdjacentNodeDistance;

                double nearestNodeToAdjacentDistance = DistanceBetweenTwoGeoCoords.
                        calculateDistance(lat, adjacentNode.getLocation().latitude, lng,
                                adjacentNode.getLocation().longitude,0, 0);

                totalDistance -= nearestNodeToAdjacentDistance;

            }

            if (totalDistance < distanceToNearest) {
                nearestPlaceNode = head;
                distanceToNearest = totalDistance;
            }

            if (head.edgeList().size() <= 0) {
                break;
            }

            head = head.edgeList().get(0).getDestination();

        }

        return nearestPlaceNode;

    }


    public Node getMinCostNodeToEvent(Place place, Node graph) {

        Node nearestPlaceNode = null;

        Node head = graph;

        double distanceToNearest = Double.MAX_VALUE;

        double referenceLat = Double.parseDouble(place.getLatitude());
        double referenceLng = Double.parseDouble(place.getLongitude());

        while (true) {

            double lat = head.getLocation().latitude;
            double lng = head.getLocation().longitude;

            if (lat == referenceLat && lng == referenceLng)
                continue;

            double totalDistance = 0;

            double minCostNodeToPlaceDistance = DistanceBetweenTwoGeoCoords.
                    calculateDistance(referenceLat, lat, referenceLng, lng, 0, 0);

            totalDistance += minCostNodeToPlaceDistance;

            if (!head.isTailNode()) {

                Node adjacentNode = head.edgeList().get(0).getDestination();

                double placeToAdjacentNodeDistance = DistanceBetweenTwoGeoCoords.
                        calculateDistance(referenceLat, adjacentNode.getLocation().latitude,
                                referenceLng, adjacentNode.getLocation().longitude, 0, 0);

                totalDistance += placeToAdjacentNodeDistance;

                double nearestNodeToAdjacentDistance = DistanceBetweenTwoGeoCoords.
                        calculateDistance(lat, adjacentNode.getLocation().latitude, lng,
                                adjacentNode.getLocation().longitude,0, 0);

                totalDistance -= nearestNodeToAdjacentDistance;

            }

            if (totalDistance < distanceToNearest && !head.isTailNode()) {
                nearestPlaceNode = head;
                distanceToNearest = totalDistance;
            }

            if (head.edgeList().size() <= 0) {
                break;
            }

            head = head.edgeList().get(0).getDestination();

        }

        return nearestPlaceNode;

    }

    private Pair getNearestPlaceToLocation(LatLngSerializable referenceLocation,
                                           ArrayList<Node> placeNodes) {

        Node nearestPlaceNode = null;

        double distanceToNearest = Double.MAX_VALUE;

        double referenceLat = referenceLocation.latitude;
        double referenceLng = referenceLocation.longitude;

        for (Node placeNode : placeNodes) {

            double lat = Double.parseDouble(placeNode.getPlace().getLatitude());
            double lng = Double.parseDouble(placeNode.getPlace().getLongitude());

            if (lat == referenceLat && lng == referenceLng)
                continue;

            double distance = DistanceBetweenTwoGeoCoords.
                    calculateDistance(referenceLat, lat, referenceLng, lng, 0, 0);

            if (distance < distanceToNearest) {
                nearestPlaceNode = placeNode;
                distanceToNearest = distance;
            }

        }

        return new Pair(nearestPlaceNode, distanceToNearest);

    }

    private ArrayList<Node> createNodes(List<Place> places) {

        ArrayList<Node> nodes = new ArrayList<>();

        for (Place place : places) {

            String placeId = place.getPlaceId();

            double rating = place.getRating();

            UserTimeUnit cost = new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 0);

            cost.amount = optimalRoute.getTimeSpentOnLocation(place);

            double lat = Double.parseDouble(place.getLatitude());
            double lng = Double.parseDouble(place.getLongitude());

            Node node = new Node(placeId, cost, rating, new LatLngSerializable(lat, lng), place);

            nodes.add(node);

        }

        return nodes;

    }

}