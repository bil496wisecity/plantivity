package com.wisecity.bil496project.util;

public class Notification {
    private String title;
    private String messageBody;

    public Notification(){
        title = "-NaN-";
        messageBody = "-Nan-";
    }


    public void setNotificationTitle(String title){ this.title = title;}
    public void setNotificationBody(String messageBody){ this.messageBody = messageBody;}

    public String getNotificationTitle(){ return this.title;}
    public String getNotificationBody(){ return this.messageBody;}
}
