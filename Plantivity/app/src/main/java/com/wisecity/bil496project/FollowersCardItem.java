package com.wisecity.bil496project;

public class FollowersCardItem {
    private int followersCardItemImageResource;
    private String followersCardItemName;
    private String followersCardItemDescription;

    public FollowersCardItem(int followersCardItemImageResource, String followersCardItemName, String followersCardItemDescription) {
        this.followersCardItemImageResource = followersCardItemImageResource;
        this.followersCardItemName = followersCardItemName;
        this.followersCardItemDescription = followersCardItemDescription;
    }

    // SETTERS
    public void setFollowersCardItemName(String followersCardItemName) {
        this.followersCardItemName = followersCardItemName;
    }

    public void setFollowersCardItemDescription(String followersCardItemDescription) {
        this.followersCardItemDescription = followersCardItemDescription;
    }

    // GETTERS
    public int getFollowersCardItemImageResource() {
        return followersCardItemImageResource;
    }

    public String getFollowersCardItemName() {
        return followersCardItemName;
    }

    public String getFollowersCardItemDescription() {
        return followersCardItemDescription;
    }
}
