package com.wisecity.bil496project;

import com.wisecity.bil496project.util.Place;

import java.util.ArrayList;

public class RouteHistoryCardItem {
    private int routeHistoryCardItemImageResource;
    private String routeHistoryCardItemName;
    private String routeHistoryCardItemDescription;
    ArrayList<Place> places = new ArrayList<>();

    public RouteHistoryCardItem(int routeHistoryCardItemImageResource, String routeHistoryCardItemName, String routeHistoryCardItemDescription, ArrayList<Place> placesList) {
        this.routeHistoryCardItemImageResource = routeHistoryCardItemImageResource;
        this.routeHistoryCardItemName = routeHistoryCardItemName;
        this.routeHistoryCardItemDescription = routeHistoryCardItemDescription;
        for(Place place : placesList) {
            this.places.add(place);
        }
    }

    // SETTERS
    public void setRouteHistoryCardItemName(String routeHistoryCardItemName) {
        this.routeHistoryCardItemName = routeHistoryCardItemName;
    }

    public void setRouteHistoryCardItemDescription(String routeHistoryCardItemDescription) {
        this.routeHistoryCardItemDescription = routeHistoryCardItemDescription;
    }

    // GETTERS
    public int getRouteHistoryCardItemImageResource() {
        return routeHistoryCardItemImageResource;
    }

    public String getRouteHistoryCardItemName() {
        return routeHistoryCardItemName;
    }

    public String getRouteHistoryCardItemDescription() {
        return routeHistoryCardItemDescription;
    }

    // Mocked method for testing find route from RouteHistory
    protected void initializeMockedRouteForCard1() {
        ArrayList<Place> mockedPlaces =  new ArrayList<>();
        Place mockedPlace1 = new Place();
        mockedPlace1.setName("Mocked Place 1");
        mockedPlace1.setLatitude("37.4142744");
        mockedPlace1.setLongitude("-122.077409");
        mockedPlace1.setRating(4);

        Place mockedPlace2 = new Place();
        mockedPlace2.setName("Mocked Place 2");
        mockedPlace2.setLatitude("37.41921110000001");
        mockedPlace2.setLongitude("-122.0942484");
        mockedPlace2.setRating(3);

        Place mockedPlace3 = new Place();
        mockedPlace3.setName("Mocked Place 3");
        mockedPlace3.setLatitude("37.4218453");
        mockedPlace3.setLongitude("-122.0965184");
        mockedPlace3.setRating(2);

        mockedPlaces.add(mockedPlace1);
        mockedPlaces.add(mockedPlace2);
        mockedPlaces.add(mockedPlace3);

        this.places.add(mockedPlace1);
        this.places.add(mockedPlace2);
        this.places.add(mockedPlace3);

    }
}
