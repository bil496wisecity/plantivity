package com.wisecity.bil496project;

import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.wisecity.bil496project.api_calls.GetNearbyPlacesToEvents;
import com.wisecity.bil496project.util.CityEvent;
import com.wisecity.bil496project.util.DiscoverPlaces.SwipeCard;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DiscoverEventsActivity extends AppCompatActivity {

    public static ArrayList<CityEvent> localChosenEvents = new ArrayList<>();
    private SwipePlaceHolderView mSwipeView;
    FloatingActionButton fab;
    private Context mContext;
    private UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_places);

        mSwipeView = findViewById(R.id.swipeView);
        mContext = getApplicationContext();
        localChosenEvents.clear();
        // User data provided from main activity
        userData = UserData.getInstance();
        userData.getChosenEvents().clear(); // In every entry to discover places restart the process
        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.swipe_out_msg_view));

        if(userData.getNearByEvents() != null){
            List<CityEvent> filteredNearbyEvents = filterEvents(userData.getNearByEvents());
            for(CityEvent cityEvent: filteredNearbyEvents){
                String photoURL = getPhotoURL(cityEvent);
                mSwipeView.addView(new SwipeCard(mContext, cityEvent, photoURL, mSwipeView, true));
            }
        }

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            if(localChosenEvents.size() != 0){
                userData.setChosenEvents(localChosenEvents);
            }
            switchToMainActivity();
        });

        findViewById(R.id.rejectBtn).setOnClickListener(v -> mSwipeView.doSwipe(false));
        findViewById(R.id.acceptBtn).setOnClickListener(v -> mSwipeView.doSwipe(true));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public String getPhotoURL(CityEvent cityEvent) {
        if(cityEvent.photoURL == null){
            return "https://source.unsplash.com/NYyCqdBOKwc/600x800";
        }
        return cityEvent.photoURL;
    }

    private List<CityEvent> filterEvents(List<CityEvent> nearbyEvents) {

        List<CityEvent> filteredEvents = new ArrayList<>();

        long travelDateInMillis = userData.getTravelDate().getTime();
        long _12hoursLater = travelDateInMillis + 12 * 3600000;

        for(CityEvent cityEvent: nearbyEvents){

            long dateInMillis = cityEvent.date.getTimeInMillis();

            if (dateInMillis > travelDateInMillis && dateInMillis < _12hoursLater)
                filteredEvents.add(cityEvent);

        }

        return filteredEvents;
    }

    private void switchToMainActivity() {
        includeNearbyPlacesToEvents();
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainActivityIntent);
    }

    private void includeNearbyPlacesToEvents() {
        Object[] transferData = new Object[1];
        transferData[0] = getApplicationContext();
        new GetNearbyPlacesToEvents().execute(transferData);

    }

    public static void fillChosenEvents(Place place){
        localChosenEvents.add((CityEvent) place);
    }
}
