package com.wisecity.bil496project.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.wisecity.bil496project.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MyAutoCompleteAdapter  <T> extends ArrayAdapter<T> implements Filterable {
    private List <T> listObjects;
    List<T> suggestions = new ArrayList<>();
    private int resource;

    private Filter mFilter = new Filter(){
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if(constraint != null) {
                suggestions.clear();
                String[] splittedConstraint = constraint.toString().split("\\s+");
                String word = "#hkb4_bphck&ass3!f52%7&t$u@*$eo)#&c5$hb+*6g=in#l)g";
                for(int i = 0; i < splittedConstraint.length; i++){
                    if(splittedConstraint[i].charAt(0) == '@'){
                        word = splittedConstraint[i];
                        break; //This is for only first mentioned user can be shown
                    }
                }
                for(T object : listObjects){
                    String autoWord = "@" + object.toString();
                    if(autoWord.toLowerCase().equals(word.toLowerCase())){
                        suggestions.clear();
                        break;
                    }

                    if(autoWord.toLowerCase().contains(word.toLowerCase())){
                        suggestions.add(object);
                    }

                }

                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence contraint, FilterResults results) {
            if(results == null){
                return;
            }

            List<T> filteredList = (List<T>) results.values;
            if(results.count > 0) {
                clear();
                for (T filteredObject : filteredList) {
                    add(filteredObject);
                }
                notifyDataSetChanged();
            }
        }
    };

    public MyAutoCompleteAdapter(Context context, List<T> listObjects) {
        super(context, R.layout.list_item_simple, listObjects);
        this.listObjects = new ArrayList<>(listObjects);
        this.resource = R.layout.list_item_simple;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Object listObject = getItem(position);
        viewHolder holder;
        if(convertView != null) {
            holder = (viewHolder) convertView.getTag();
        }else{
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);
            holder = new viewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.name.setText(listObject.toString());

        return convertView;
    }


    static class viewHolder {
        @Bind(R.id.text_view_simple)
        TextView name;

        public viewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
