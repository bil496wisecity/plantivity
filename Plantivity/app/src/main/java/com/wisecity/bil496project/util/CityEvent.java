package com.wisecity.bil496project.util;

import android.icu.util.Calendar;

public class CityEvent extends Place {

    public String title;
    public String description;
    public String photoURL;
    public Calendar date;
    public double duration;
    public String address;
    public double latitude;
    public double longitude;

}
