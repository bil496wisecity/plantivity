package com.wisecity.bil496project.api_calls;

import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wisecity.bil496project.AsyncResponse;
import com.wisecity.bil496project.util.DownloadUrl;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.UserInterest;

import java.util.HashSet;
import java.util.List;


// <Params, Progress, Result>
// Params goes to doInBackground
// Result, the type of the result of the background computation.
public class GetNearbyPlaces extends AsyncTask<Object, List<Place>, List<Place>> {
    public AsyncResponse delegate;
    private String url;
    private GoogleMap mMap;
    private List<Place> nearbyPlaces;
    private List<UserInterest> interestedTypes;
    private UserData userData;

    // This method passes jsonString to onPostExecute.
    @Override
    protected List<Place> doInBackground(Object... objects) {
        mMap = (GoogleMap) objects[0];
        url = (String) objects[1];
        userData = UserData.getInstance();
        interestedTypes = userData.getUserInterests();

        DownloadUrl downloadUrl = new DownloadUrl();
        try {
            nearbyPlaces = downloadUrl.getAllNearbyPlaces(url, interestedTypes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        removeDuplicates();
        return nearbyPlaces;
    }


    @Override
    protected void onPostExecute(List<Place> nearbyPlaces) {
        userData.setNearByPlaces(nearbyPlaces);
        delegate.processFinish(userData);
        DisplayNearbyPlaces(nearbyPlaces, mMap);
    }


    public void DisplayNearbyPlaces(List<Place> nearByPlacesList, GoogleMap mMap) {
        for (int i = 0; i < nearByPlacesList.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();

            Place googleNearbyPlace = nearByPlacesList.get(i);
            String nameOfPlace = googleNearbyPlace.getName();
            String vicinity = googleNearbyPlace.getVicinity();

            double lat = Double.parseDouble(googleNearbyPlace.getLatitude());
            double lng = Double.parseDouble(googleNearbyPlace.getLongitude());

            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(nameOfPlace + " : " + vicinity);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        }
    }

    private void removeDuplicates() {

        if (nearbyPlaces == null)
            return;

        HashSet<String> appendedPlaces = new HashSet<>();
        nearbyPlaces.removeIf(e->!appendedPlaces.add(e.getPlaceId()));
    }
}
