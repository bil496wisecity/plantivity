package com.wisecity.bil496project.optimal_route;

import com.wisecity.bil496project.util.LatLngSerializable;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.QuickSort;

import java.util.List;

public class PlantivityScore {

    public static double calculate(Place place) {
        return place.getRating();
    }

    public static void orderByRating(List<Place> nearbyPlaces) {
        QuickSort.sortByRating(nearbyPlaces, 0, nearbyPlaces.size()-1);
    }

    public static void orderByDistance(List<Place> nearbyPlaces, LatLngSerializable latlon) {
        QuickSort.sortByDistance(nearbyPlaces, latlon, 0, nearbyPlaces.size()-1);
    }

}
