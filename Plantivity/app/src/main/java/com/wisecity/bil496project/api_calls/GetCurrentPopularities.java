package com.wisecity.bil496project.api_calls;

import android.os.AsyncTask;
import android.util.Log;

import com.wisecity.bil496project.util.DownloadUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class GetCurrentPopularities extends AsyncTask<Object, String, String> {

    @Override
    protected String doInBackground(Object... objects) {

        String placeName = (String) objects[0];
        String placeVicinity = (String) objects[0];

        String url = generateURL(placeName, placeVicinity);

        String searchData;

        DownloadUrl downloadUrl = new DownloadUrl();

        try {
            searchData = downloadUrl.urlToJsonString(url);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        searchData = searchData.split("/\\*\"\"\\*/")[0];

        int bracketIndex = searchData.lastIndexOf("}");

        if (bracketIndex >= 0)
            searchData = searchData.substring(0, bracketIndex + 1);


        String currentPopularity = "";

        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(searchData);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        try {
            currentPopularity = jsonObject.get("d").toString().substring(4);
            JSONArray jsonArray = new JSONArray(currentPopularity);
            return parseCurrentPopularity(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onPostExecute(String s) {
    }


    private String parseCurrentPopularity(JSONArray jsonArray) {

        String currentPopularity = "";

        try {
            currentPopularity = jsonArray.getJSONArray(0).getJSONArray(1).getJSONArray(0).
                    getJSONArray(14).getJSONArray(84).getJSONArray(7).getString(1);
        } catch (JSONException e) {
            Log.d("Error", "Popularity data not found.");
            return null;
        }

        return currentPopularity;

    }

    public String generateURL(String placeName, String placeAdress) {
        String url = "https://www.google.de/search?";
        url = url + "tbm=map&";
        url = url + "tch=1&";
        url = url + "hl=en&";
        url = url + "q=" + placeName.replace(' ', '+') + "+"
                + placeAdress.replace(' ', '+') + "&";
        url = url + "pb=!4m12!1m3!1d4005.9771522653964!2d-122.42072974863942!3d37.8077459796541!2m3!1f0!2f0!3f0!3m2!1i1125!2i976\"\n" +
                "              \"!4f13.1!7i20!10b1!12m6!2m3!5m1!6e2!20e3!10b1!16b1!19m3!2m2!1i392!2i106!20m61!2m2!1i203!2i100!3m2!2i4!5b1\"\n" +
                "              \"!6m6!1m2!1i86!2i86!1m2!1i408!2i200!7m46!1m3!1e1!2b0!3e3!1m3!1e2!2b1!3e2!1m3!1e2!2b0!3e3!1m3!1e3!2b0!3e3!\"\n" +
                "              \"1m3!1e4!2b0!3e3!1m3!1e8!2b0!3e3!1m3!1e3!2b1!3e2!1m3!1e9!2b1!3e2!1m3!1e10!2b0!3e3!1m3!1e10!2b1!3e2!1m3!1e\"\n" +
                "              \"10!2b0!3e4!2b1!4b1!9b0!22m6!1sa9fVWea_MsX8adX8j8AE%3A1!2zMWk6Mix0OjExODg3LGU6MSxwOmE5ZlZXZWFfTXNYOGFkWDh\"\n" +
                "              \"qOEFFOjE!7e81!12e3!17sa9fVWea_MsX8adX8j8AE%3A564!18e15!24m15!2b1!5m4!2b1!3b1!5b1!6b1!10m1!8e3!17b1!24b1!\"\n" +
                "              \"25b1!26b1!30m1!2b1!36b1!26m3!2m2!1i80!2i92!30m28!1m6!1m2!1i0!2i0!2m2!1i458!2i976!1m6!1m2!1i1075!2i0!2m2!\"\n" +
                "              \"1i1125!2i976!1m6!1m2!1i0!2i0!2m2!1i1125!2i20!1m6!1m2!1i0!2i956!2m2!1i1125!2i976!37m1!1e81!42b1!47m0!49m1\"\n" +
                "              \"!3b1";

        return url;

    }

}

