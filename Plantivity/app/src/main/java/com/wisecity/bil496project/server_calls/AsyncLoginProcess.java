package com.wisecity.bil496project.server_calls;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.Route;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class AsyncLoginProcess extends AsyncTask<Object, User, User> {
    private Context contextOfTheCallerActivity;
    private User onlineUser;
    private String emailOfUser;
    private String passwordOfUser;
    private ArrayList<String> routeIds;
    private ArrayList<String> followers;
    private ArrayList<String> followings;

    @Override
    protected User doInBackground(Object... objects) {
        emailOfUser = (String) objects[0];
        passwordOfUser = (String) objects[1];
        contextOfTheCallerActivity = (Context) objects[2];

        try {
            sendLoginData(emailOfUser, passwordOfUser, contextOfTheCallerActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("BEKLEMIYORUMABIBENOCUYUM ASYNC ICI");
        return onlineUser;
    }


    protected void sendLoginData(String email, String password, Context context) {
        WebServerManager.getAllUsers(context, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println(email + password);
                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                for (User user : listOfAllRegisteredUsers) {
                    if ((user.getEmail().equals(email)) && (user.getPassword().equals(password))) {
                        System.out.println("ICERISI1");
                        initializeOnlineUser(user.getEmail(), user.getPassword(), user.getFollowers(), user.getFollowings());
                        Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show();
                        extractRouteIdsOfOnlineUser(context);

                    } else {
                        //System.out.println(listOfAllRegisteredUsers.size()); // For debug purposes
                        Toast.makeText(context, "No such user found, please try again!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });

    }

    private void initializeOnlineUser(String emailOfOnlineUser, String passwordOfOnlineUser, ArrayList<String> followersOfOnlineUser, ArrayList<String>followingsOfOnlineUser) {
        if(onlineUser == null) {
            onlineUser = new User();
        }
        onlineUser.setEmail(emailOfOnlineUser);
        onlineUser.setPassword(passwordOfOnlineUser);


        for(String follower : followersOfOnlineUser) {
            onlineUser.addFollower(follower);
        }

        for(String following : followingsOfOnlineUser) {
            onlineUser.addFollowing(following);
        }
        System.out.println("ICERISI2");
    }

    private void extractRouteIdsOfOnlineUser(Context context) {
        WebServerManager.getAllRoutes(context, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                System.out.println("ICERISI3");
                ArrayList<Route> listOfAllRoutes = new ArrayList<>(JsonOperations.JsontoAllRoutes(result));

                for (Route route : listOfAllRoutes) {

                    if (onlineUser.getEmail().equals(route.getUserEmail())) {
                        onlineUser.addRouteId(route.getID());
                    }
                }
                for (String route : onlineUser.getRouteIds()) {
                    System.out.println("ASYNCRoute [" + route + "].");
                }
            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }
}
