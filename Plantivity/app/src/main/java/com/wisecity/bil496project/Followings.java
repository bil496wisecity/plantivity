package com.wisecity.bil496project;


import android.content.Intent;
import android.os.Bundle;


import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;
import java.util.Timer;


/**
 * A simple {@link Fragment} subclass.
 */
public class Followings extends Fragment {
    private RecyclerView followingsRecyclerView;
    private FollowingsAdapter followingsRecyclerViewAdapter;
    private RecyclerView.LayoutManager followingsRecyclerViewLayoutManager;

    View view;
    ProgressBar spinner;
    User onlineUser;
    ArrayList<User> followingsList;

    public Followings() {
        // Required empty public constructor
    }

    public Followings(User onlineUser, ArrayList<User> followingsList) {
        this.onlineUser = onlineUser;
        this.followingsList = followingsList;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ArrayList<FollowingsCardItem> followingsCardItems = new ArrayList<>();
        if(followingsList.equals(null) == false) {
            if(followingsList.size() > 0) {
                for(User following : followingsList) {
                    followingsCardItems.add(new FollowingsCardItem(R.drawable.background_profile_routes, following.getEmail(), following.getPassword()));
                }
            }
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_followings, container, false);

        followingsRecyclerView = view.findViewById(R.id.followingsRecyclerView);
        followingsRecyclerView.setHasFixedSize(true);
        followingsRecyclerViewLayoutManager = new LinearLayoutManager(this.getActivity());
        followingsRecyclerViewAdapter = new FollowingsAdapter(followingsCardItems);

        followingsRecyclerView.setLayoutManager(followingsRecyclerViewLayoutManager);
        followingsRecyclerView.setAdapter(followingsRecyclerViewAdapter);

        followingsRecyclerViewAdapter.setOnItemClickListener(new FollowingsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                followingsRecyclerViewAdapter.notifyItemChanged(position);
                UserData tempUserData =  UserData.getInstance();
                User following = new User();
                following.setEmail(followingsCardItems.get(position).getFollowingsCardItemName());
                Intent followingsProfileActivityIntent = new Intent(view.getContext(), FollowingsProfileActivity.class);
                followingsProfileActivityIntent.putExtra("OnlineUser", onlineUser);
                followingsProfileActivityIntent.putExtra("Following", following);
                followingsProfileActivityIntent.putExtra("UserData", tempUserData);
                view.getContext().startActivity(followingsProfileActivityIntent);
            }

            @Override
            public void onDeleteClick(int position) {
                WebServerManager.putFriendUnFollow(view.getContext(), onlineUser.getEmail(), followingsList.get(position).getEmail());
                followingsCardItems.remove(position);
                followingsList.remove(position);
                followingsRecyclerViewAdapter.notifyItemRemoved(position);
            }
        });

        return view;
    }

    protected void updateOnlineUserAndFollowingsList(User updatedOnlineUser, ArrayList<User> updatedFollowingsList) {
        this.onlineUser = updatedOnlineUser;
        this.followingsList = updatedFollowingsList;
    }



}
