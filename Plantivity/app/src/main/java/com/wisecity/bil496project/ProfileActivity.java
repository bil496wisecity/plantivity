package com.wisecity.bil496project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.Route;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {
    private ProgressBar spinner;
    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Toolbar toolbar;

    // a static variable to get a reference of our application context
    public static Context contextOfApplication;
    public static Context getContextOfApplication()
    {
        return contextOfApplication;
    }

    User onlineUser;
    private ArrayList<String> routesNamesList;
    private ArrayList<Route> routesList;
    private ArrayList<String> followersEmailsList;
    private ArrayList<User> followersList;
    private ArrayList<String> followingsEmailsList;
    private ArrayList<User> followingsList;

    private String emailOfUserToBeFollowed;

    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        contextOfApplication = getApplicationContext();
        spinner = findViewById(R.id.spinnerProfile);
        spinner.setVisibility(View.VISIBLE);
        initializeOnlineUserData();

        // User data provided from another activity
        userData = UserData.getInstance();
        if(userData.getUserInterests() != null) {
            userData.getUserInterests();
            Toast fabToast = Toast.makeText(getApplicationContext(), "User Activities Has Been Initialized", Toast.LENGTH_SHORT);
            fabToast.show();
        }

        addItemsAndFunctionalityToDrawer();

        Handler handler = new Handler();
        int delay = 2000; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){

                checkOnlineUserUpdate();
                clearFollowingsList();
                followersEmailsList = onlineUser.getFollowings();
                WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                        for (String followingsEmail : followingsEmailsList) {

                            for (User registeredUser : listOfAllRegisteredUsers) {

                                if (followingsEmail.equals(registeredUser.getEmail())) {
                                    followingsList.add(registeredUser);
                                }
                            }
                        }
                    }
                }, new ServerCallback() {
                    @Override
                    public void onSuccess(String result) {

                    }
                });
//                followings.updateOnlineUserAndFollowingsList(onlineUser, followingsList);
                handler.postDelayed(this, delay);
//                System.out.println("FOLLOWINGS");
//                for (int i = 0; i < onlineUser.getFollowings().size(); i++) {
//                    System.out.println(onlineUser.getFollowings().get(i));
//                }
            }
        }, delay);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void addItemsAndFunctionalityToDrawer() {

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.material_drawer_dark_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("Berk Utku Yenisey").withEmail("buy@gmail.com").withIcon(getResources().getDrawable(R.drawable.background_profile_routes))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_all_activities);
        SecondaryDrawerItem itemMainActivity = new SecondaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_main_Activity);

        PrimaryDrawerItem itemAddFriends = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_follow_users);

        PrimaryDrawerItem itemLogout = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_logout);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        itemMainActivity,
                        new DividerDrawerItem(),
                        itemAddFriends,
                        new DividerDrawerItem(),
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch ((int) drawerItem.getIdentifier()) {
                            case 2:
                                Toast.makeText(getApplicationContext(), "MAIN ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToMainActivity();
                                break;
                            case 3:
                                followUser();
                                Toast.makeText(getApplicationContext(), "FRIEND ADDED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                break;
                            case 4:
                                Toast.makeText(getApplicationContext(), "LOGGED OUT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                logout();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true).withAccountHeader(headerResult).build();
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        mainActivityIntent.putExtra("UserData", userData);
        startActivity(mainActivityIntent);
    }

    private void followUser() {
        switchToFollowUserActivity();
    }

    private void switchToFollowUserActivity() {
        Intent followUserActivityIntent = new Intent(getApplicationContext(), FollowUserActivity.class);
        followUserActivityIntent.putExtra("OnlineUser", onlineUser);
        followUserActivityIntent.putExtra("UserData", userData);
        startActivity(followUserActivityIntent);
    }

    private void initializeOnlineUserData() {
        // Online user provided from the main activity
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");

        routesNamesList = new ArrayList<>(onlineUser.getRouteIds());
        routesList = new ArrayList<>();
        WebServerManager.getAllRoutes(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<Route> listOfAllRoutes = new ArrayList<>(JsonOperations.JsontoAllRoutes(result));
                for (String routeId : routesNamesList) {

                    for (Route route : listOfAllRoutes) {

                        if (routeId.equals(route.getID())) {
                            routesList.add(route);
                        }
                    }
                }
                toolbar = findViewById(R.id.profileToolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().setTitle(getString(R.string.app_name));
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                // Tabbed Activity
                tabLayout = findViewById(R.id.profileTabLayout);
                viewPager = findViewById(R.id.profileViewPager);
                viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                viewPagerAdapter.addFragment(new UserInfo(), "");
                viewPagerAdapter.addFragment(new RouteHistory(onlineUser, routesList), "");

                followersEmailsList = new ArrayList<>(onlineUser.getFollowers());
                followersList = new ArrayList<>();
                WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                        for (String followersEmail : followersEmailsList) {

                            for (User registeredUser : listOfAllRegisteredUsers) {

                                if (followersEmail.equals(registeredUser.getEmail())) {
                                    followersList.add(registeredUser);
                                }
                            }
                        }

                        viewPagerAdapter.addFragment(new Followers(onlineUser, followersList), "");



                        followingsEmailsList = new ArrayList<>(onlineUser.getFollowings());
                        followingsList = new ArrayList<>();
                        WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
                            @Override
                            public void onSuccessResponse(String result) {
                                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                                for (String followingsEmail : followingsEmailsList) {

                                    for (User registeredUser : listOfAllRegisteredUsers) {

                                        if (followingsEmail.equals(registeredUser.getEmail())) {
                                            followingsList.add(registeredUser);
                                        }
                                    }
                                }
                                // viewPagerAdapter


                                Followings followings = new Followings(onlineUser, followingsList);
                                viewPagerAdapter.addFragment(followings, "Followings");

                                viewPager.setAdapter(viewPagerAdapter);
                                tabLayout.setupWithViewPager(viewPager);
                                tabLayout.getTabAt(0).setIcon(R.drawable.ic_person_black_24dp);
                                tabLayout.getTabAt(1).setIcon(R.drawable.ic_map_black_24dp);
                                tabLayout.getTabAt(2).setIcon(R.drawable.ic_people_black_24dp);
                                tabLayout.getTabAt(3).setIcon(R.drawable.ic_people_black_24dp);
                                spinner.setVisibility(View.GONE);
                            }
                        }, new ServerCallback() {
                            @Override
                            public void onSuccess(String result) {

                            }
                        });
                    }
                }, new ServerCallback() {
                    @Override
                    public void onSuccess(String result) {

                    }
                });
            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });

    }

    private void logout() {
        // TO DO Switch To Login Activity and logout
    }

    private void updateOnlineUser(String emailOfOnlineUser, String passwordOfOnlineUser, ArrayList<String> routeIdsOfOnlineUser, ArrayList<String> followersOfOnlineUser, ArrayList<String>followingsOfOnlineUser) {
        onlineUser = new User();
        onlineUser.setEmail(emailOfOnlineUser);
        onlineUser.setPassword(passwordOfOnlineUser);

        for(String routeId : routeIdsOfOnlineUser) {
            onlineUser.addRouteId((routeId));
        }

        for(String follower : followersOfOnlineUser) {
            onlineUser.addFollower(follower);
        }

        for(String following : followingsOfOnlineUser) {
            onlineUser.addFollowing(following);
        }
    }

    private void checkOnlineUserUpdate() {
        WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                for (User user : listOfAllRegisteredUsers) {
                    if ((user.getEmail().equals(onlineUser.getEmail())) && (user.getPassword().equals(onlineUser.getPassword()))) {
                        updateOnlineUser(user.getEmail(), user.getPassword(), user.getRouteIds(), user.getFollowers(), user.getFollowings());

                    } else {
                        //System.out.println(listOfAllRegisteredUsers.size()); // For debug purposes
                    }
                }

            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }

    private void clearFollowingsList() {
        followingsList.clear();
    }

}
