package com.wisecity.bil496project.util;

import java.io.Serializable;

public class UserTimeUnit implements Serializable {

    public enum TimeUnit{
        HOURS,
        MINUTES,
        SECONDS
    }

    public TimeUnit timeUnit;
    public double amount;

    public UserTimeUnit(TimeUnit timeUnit, double amount) {
        this.timeUnit = timeUnit;
        this.amount = amount;
    }
}
