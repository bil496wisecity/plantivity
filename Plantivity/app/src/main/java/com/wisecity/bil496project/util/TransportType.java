package com.wisecity.bil496project.util;

public enum TransportType {
    CAR,
    BUS,
    BICYCLE,
    FOOT
}
