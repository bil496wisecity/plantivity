package com.wisecity.bil496project.api_calls;

import android.content.Context;
import android.os.AsyncTask;

import com.wisecity.bil496project.R;
import com.wisecity.bil496project.util.CityEvent;
import com.wisecity.bil496project.util.DownloadUrl;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.UserData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class GetNearbyPlacesToEvents extends AsyncTask {

    private Context context;
    private UserData userData;
    private List<Place> nearbyPlacesToEvents;

    @Override
    protected Object doInBackground(Object[] objects) {
        context = (Context) objects[0];
        userData = UserData.getInstance();
        nearbyPlacesToEvents = new ArrayList<>();
        List<CityEvent> chosenEvents = userData.getChosenEvents();

        for (CityEvent cityEvent : chosenEvents) {

            String url;
            DownloadUrl downloadUrl = new DownloadUrl();
            List<Place> nearbyPlacesToEvent = new ArrayList<>();

            try {
                double latitude = Double.parseDouble(cityEvent.getLatitude());
                double longitude = Double.parseDouble(cityEvent.getLongitude());
                url = getUrl(latitude, longitude);
                nearbyPlacesToEvent = downloadUrl.getAllNearbyPlaces(url, userData.getUserInterests());
            } catch (Exception e) {
                e.printStackTrace();
            }

            nearbyPlacesToEvents.addAll(nearbyPlacesToEvent);
        }

        addPlacesToUserData();

        return nearbyPlacesToEvents;
    }

    private void addPlacesToUserData() {
        if (nearbyPlacesToEvents == null)
            return;

        userData.getNearByPlaces().addAll(nearbyPlacesToEvents);

        HashSet<String> checkedPlaces = new HashSet<>();
        userData.getNearByPlaces().removeIf(e->!checkedPlaces.add(e.getPlaceId()));
    }

    private String getUrl(double latitude, double longitude) {
        StringBuilder googleURL = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googleURL.append("location=" + latitude + "," + longitude);
        googleURL.append("&radius=" + 1500);
        googleURL.append("&key=" + context.getString(R.string.google_maps_api));

        return googleURL.toString();
    }


}
