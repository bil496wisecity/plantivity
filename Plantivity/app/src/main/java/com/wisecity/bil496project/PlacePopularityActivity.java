package com.wisecity.bil496project;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.WebServerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PlacePopularityActivity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap map;
    ArrayList<Circle> mapCircles = new ArrayList<>();
    Circle circle;
    private Button btnFindPopularitiesOfPlaces;
    private Button btnSpotify;
    private TextView textView;
    private SeekBar seekBar;
    private ProgressBar spinner;

    private User onlineUser;
    private UserData userData;
    private final float DEFAULT_ZOOM = 15;
    int day;
    int seekBarProgress; //hour
    int stop_count;
    int current_count;


    private int getTodayName(){

        /*
        LocalDate date = LocalDate.now();
        DayOfWeek dow = date.getDayOfWeek();
        int dayName = Integer.parseInt(dow.getDisplayName(TextStyle.NARROW_STANDALONE, Locale.ENGLISH));
        return dayName;
        */
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today); // don't forget this if date is arbitrary
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK); // 1 being Sunday
        return dayOfWeek;
    }


    private void fillPopularHours(JSONArray jsonArray, Place place){
        JSONObject jsonObject;
        ArrayList<JSONObject> popularHours = new ArrayList<>();
        boolean isBlank = true;
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = (JSONObject) jsonArray.get(i);

                // This process is needed for checking if data is blank, which will be predicted.
                JSONArray hourlyDataJSONArray = jsonObject.getJSONArray("data");
                for(int j = 0; j < hourlyDataJSONArray.length(); j++){
                    if(hourlyDataJSONArray.getInt(j) != 0)
                        isBlank = false;
                }

                popularHours.add(jsonObject);
                System.out.println(jsonObject);
            }
            place.setIsBlank(isBlank);
            place.setPopularHours(popularHours);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_popularity);
        addItemsAndFunctionalityToDrawer();

        btnSpotify = findViewById(R.id.btnSpotify);
        btnSpotify.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), SpotifyActivity.class);
            intent.putExtra("UserData", userData);
            intent.putExtra("OnlineUser", onlineUser);
            view.getContext().startActivity(intent);});

        // User data provided from main activity
        userData = (UserData) getIntent().getSerializableExtra("UserData");
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");

        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        seekBarProgress = seekBar.getProgress();
        textView = findViewById(R.id.textView);
        textView.setText("Selected Hour: " + seekBarProgress);

        spinner = findViewById(R.id.spinnerPopularity);

        // What is this?
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentPopularityMap);
        mapFragment.getMapAsync(this);

        spinner.setVisibility(View.VISIBLE);
        stop_count = userData.getNearByPlaces().size();
        current_count = 0;
        RequestQueue queue = Volley.newRequestQueue(this);
        for(Place place: userData.getNearByPlaces()){
            String url = "http://192.168.1.27:8000/populartimes/" + place.getReference();
            //System.out.println(url);


            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if (null != response) {
                                try {
                                    fillPopularHours(response, place);
                                    System.out.println(response);
                                    current_count += 1;
                                    if(current_count == stop_count) {
                                        spinner.setVisibility(View.GONE);
                                        WebServerManager.postPlaces(getApplicationContext(), userData.getNearByPlaces());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println(error);
                }
            });
            queue.add(jsonArrayRequest);
        }
        //spinner.setVisibility(View.GONE);
        // This won't work because requests are sent immediately. You need to check time after getting response.

        day = getTodayName();
        final int[] popularity = new int[1];
        popularity[0] = 0;

        btnFindPopularitiesOfPlaces = findViewById(R.id.btnFindPopularitesOfPlaces);
        btnFindPopularitiesOfPlaces.setOnClickListener(v -> {
            for(Circle circle: mapCircles)
                circle.remove(); // clearing map to get rid of collapsing circles.
            mapCircles.clear(); // clearing array.

            for(Place place : userData.getNearByPlaces()) {
                try{
                    System.out.println(place.getPopularHours());
                    popularity[0] = (int) place.getPopularHours().get(day).getJSONArray("data").get(seekBarProgress);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                double lat = Double.parseDouble(place.getLatitude());
                double lng = Double.parseDouble(place.getLongitude());
                double radius = Math.sqrt(popularity[0] *5 + 10);
                Log.d("Radius", "" + radius);

                circle = map.addCircle(new CircleOptions()
                        .center(new LatLng(lat, lng))
                        .radius(radius)
                        .strokeColor(0x0100dfdf)
                        .fillColor(0xaa00dfdf));
                mapCircles.add(circle);
            }
        });
    }

    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int tempSeekBarProgress, boolean fromUser) {
            // updated continuously as the user slides the thumb
            textView.setText("Selected Hour: " + tempSeekBarProgress);
            seekBarProgress = seekBar.getProgress();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // called when the user first touches the SeekBar
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // called after the user finishes moving the SeekBar
        }
    };


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);

        LatLng currentLatLng = new LatLng(userData.getCurrentLocation().latitude, userData.getCurrentLocation().longitude);

        map.addMarker(new MarkerOptions().position(currentLatLng).title("Current Location"));
        //map.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_ZOOM));

        /*
        for(Place place : userData.getNearByPlaces()) {
            double lat = Double.parseDouble(place.getLatitude());
            double lng = Double.parseDouble(place.getLongtitude());
            map.addCircle(new CircleOptions()
                    .center(new LatLng(lat, lng))
                    .radius(5)
                    .strokeColor(Color.RED)
                    .fillColor(0xcc0000ff));
        }
         */

    }

    private void addItemsAndFunctionalityToDrawer() {

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.material_drawer_dark_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("Berk Utku Yenisey").withEmail("buy@gmail.com").withIcon(getResources().getDrawable(R.drawable.background_profile_routes))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_all_activities);
        PrimaryDrawerItem itemProfileActivity = new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_profile_activity);
        PrimaryDrawerItem itemUserChoicesActivity = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_user_choices_activity);
        PrimaryDrawerItem itemDiscoverPlacesActivity = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_discover_places_activity);
        PrimaryDrawerItem itemMainActivity= new PrimaryDrawerItem().withIdentifier(5).withName(R.string.drawer_item_main_Activity);
        PrimaryDrawerItem itemLogout = new PrimaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_logout);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        itemProfileActivity,
                        itemUserChoicesActivity,
                        itemDiscoverPlacesActivity,
                        itemMainActivity,
                        new DividerDrawerItem(),
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        switch ((int) drawerItem.getIdentifier()) {
                            case 2:
                                Toast.makeText(getApplicationContext(), "PROFILE ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToProfileActivity();
                                break;
                            case 3:
                                Toast.makeText(getApplicationContext(), "USER CHOICES ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToUserChoicesActivity();
                                break;
                            case 4:
                                Toast.makeText(getApplicationContext(), "DISCOVER PLACES ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToDiscoverPlacesActivity();
                                break;
                            case 5:
                                Toast.makeText(getApplicationContext(), "MAIN ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToMainActivity();
                                break;
                            case 6:
                                Toast.makeText(getApplicationContext(), "LOGGED OUT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                logout();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true).withAccountHeader(headerResult).build();
    }

    private void switchToProfileActivity() {
        Intent profileActivityIntent = new Intent(getApplicationContext(), ProfileActivity.class);
        profileActivityIntent.putExtra("OnlineUser", onlineUser);
        profileActivityIntent.putExtra("UserData", userData);
        startActivity(profileActivityIntent);
    }

    private void switchToUserChoicesActivity() {
        Intent userChoicesActivityIntent = new Intent(getApplicationContext(), UserChoicesActivity.class);
        userChoicesActivityIntent.putExtra("OnlineUser", onlineUser);
        userChoicesActivityIntent.putExtra("UserData", userData);
        startActivity(userChoicesActivityIntent);
    }

    private void switchToDiscoverPlacesActivity() {
        Intent discoverPlacesActivityIntent = new Intent(getApplicationContext(), DiscoverPlacesActivity.class);
        discoverPlacesActivityIntent.putExtra("OnlineUser", onlineUser);
        discoverPlacesActivityIntent.putExtra("UserData", userData);
        startActivity(discoverPlacesActivityIntent);
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        mainActivityIntent.putExtra("UserData", userData);
        startActivity(mainActivityIntent);
    }

    private void logout() {
        // TO DO Switch To Login Activity and logout
    }
}
