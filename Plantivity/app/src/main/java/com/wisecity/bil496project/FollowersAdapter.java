package com.wisecity.bil496project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.FollowersViewHolder> {

    private ArrayList<FollowersCardItem> followersCardItems;

    private OnItemClickListener followersOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        followersOnItemClickListener = listener;
    }

    public static class FollowersViewHolder extends RecyclerView.ViewHolder{

        public ImageView iVFollowersCardItem;
        public TextView tVFollowersCardItemName;
        public TextView tVFollowersCardItemDescription;
        public ImageView iVDeleteFollowersCardItem;

        public FollowersViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            iVFollowersCardItem = itemView.findViewById(R.id.iVFollowersCardItem);
            tVFollowersCardItemName = itemView.findViewById(R.id.tVFollowersName);
            tVFollowersCardItemDescription = itemView.findViewById(R.id.tVFollowersDescription);
            iVDeleteFollowersCardItem = itemView.findViewById(R.id.iVDeleteFollowersCardItem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            iVDeleteFollowersCardItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public FollowersAdapter(ArrayList<FollowersCardItem> followersCardItems) {
        this.followersCardItems = followersCardItems;
    }

    @NonNull
    @Override
    public FollowersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.followers_card_item, parent, false);
        FollowersViewHolder fVH = new FollowersViewHolder(v, followersOnItemClickListener);
        return  fVH;
    }

    @Override
    public void onBindViewHolder(@NonNull FollowersViewHolder holder, int position) {
        // At first item position is 0
        FollowersCardItem currentFollowersCardItem = followersCardItems.get(position);

        holder.iVFollowersCardItem.setImageResource(currentFollowersCardItem.getFollowersCardItemImageResource());
        holder.tVFollowersCardItemName.setText(currentFollowersCardItem.getFollowersCardItemName());
        holder.tVFollowersCardItemDescription.setText(currentFollowersCardItem.getFollowersCardItemDescription());
    }

    @Override
    public int getItemCount() {
        return followersCardItems.size();
    }
}
