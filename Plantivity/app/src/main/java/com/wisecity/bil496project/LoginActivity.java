package com.wisecity.bil496project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wisecity.bil496project.server_calls.AsyncLoginProcess;
import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.Route;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements VolleyCallback {

    public static final int HTTP_STATUS_CODE_CONTINUE = 100;
    public static final int HTTP_STATUS_CODE_OK = 200;
    private AsyncLoginProcess asyncLoginProcess;
    private User onlineUser;
    private ProgressBar spinner;

    protected static String userName;
    private String password;

    EditText eTUsername;
    EditText eTPassword;
    Button btnLogin;
    TextView tVSignUpHere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        eTUsername = findViewById(R.id.eTUsername);
        eTPassword = findViewById(R.id.eTPassword);
        spinner = findViewById(R.id.spinnerLogin);
        spinner.setVisibility(View.GONE);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinner.setVisibility(View.VISIBLE);
                userName = eTUsername.getText().toString();
                password = eTPassword.getText().toString();
//               //This part was for async task
//                asyncLoginProcess = new AsyncLoginProcess();
//                try {
//                    onlineUser = new User();
//                    onlineUser = asyncLoginProcess.execute(userName, password, getApplicationContext()).get();
//
//                }catch (Exception e) {
//                    e.printStackTrace();
//                }
                sendLoginData();

            }
        });

        tVSignUpHere = findViewById(R.id.tVRegisterHere);
        tVSignUpHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });
    }

    protected void sendLoginData() {
        WebServerManager.getAllUsers(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
                for (User user : listOfAllRegisteredUsers) {
                    if ((user.getEmail().equals(userName)) && (user.getPassword().equals(password))) {
                        initializeOnlineUser(user.getEmail(), user.getPassword(), user.getFollowers(), user.getFollowings());
                        Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                        extractRouteIdsOfOnlineUser();
                    } else {
                        //System.out.println(listOfAllRegisteredUsers.size()); // For debug purposes
                        Toast.makeText(LoginActivity.this, "No such user found, please try again!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });

    }

    private void switchToMainActivity() {
        spinner.setVisibility(View.GONE);
        Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        startActivity(mainActivityIntent);
    }

    private void initializeOnlineUser(String emailOfOnlineUser, String passwordOfOnlineUser, ArrayList<String> followersOfOnlineUser, ArrayList<String>followingsOfOnlineUser) {
        if(onlineUser == null) {
            onlineUser = new User();
        }
        onlineUser.setEmail(emailOfOnlineUser);
        onlineUser.setPassword(passwordOfOnlineUser);


        for(String follower : followersOfOnlineUser) {
            onlineUser.addFollower(follower);
        }

        for(String following : followingsOfOnlineUser) {
            onlineUser.addFollowing(following);
        }
    }

    private void extractRouteIdsOfOnlineUser() {
        WebServerManager.getAllRoutes(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                ArrayList<Route> listOfAllRoutes = new ArrayList<>(JsonOperations.JsontoAllRoutes(result));

                for (Route route : listOfAllRoutes) {

                    if (onlineUser.getEmail().equals(route.getUserEmail())) {
                        onlineUser.addRouteId(route.getID());
                    }
                }
                switchToMainActivity();
            }
        }, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }

    @Override
    public void onSuccessResponse(String result) {

    }

}
