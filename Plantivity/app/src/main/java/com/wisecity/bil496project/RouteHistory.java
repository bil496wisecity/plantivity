package com.wisecity.bil496project;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.wisecity.bil496project.util.LatLngSerializable;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.Route;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class RouteHistory extends Fragment {
    private RecyclerView routeHistoryRecyclerView;
    private RouteHistoryAdapter routeHistoryRecyclerViewAdapter;
    private RecyclerView.LayoutManager routeHistoryRecyclerViewLayoutManager;

    View view;

    User onlineUser;
    User followerOrFollowingUser;
    private boolean isFollowerOrFollowingUser = false; // Initially
    UserData userData;
    ArrayList<Route> routesList;

    private int ROUTESPECCOUNT = 3; // Count of name and rating to be listed in a route history card item.

    public RouteHistory(User onlineUser, ArrayList<Route> routesList) {
        this.onlineUser = onlineUser;
        this.routesList = routesList;
    }

    public RouteHistory(User onlineUser, ArrayList<Route> routesList, boolean isFollowerOrFollowingUser, User followerOrFollowingUser) {
        this.onlineUser = onlineUser;
        this.routesList = routesList;
        this.isFollowerOrFollowingUser = isFollowerOrFollowingUser;
        this.followerOrFollowingUser = followerOrFollowingUser;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Initialization of userData
        userData = UserData.getInstance();

        ArrayList<RouteHistoryCardItem> routeHistoryCardItems = new ArrayList<>();
        if(routesList.size() > 0) {
            ArrayList<String> namesOfPlaces = new ArrayList<>();
            ArrayList<String> ratingsOfPlaces = new ArrayList<>();
            ArrayList<Place> places = new ArrayList<>();
            for(Route route : routesList) {
                for(int placeIndexAtRoute = 0; placeIndexAtRoute < route.getPlaces().size(); placeIndexAtRoute++) {
                    namesOfPlaces.add(route.getPlaces().get(placeIndexAtRoute).getName());
                    ratingsOfPlaces.add(String.valueOf(route.getPlaces().get(placeIndexAtRoute).getRating()));
                }

                if(ROUTESPECCOUNT > namesOfPlaces.size()) {
                    ROUTESPECCOUNT = namesOfPlaces.size();
                }
                else {
                    ROUTESPECCOUNT = 3;
                }

                StringBuilder stringBuilderNames = new StringBuilder();
                for(int i = 0; i < ROUTESPECCOUNT; i++) {
                    stringBuilderNames.append(namesOfPlaces.get(i));
                    if(i != ROUTESPECCOUNT-1) {
                        stringBuilderNames.append(" - ");
                    }
                }
                StringBuilder stringBuilderRatings = new StringBuilder();
                for(int i = 0; i < ROUTESPECCOUNT; i++) {
                    stringBuilderRatings.append(ratingsOfPlaces.get(i));
                    if(i != ROUTESPECCOUNT-1) {
                        stringBuilderRatings.append(" - ");
                    }
                }
                for(int i = 0; i < route.getPlaces().size(); i++) {
                    places.add(route.getPlaces().get(i));
                }
                routeHistoryCardItems.add(new RouteHistoryCardItem(R.drawable.background_profile_routes, stringBuilderNames.toString(), stringBuilderRatings.toString(), places));
                places.clear(); // Clear to get places for every individual route.
                namesOfPlaces.clear();
                ratingsOfPlaces.clear();
            }
        }

        //routeHistoryCardItems.get(0).initializeMockedRouteForCard1(); // Was for debug purposes at the early development stages.

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_route_history, container, false);
        routeHistoryRecyclerView = view.findViewById(R.id.routeHistoryRecyclerView);
        routeHistoryRecyclerView.setHasFixedSize(true);
        routeHistoryRecyclerViewLayoutManager = new LinearLayoutManager(this.getActivity());
        routeHistoryRecyclerViewAdapter = new RouteHistoryAdapter(routeHistoryCardItems);

        routeHistoryRecyclerView.setLayoutManager(routeHistoryRecyclerViewLayoutManager);
        routeHistoryRecyclerView.setAdapter(routeHistoryRecyclerViewAdapter);

        routeHistoryRecyclerViewAdapter.setOnItemClickListener(new RouteHistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
//                routeHistoryCardItems.get(position).setRouteHistoryCardItemName("Clicked");
//                routeHistoryCardItems.get(position).setRouteHistoryCardItemDescription("Clicked");
//                routeHistoryRecyclerViewAdapter.notifyItemChanged(position);
                    userData.setRoute(routeHistoryCardItems.get(position).places);
                    userData.setCurrentLocation(new LatLngSerializable(Double.parseDouble(routeHistoryCardItems.get(position).places.get(0).getLatitude()), Double.parseDouble(routeHistoryCardItems.get(position).places.get(0).getLongitude())));
                    Intent visualizeRouteActivityIntent = new Intent(view.getContext(), VisualizeRouteActivity.class);
                    visualizeRouteActivityIntent.putExtra("OnlineUser", onlineUser);
                    userData.setSentFromProfileActivity(true);
                    visualizeRouteActivityIntent.putExtra("UserData", userData);
                    view.getContext().startActivity(visualizeRouteActivityIntent);
            }

            @Override
            public void onDeleteClick(int position) {
                if(isFollowerOrFollowingUser) {
                    // Do Nothing, no deletion happens.
                }
                else {
                    WebServerManager.deleteRoute(view.getContext(), routesList.get(position).getID());
                    routeHistoryCardItems.remove(position);
                    routesList.remove(position);
                    routeHistoryRecyclerViewAdapter.notifyItemRemoved(position);
                }
            }
        });
        return view;
    }

    private void shareUrl(String routeId) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "https://plantivity/routeHistory/" + routeId);
        startActivity(Intent.createChooser(shareIntent, "Share with"));
    }
}
