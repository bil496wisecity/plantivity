 package com.wisecity.bil496project.optimal_route;

public class ScoreConfigurations {

    public static final double AMUSEMENT_PARK_BASE_SCORE = 3.1;
    public static final double AQUARIUM_PARK_BASE_SCORE = 3.1;
    public static final double BOWLING_ALLEY_BASE_SCORE = 3.1;
    public static final double MOVIE_THEATER_BASE_SCORE = 3.1;

    public static final double AMUSEMENT_PARK_DECREASE_FACTOR = -5;
    public static final double AQUARIUM_PARK_DECREASE_FACTOR = -5;
    public static final double BOWLING_ALLEY_DECREASE_FACTOR = -5;
    public static final double MOVIE_THEATER_DECREASE_FACTOR = -5;

    public static final double BAKERY_BASE_SCORE  = 3.05;
    public static final double MEAL_TAKEAWAY_BASE_SCORE = 3.05;

    public static final double BAKERY_DECREASE_FACTOR  = -3;
    public static final double MEAL_TAKEAWAY_DECREASE_FACTOR = -3;

    public static final double CAFE_BASE_SCORE = 3.05;
    public static final double RESTAURANT_BASE_SCORE = 3.05;

    public static final double CAFE_DECREASE_FACTOR = -3;
    public static final double RESTAURANT_DECREASE_FACTOR = -3;

    public static final double CASINO_BASE_SCORE = 2.95;
    public static final double BAR_BASE_SCORE = 2.95;
    public static final double NIGHT_CLUB_BASE_SCORE = 2.95;

    public static final double CASINO_DECREASE_FACTOR = -7;
    public static final double BAR_DECREASE_FACTOR = -7;
    public static final double NIGHT_CLUB_DECREASE_FACTOR = -7;

    public static final double BEAUTY_SALON_BASE_SCORE = 2.95;
    public static final double HAIR_CARE_BASE_SCORE = 2.95;

    public static final double BEAUTY_SALON_DECREASE_FACTOR = -10;
    public static final double HAIR_CARE_DECREASE_FACTOR = -10;

    public static final double FURNITURE_STORE_BASE_SCORE = 2.95;
    public static final double HOME_GOODS_BASE_SCORE = 2.95;

    public static final double FURNITURE_STORE_DECREASE_FACTOR = -3;
    public static final double HOME_GOODS_DECREASE_FACTOR = -3;

    public static final double CLOTHING_SCORE_BASE_SCORE = 2.95;
    public static final double SHOE_STORE_BASE_SCORE = 2.95;
    public static final double JEWELRY_BASE_SCORE = 2.95;

    public static final double CLOTHING_SCORE_DECREASE_FACTOR = -2;
    public static final double SHOE_STORE_DECREASE_FACTOR = -2;
    public static final double JEWELRY_DECREASE_FACTOR = -2;

    public static final double BOOK_STORE_BASE_SCORE = 2.95;

    public static final double BOOK_STORE_DECREASE_FACTOR = -5;

    public static final double HARDWARE_BASE_SCORE = 2.95;

    public static final double HARDWARE_DECREASE_FACTOR = -5;

    public static final double FLORIST_BASE_SCORE = 2.95;

    public static final double FLORIST_DECREASE_FACTOR = -5;

    public static final double LIQUOR_STORE_BASE_SCORE = 2.9;

    public static final double LIQUOR_STORE_DECREASE_FACTOR = -10;

    public static final double CHURCH_BASE_SCORE = 2.9;
    public static final double MOSQUE_BASE_SCORE = 2.9;
    public static final double SYNAGOGUE_BASE_SCORE = 2.9;

    public static final double CHURCH_DECREASE_FACTOR = -10;
    public static final double MOSQUE_DECREASE_FACTOR = -10;
    public static final double SYNAGOGUE_DECREASE_FACTOR = -10;

    public static final double ART_GALLERY_BASE_SCORE = 3.2;
    public static final double LIBRARY_BASE_SCORE = 3.2;
    public static final double MUSEUM_BASE_SCORE = 3.2;

    public static final double ART_GALLERY_DECREASE_FACTOR = -1;
    public static final double LIBRARY_DECREASE_FACTOR = -1;
    public static final double MUSEUM_DECREASE_FACTOR = -1;

    public static final double CAMPGROUND_BASE_SCORE = 3.15;
    public static final double PARK_BASE_SCORE = 3.15;
    public static final double ZOO_BASE_SCORE = 3.15;

    public static final double CAMPGROUND_DECREASE_FACTOR = -10;
    public static final double PARK_DECREASE_FACTOR = -5;
    public static final double ZOO_DECREASE_FACTOR = -10;

    public static final double SPA_BASE_SCORE = 3;

    public static final double SPA_DECREASE_FACTOR = -10;

    public static final double TOURIST_ATTRACTION_BASE_SCORE = 8;

    public static final double TOURIST_ATTRACTION_DECREASE_FACTOR = -1;

}
