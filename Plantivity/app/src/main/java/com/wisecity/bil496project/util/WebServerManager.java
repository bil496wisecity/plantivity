package com.wisecity.bil496project.util;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.maps.model.TravelMode;
import com.wisecity.bil496project.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebServerManager {

    public static RequestQueue queue;
    public static final String POSTROUTEURL = "http://192.168.1.21:8000/route";
    public static final String POSTUSERURL = "http://192.168.1.21:8000/user";
    public static final String GETROUTEURL = "http://192.168.1.21:8000/route/";
    public static final String GETALLUSERSURL = "http://192.168.1.21:8000/allusers";
    public static final String GETALLROUTESURL = "http://192.168.1.21:8000/allroutes";
    public static final String GETUSERURL = "http://192.168.1.21:8000/user/";
    public static final String PUTFRIENDFOLLOWURL = "http://192.168.1.21:8000/friendfollow/";
    public static final String POST_SPOTIFY_TOKEN_URL = "http://192.168.1.21:8000/spotifyLinking/";
    public static final String PUTROUTERATEURL = "http://192.168.1.21:8000/route/";
    public static final String POST_PLACES_URL = "http://192.168.1.21:8000/places/";
    public static final String DELETEROUTEURL = "http://192.168.1.21:8000/deleteroute/";
    public static final String PUTFRIENDUNFOLLOWURL = "http://192.168.1.21:8000/friendunfollow/";
    public static final String PUTUSERDEVICEIDURL = "http://192.168.1.21:8000/user/";
    public static final String POSTNOTIFICATIONTOFIREBASE = "https://fcm.googleapis.com/fcm/send";

    public static final String firebase_auth_key = "AAAAMhtdaJ8:APA91bHbFGACIplBj8xrELvqoTT8NUKXqxMhXOhZdP3x4OFHF1QxyyKJ3knw-RauqmdQ_g2ABlpSmoDCzJ8CwdZP6wv7E7WGSZNRMhRj52xCWfsvbzyFUd3z57k_XUVLUAHIeHSes9v0";

    public static void postNotification(Context context, User user, Notification notification){
        try {
            queue = Volley.newRequestQueue(context);

            JSONObject notificationJSON = new JSONObject();
            try {
                notificationJSON.put("title", notification.getNotificationTitle());
                notificationJSON.put("body", notification.getNotificationBody());
            } catch (JSONException e) {
                e.printStackTrace();
            }


            JSONObject dataJSON = new JSONObject();
            try {
                dataJSON.put("title", "Title of Your Notification in Title");
                dataJSON.put("body", "Body of Your Notification in Data"    );
                dataJSON.put("key_1", "Value for key_1"    );
                dataJSON.put("key_2", "Value for key_2"    );
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject mainJSON = new JSONObject();
            try {
                mainJSON.put("to", user.getDeviceId());
                mainJSON.put("collapse_key", "type_a");
                mainJSON.put("data", dataJSON);
                mainJSON.put("notification", notificationJSON);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject toJSON = new JSONObject();
            try {
                toJSON.put("token", user.getDeviceId());
            } catch (JSONException e) {
                e.printStackTrace();
            }


            String strJSON = "{" + "\"message\":{" +
                                            "\"token\":" + "\"" + user.getDeviceId() + "\"" +  "," +
                                            "\"notification\":{" +
                                                                      "\"title\":" + "\"" + notification.getNotificationTitle() + "\"" + "," +
                                                                      "\"body\":" +  "\"" + notification.getNotificationBody() + "\""  +
                                            "}" +
                                    "}" +
                             "}";

            final String requestBody = mainJSON.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, POSTNOTIFICATIONTOFIREBASE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        Log.d("AA:","TimeoutError");
                    } else if (error instanceof AuthFailureError) {
                        Log.d("AA:","TimeoutError");
                    } else if (error instanceof ServerError) {
                        Log.d("AA:","TimeoutError");
                    } else if (error instanceof NetworkError) {
                        Log.d("AA:","TimeoutError");
                    } else if (error instanceof ParseError) {
                        Log.d("AA:","TimeoutError");
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String auth = "key="
                            + firebase_auth_key;
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", auth);
                    return headers;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //It will call like this => WebServerManager.postUser(this, user);
    public static void postUser(Context context, User user){
        try {
            queue = Volley.newRequestQueue(context);

            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("email", user.getEmail());
                jsonBody.put("password", user.getPassword());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, POSTUSERURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //It will call like this => WebServerManager.putFriendFollow(this, selfEmail, userEmail);
    public static void putUserDeviceId(Context context, String userEmail, String deviceId){
        try {
            queue = Volley.newRequestQueue(context);
            final String requestBody = "{}";

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, PUTUSERDEVICEIDURL + userEmail + "/" + deviceId, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //It will call like this => WebServerManager.putFriendFollow(this, selfEmail, userEmail);
    public static void deleteRoute(Context context, String pk){
        try {
            queue = Volley.newRequestQueue(context);
            final String requestBody = "{}";

            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, DELETEROUTEURL + pk, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //It will call like this => WebServerManager.postRoute(this, route, userData.getTravelMode().ordinal(), "utku");
    public static void postRoute(Context context, List<Place> route, int travelMode, String userEmail){
        try {
            queue = Volley.newRequestQueue(context);

            JSONObject outerJSONBody = new JSONObject();

            JSONArray JSONarray = new JSONArray();

            for(int i=0;i<route.size();i++){
                JSONObject innerJSONBody = new JSONObject();
                try {
                    innerJSONBody.put("name", route.get(i).getName());
                    innerJSONBody.put("latitude", route.get(i).getLatitude());
                    innerJSONBody.put("longitude", route.get(i).getLongitude());
                    innerJSONBody.put("rating", ""+route.get(i).getRating());
                    innerJSONBody.put("travelMode", ""+travelMode);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONarray.put(innerJSONBody);
            }
            outerJSONBody.put("places", JSONarray);
            outerJSONBody.put("userEmail", userEmail);

            final String requestBody = outerJSONBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, POSTROUTEURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //It will call like this => WebServerManager.putFriendFollow(this, selfEmail, userEmail);
    public static void putFriendFollow(Context context, String selfEmail, String userEmail){
        try {
            queue = Volley.newRequestQueue(context);
            final String requestBody = "{}";

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, PUTFRIENDFOLLOWURL + selfEmail + "/" + userEmail + "/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //It will call like this => WebServerManager.putFriendUnFollow(this, selfEmail, userEmail);
    public static void putFriendUnFollow(Context context, String selfEmail, String userEmail){
        try {
            queue = Volley.newRequestQueue(context);
            final String requestBody = "{}";

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, PUTFRIENDUNFOLLOWURL + selfEmail + "/" + userEmail + "/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //It will call like this => WebServerManager.putRouteRate(this, routeId, rate);
    public static void putRouteRate(Context context, String routeId, double rate){
        try {
            queue = Volley.newRequestQueue(context);
            final String requestBody = "{}";

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, PUTROUTERATEURL + routeId + "/" + ""+rate + "/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Class which calls this method should implement the VolleyCallback
    public static void getUser(Context context, String userEmail, VolleyCallback callback) {

        getResponse(Request.Method.GET, context, GETUSERURL + userEmail, null,
                new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        if(result != null)
                            callback.onSuccessResponse(result);
                    }
                });
    }

    //Class which calls this method should implement the VolleyCallback
    public static void getAllRoutes(Context context, VolleyCallback callback, ServerCallback serverCallback) {

        getResponse(Request.Method.GET, context, GETALLROUTESURL, null,
                new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        if(result != null) {
                            callback.onSuccessResponse(result);
                            serverCallback.onSuccess(result);
                        }
                    }
                });
    }

    //Class which calls this method should implement the VolleyCallback
    public static void getAllUsers(Context context, VolleyCallback callback, ServerCallback serverCallback) {

        getResponse(Request.Method.GET, context, GETALLUSERSURL, null,
                new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        if(result != null) {
                            callback.onSuccessResponse(result);
                            serverCallback.onSuccess(result);
                        }
                    }
                });
    }

    //Class which calls this method should implement the VolleyCallback
    public static void getRoutes(Context context, String userEmail, VolleyCallback callback) {

        getResponse(Request.Method.GET, context, GETROUTEURL + userEmail, null,
                new VolleyCallback() {
                    @Override
                    public void onSuccessResponse(String result) {
                        if(result != null)
                            callback.onSuccessResponse(result);
                    }
                });
    }


    //It will call like this => WebServerManager.postUser(this, spotifyToken, spotifyUsername, userEmail);
    public static void postSpotifyToken(Context context, String spotifyToken, String spotifyUsername, String userEmail){
        try {
            queue = Volley.newRequestQueue(context);

            JSONObject outerJSONBody = new JSONObject();
            outerJSONBody.put("token", spotifyToken);
            outerJSONBody.put("spotify_username", spotifyUsername);
            outerJSONBody.put("email", userEmail);


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, POST_SPOTIFY_TOKEN_URL, outerJSONBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("VOLLEY", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }


                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    JSONObject responseString = new JSONObject();
                    if (response != null) {
                        try {
                            responseString.put("response", String.valueOf(response.statusCode));
                            // can get more details such as response.headers
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            queue.add(jsonObjReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void postPlaces(Context context, List<Place> places){
        queue = Volley.newRequestQueue(context);

        JSONObject payload = new JSONObject();
        JSONArray placesJsonArray = new JSONArray();

        try{
            for(int i = 0; i < places.size(); i++){
                JSONObject placeJson = new JSONObject();
                Place place = places.get(i);
                placeJson.put("id", place.getPlaceId());
                placeJson.put("rating", place.getRating());
                placeJson.put("price_level", place.getPriceLevel());
                placeJson.put("user_ratings_total", place.getUserRatingsTotal());
                placeJson.put("compound_code", place.getCompoundCode());
                placeJson.put("name", place.getName());
                placeJson.put("latitude", place.getLatitude());
                placeJson.put("longitude", place.getLongitude());
                placeJson.put("reference", place.getReference());

                ArrayList<UserInterest.Group> groups = new ArrayList<>(place.getGroups());
                JSONArray groupsJSONArray = new JSONArray();
                for(int j = 0; j < groups.size(); j++){
                    groupsJSONArray.put(groups.get(j).toString());
                }
                placeJson.put("groups", groupsJSONArray);

                // Adding place popularity.
                JSONArray placesPopularityJSONArray = new JSONArray();
                ArrayList<JSONObject> placePopularHours = new ArrayList<>(place.getPopularHours());
                for(int j = 0; j < placePopularHours.size(); j++){
                    placesPopularityJSONArray.put(placePopularHours.get(i));
                }
                placeJson.put("popularity", placesPopularityJSONArray);
                placeJson.put("is_blank", place.getIsBlank());

                placesJsonArray.put(placeJson);
            }

            payload.put("places", placesJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, POST_PLACES_URL, payload, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("VOLLEY", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        });

        queue.add(jsonObjReq);

    }

    //Class which calls this method should implement the VolleyCallback
    public static void getResponse(int method, Context context, String url, JSONObject jsonValue, final VolleyCallback callback) {

        queue = MySingleton.getInstance(context).getRequestQueue();

        StringRequest strreq = new StringRequest(method, url, new Response.Listener < String > () {

            @Override
            public void onResponse(String Response) {
                callback.onSuccessResponse(Response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                e.printStackTrace();
                Toast.makeText(context, e + "error", Toast.LENGTH_LONG).show();
            }
        })
        {

        };
        MySingleton.getInstance(context).addToRequestQueue(strreq);
    }

}
