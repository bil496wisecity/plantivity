package com.wisecity.bil496project.optimal_route;

import android.content.Context;
import android.icu.util.Calendar;
import android.util.Log;

import com.google.maps.model.TravelMode;
import com.wisecity.bil496project.api_calls.GetDistanceMatrix;
import com.wisecity.bil496project.optimal_route.place_graph.Edge;
import com.wisecity.bil496project.optimal_route.place_graph.Node;
import com.wisecity.bil496project.optimal_route.place_graph.PlaceGraph;
import com.wisecity.bil496project.util.CityEvent;
import com.wisecity.bil496project.util.DataParser;
import com.wisecity.bil496project.util.LatLngSerializable;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.Place;
import com.wisecity.bil496project.util.UserTimeUnit;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class OptimalRoute {

    private Context context;
    private DataParser dataParser;
    private PlantivityRanker plantivityRanker;
    private PlaceGraph placeGraph;
    private Node placeGraphRoot;
    private List<Place> nearbyPlaces;
    private List<Place> chosenPlaces;
    private List<CityEvent> nearbyEvents;
    private List<Place> visitedPlaces;
    private TravelMode travelMode;
    private boolean cityEventsIncluded;
    private double timeLimit;
    private double timeLimitBetweenEvents;
    private boolean placesEnded;

    public OptimalRoute(UserData userData, Context context) {
        this.context = context;
        this.dataParser = new DataParser();
        this.placeGraph = new PlaceGraph();
        this.nearbyPlaces = userData.getNearByPlaces();
        this.chosenPlaces = userData.getChosenPlaces();
        this.nearbyEvents = userData.getChosenEvents();
        this.visitedPlaces = new ArrayList<>();
        this.travelMode = userData.getTravelMode();
        this.timeLimit = userData.getMinutes().amount;
        this.placesEnded = false;

        plantivityRanker = PlantivityRanker.getInstance();

        timeLimit = Double.max(timeLimit, 100);

        initializeGraph(userData);
    }

    private void initializeGraph(UserData userData) {

        sortEventsByTime();
        sortNearbyPlacesByScore();

        if (nearbyEvents != null && nearbyEvents.size() > 0) {
            placeGraphRoot = new Node("root", new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 0),
                    0, userData.getCurrentLocation());
            cityEventsIncluded = true;
        } else {
            placeGraphRoot = placeGraph.buildPlaceGraph(userData.getCurrentLocation(), chosenPlaces,
                    this);
            cityEventsIncluded = false;
        }

        placeGraphRoot.setArrivalTime(userData.getTravelDate());

    }

    public void findOptimalRoute() {

        if (cityEventsIncluded) {

            findOptimalRouteOnCityEvents();

        } else {

            findOptimalRouteOnUserSpecifiedPlaces();

            while (timeLimit > 0 && !placesEnded)
                insertExtraPlaceToRoute();

        }

        List<Place> finalRoute = convertGraphToList(placeGraphRoot);
        UserData.getInstance().setRoute(finalRoute);

    }

    private void findOptimalRouteOnUserSpecifiedPlaces() {

        Node head = placeGraphRoot;

        while (true) {

            if (head.edgeList().size() < 1 || timeLimit < 0)
                return;

            Node destination = head.edgeList().get(0).getDestination();

            double timeToTravel = getRealDurationBetweenNodes(head, destination);

            head.edgeList().get(0).setCost(timeToTravel);

            timeLimit -= timeToTravel;

            timeLimit -= destination.getCost().amount;

            if (timeLimit < 0) {
                head.cutEdges();
                timeLimit += timeToTravel;
                timeLimit += destination.getCost().amount;
                return;
            }

            visitedPlaces.add(destination.getPlace());

            head = destination;

        }

    }

    private void findOptimalRouteOnCityEvents() {

        Node head = placeGraphRoot;

        for (int i = 0; i < nearbyEvents.size(); i++) {

            Node startingPoint = head;

            CityEvent nearbyEvent = nearbyEvents.get(i);

            UserTimeUnit nearbyEventCost = new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES,
                    nearbyEvent.duration);

            LatLngSerializable nearbyEventLatLng = new LatLngSerializable(nearbyEvent.getLatitude(),
                    nearbyEvent.getLongitude());

            Node endPoint = new Node(nearbyEvent.title, nearbyEventCost, 5, nearbyEventLatLng,
                    nearbyEvent);

            long millis;

            if (startingPoint == placeGraphRoot)
                millis = nearbyEvent.date.getTimeInMillis() - placeGraphRoot.getArrivalTime().getTime();
            else {
                CityEvent previousEvent = (CityEvent) head.getPlace();
                millis = nearbyEvent.date.getTimeInMillis() - previousEvent.date.getTimeInMillis();
            }

            timeLimitBetweenEvents = TimeUnit.MILLISECONDS.toMinutes(millis);

            fillSpaceBetweenEvents(startingPoint, endPoint);

            head = endPoint;


        }

    }

    private void fillSpaceBetweenEvents(Node startingPoint, Node endPoint) {

        double timeFromStartingToEnd = getRealDurationBetweenNodes(startingPoint, endPoint);

        Edge edgeFromStartingToEnd = new Edge(endPoint, timeFromStartingToEnd);

        startingPoint.addEdge(edgeFromStartingToEnd);

        Node chosenPlacesGraph = placeGraph.buildPlaceGraph(startingPoint.getLocation(),
                chosenPlaces, this);

        List<Place> chosenPlacesSorted = convertGraphToList(chosenPlacesGraph);

        boolean placesEnded = false;

        timeLimitBetweenEvents -= timeFromStartingToEnd;

        while (timeLimitBetweenEvents > 0 && !placesEnded) {

            placesEnded = insertChosenPlaceToEventRoute(startingPoint, chosenPlacesSorted);

        }

        placesEnded = false;

        while (timeLimitBetweenEvents > 0 && !placesEnded) {

            placesEnded = insertPlaceToEventRoute(startingPoint);

        }

    }

    private void insertExtraPlaceToRoute() {

        while (plantivityRanker.bestPlace() != null) {

            Place extraPlace = plantivityRanker.bestPlace();

            if (placeVisited(extraPlace)) {
                plantivityRanker.popBestPlace(false);
                continue;
            } else
                plantivityRanker.popBestPlace(true);

            Node nearestToExtraPlace = placeGraph.getMinCostNodeToPlace(extraPlace, placeGraphRoot);

            Node adjacentToNearest;

            if (nearestToExtraPlace.isTailNode())
                adjacentToNearest = nearestToExtraPlace.getOrigin(placeGraphRoot);
            else
                adjacentToNearest = nearestToExtraPlace.edgeList().get(0).getDestination();

            // case that there is only one node in graph.

            boolean singleNodeGraph = false;

            // todo : check here later for any bug
            if (adjacentToNearest == nearestToExtraPlace || adjacentToNearest == null)
                singleNodeGraph = true;

            double travelTimePrediction;

            if (singleNodeGraph)
                travelTimePrediction = predictDuration(nearestToExtraPlace, extraPlace);
            else
                travelTimePrediction = predictDuration(nearestToExtraPlace, adjacentToNearest, extraPlace);

            if (timeLimit - travelTimePrediction > 0) {

                String placeId = extraPlace.getPlaceId();

                // todo: change default rating later
                double rating = extraPlace.getRating();

                UserTimeUnit cost = new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 0);

                cost.amount = getTimeSpentOnLocation(extraPlace);

                if (cost.amount > timeLimit - travelTimePrediction)
                    continue;

                double lat = Double.parseDouble(extraPlace.getLatitude());
                double lng = Double.parseDouble(extraPlace.getLongitude());

                Node extraPlaceNode = new Node(placeId, cost, rating, new LatLngSerializable(lat, lng),
                        extraPlace);

                double timeInRoadNearestToExtra = getRealDurationBetweenNodes(nearestToExtraPlace, extraPlaceNode);

                double totalTimeFromNearestToExtra = timeInRoadNearestToExtra + cost.amount;

                if (timeLimit - totalTimeFromNearestToExtra > 0) {

                    if (nearestToExtraPlace.isTailNode()) {

                        nearestToExtraPlace.addEdge(new Edge(extraPlaceNode, timeInRoadNearestToExtra));

                        timeLimit -= totalTimeFromNearestToExtra;

                        visitedPlaces.add(extraPlace);

                        return;

                    }

                    // if nearest is not tail node then calculate duration between extra node and
                    // next node to nearest.
                    else {

                        double timeInRoadExtraToAdjacent = getRealDurationBetweenNodes(extraPlaceNode, adjacentToNearest);

                        if (timeLimit - totalTimeFromNearestToExtra - timeInRoadExtraToAdjacent > 0) {

                            Edge fromNearestToAdjacent = nearestToExtraPlace.edgeList().get(0);

                            timeLimit += fromNearestToAdjacent.getCost();

                            nearestToExtraPlace.edgeList().get(0).setDestination(extraPlaceNode);

                            Edge edgeToAdjacentNode = new Edge(adjacentToNearest, timeInRoadExtraToAdjacent);

                            extraPlaceNode.addEdge(edgeToAdjacentNode);

                            timeLimit = timeLimit - totalTimeFromNearestToExtra - timeInRoadExtraToAdjacent;

                            visitedPlaces.add(extraPlace);

                            return;

                        } else continue;

                    }

                }

            }

        }

        placesEnded = true;

    }


    private boolean insertPlaceToEventRoute(Node startingPoint) {

        while (plantivityRanker.bestPlace() != null) {

            Place extraPlace = plantivityRanker.bestPlace();

            if (placeVisited(extraPlace)) {
                plantivityRanker.popBestPlace(false);
                continue;
            } else
                plantivityRanker.popBestPlace(true);

            Node minCostNodeToExtraPlace = placeGraph.getMinCostNodeToEvent(extraPlace,
                    startingPoint);

            // TODO: do same for chosen places path
            double timeOnRoadMinCostToAdjacent = minCostNodeToExtraPlace.edgeList().get(0).getCost();

            Node adjacentToNearest = minCostNodeToExtraPlace.edgeList().get(0).getDestination();

            double travelTimePrediction = predictDuration(minCostNodeToExtraPlace,
                    adjacentToNearest, extraPlace);

            if (timeLimitBetweenEvents - travelTimePrediction + timeOnRoadMinCostToAdjacent > 0) {

                String placeId = extraPlace.getPlaceId();

                // todo: change default rating later
                double rating = extraPlace.getRating();

                // TODO: change default cost
                UserTimeUnit cost = new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 0);

                cost.amount = getTimeSpentOnLocation(extraPlace);

                if (cost.amount > timeLimitBetweenEvents - travelTimePrediction)
                    continue;

                double lat = Double.parseDouble(extraPlace.getLatitude());
                double lng = Double.parseDouble(extraPlace.getLongitude());

                Node extraPlaceNode = new Node(placeId, cost, rating, new LatLngSerializable(lat, lng),
                        extraPlace);

                double timeInRoadMinCostToExtra = getRealDurationBetweenNodes(minCostNodeToExtraPlace,
                        extraPlaceNode);

                double totalTimeFromNearestToExtra = timeInRoadMinCostToExtra + cost.amount;

                if (timeLimitBetweenEvents - totalTimeFromNearestToExtra + timeOnRoadMinCostToAdjacent > 0) {

                    double timeInRoadExtraToAdjacent = getRealDurationBetweenNodes(extraPlaceNode, adjacentToNearest);

                    if (timeLimitBetweenEvents - totalTimeFromNearestToExtra - timeInRoadExtraToAdjacent
                            + timeOnRoadMinCostToAdjacent > 0) {

                        timeLimitBetweenEvents += timeOnRoadMinCostToAdjacent;

                        minCostNodeToExtraPlace.edgeList().get(0).setDestination(extraPlaceNode);

                        Edge edgeToAdjacentNode = new Edge(adjacentToNearest, timeInRoadExtraToAdjacent);

                        extraPlaceNode.addEdge(edgeToAdjacentNode);

                        timeLimitBetweenEvents = timeLimitBetweenEvents -
                                totalTimeFromNearestToExtra - timeInRoadExtraToAdjacent;

                        visitedPlaces.add(extraPlace);

                        return false;

                    }

                }

            }

        }

        return true;

    }

    private boolean insertChosenPlaceToEventRoute(Node startingPoint, List<Place> chosenPlaces) {

        for (Place extraPlace : chosenPlaces) {

            if (placeVisited(extraPlace))
                continue;

            Node minCostNodeToExtraPlace = placeGraph.getMinCostNodeToEvent(extraPlace,
                    startingPoint);

            // TODO: do same for chosen places path
            double timeOnRoadMinCostToAdjacent = minCostNodeToExtraPlace.edgeList().get(0).getCost();

            Node adjacentToNearest = minCostNodeToExtraPlace.edgeList().get(0).getDestination();

            double travelTimePrediction = predictDuration(minCostNodeToExtraPlace,
                    adjacentToNearest, extraPlace);

            if (timeLimitBetweenEvents - travelTimePrediction + timeOnRoadMinCostToAdjacent > 0) {

                String placeId = extraPlace.getPlaceId();

                // todo: change default rating later
                double rating = extraPlace.getRating();

                // TODO: change default cost
                UserTimeUnit cost = new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, 0);

                cost.amount = getTimeSpentOnLocation(extraPlace);

                if (cost.amount > timeLimitBetweenEvents - travelTimePrediction)
                    continue;

                double lat = Double.parseDouble(extraPlace.getLatitude());
                double lng = Double.parseDouble(extraPlace.getLongitude());

                Node extraPlaceNode = new Node(placeId, cost, rating, new LatLngSerializable(lat, lng),
                        extraPlace);

                double timeInRoadMinCostToExtra = getRealDurationBetweenNodes(minCostNodeToExtraPlace,
                        extraPlaceNode);

                double totalTimeFromNearestToExtra = timeInRoadMinCostToExtra + cost.amount;

                if (timeLimitBetweenEvents - totalTimeFromNearestToExtra + timeOnRoadMinCostToAdjacent > 0) {

                    double timeInRoadExtraToAdjacent = getRealDurationBetweenNodes(extraPlaceNode, adjacentToNearest);

                    if (timeLimitBetweenEvents - totalTimeFromNearestToExtra - timeInRoadExtraToAdjacent
                            + timeOnRoadMinCostToAdjacent > 0) {

                        timeLimitBetweenEvents += timeOnRoadMinCostToAdjacent;

                        minCostNodeToExtraPlace.edgeList().get(0).setDestination(extraPlaceNode);

                        Edge edgeToAdjacentNode = new Edge(adjacentToNearest, timeInRoadExtraToAdjacent);

                        extraPlaceNode.addEdge(edgeToAdjacentNode);

                        timeLimitBetweenEvents = timeLimitBetweenEvents -
                                totalTimeFromNearestToExtra - timeInRoadExtraToAdjacent;

                        visitedPlaces.add(extraPlace);

                        return false;

                    }

                }

            }

        }

        return true;

    }

    private double predictDuration(Node nearestToExtraPlace, Node adjacentToNearest, Place extraPlace) {

        double durationPrediction = 0;

        double nearestPlaceLat = nearestToExtraPlace.getLocation().latitude;
        double nearestPlaceLng = nearestToExtraPlace.getLocation().longitude;

        double adjacentLat = adjacentToNearest.getLocation().latitude;
        double adjacentLng = adjacentToNearest.getLocation().longitude;

        double sampleDistance = DistanceBetweenTwoGeoCoords.calculateDistance(nearestPlaceLat,
                adjacentLat, nearestPlaceLng, adjacentLng, 0, 0);

        double sampleDuration;

        if (nearestToExtraPlace.edgeList().size() > 0)
            sampleDuration = nearestToExtraPlace.edgeList().get(0).getCost();
        else
            sampleDuration = adjacentToNearest.edgeList().get(0).getCost();

        double extraPlaceLat = Double.parseDouble(extraPlace.getLatitude());
        double extraPlaceLng = Double.parseDouble(extraPlace.getLongitude());

        LatLngSerializable latLngExtra = new LatLngSerializable(extraPlaceLat, extraPlaceLng);
        LatLngSerializable latLngNearest = new LatLngSerializable(nearestPlaceLat, nearestPlaceLng);
        LatLngSerializable latLngAdjacent = new LatLngSerializable(adjacentLat, adjacentLng);

        //give sampleDuration and sampleDistance from nearby locations and then get prediction
        // between two locations

        durationPrediction += DistanceBetweenTwoGeoCoords.predictDurationBetweenTwoLocations(
                sampleDuration, sampleDistance, latLngExtra, latLngNearest);

        // check if extra place becomes the last node, if it's last node no need to append extra
        // duration time from extra node to adjacent node of nearest

        if (nearestToExtraPlace.edgeList().size() > 0)
            durationPrediction += DistanceBetweenTwoGeoCoords.predictDurationBetweenTwoLocations(
                    sampleDuration, sampleDistance, latLngExtra, latLngAdjacent);

        return durationPrediction;

    }

    // if there is no adjacent node to nearest call this

    private double predictDuration(Node nearestToExtraPlace, Place extraPlace) {

        double durationPrediction = 0;

        double nearestPlaceLat = nearestToExtraPlace.getLocation().latitude;
        double nearestPlaceLng = nearestToExtraPlace.getLocation().longitude;

        double sampleDistance = 882.6700893464007;
        double sampleDuration = 5;

        double extraPlaceLat = Double.parseDouble(extraPlace.getLatitude());
        double extraPlaceLng = Double.parseDouble(extraPlace.getLongitude());

        LatLngSerializable latLngExtra = new LatLngSerializable(extraPlaceLat, extraPlaceLng);
        LatLngSerializable latLngNearest = new LatLngSerializable(nearestPlaceLat, nearestPlaceLng);

        //give sampleDuration and sampleDistance from nearby locations and then get prediction
        // between two locations

        durationPrediction += DistanceBetweenTwoGeoCoords.predictDurationBetweenTwoLocations(
                sampleDuration, sampleDistance, latLngExtra, latLngNearest);


        return durationPrediction;

    }

    public double getTimeSpentOnLocation(Place place) {

        double timeSpent = 30; // default time spent value
        String type = place.getType();

        int stringIdentifier = context.getResources().getIdentifier(type, "string",
                context.getPackageName());

        if (stringIdentifier > 0) {
            String timeSpentRaw = context.getString(stringIdentifier);
            timeSpent = Double.parseDouble(timeSpentRaw);
        }

        return timeSpent;

    }

    private double getRealDurationBetweenNodes(Node from, Node to) {

        long departureTime = placeGraphRoot.getArrivalTime().getTime() / 1000;

        Object[] transferData = new Object[5];

        transferData[0] = context;
        transferData[1] = from.getLocation();
        transferData[2] = to.getLocation();
        transferData[3] = departureTime;
        transferData[4] = travelMode;

        String distanceMatrix;

        try {
            distanceMatrix = (String) new GetDistanceMatrix().execute(transferData).get();
        } catch (ExecutionException e) {
            Log.d("Error", "Cannot get distance matrix.");
            return -1;
        } catch (InterruptedException e) {
            Log.d("Error", "Cannot get distance matrix.");
            return -1;
        }
        Log.d("API CALL", "API CALL");

        return dataParser.parseTimeFromDistanceMatrix(distanceMatrix);

    }

    private Date addMinutes(Date date, double minutes) {
        return new Date(date.getTime() + (long) minutes * 60000);
    }

    private void sortNearbyPlacesByScore() {

        for (int i = 0; i < nearbyPlaces.size(); i++)

            for (int j = i + 1; j < nearbyPlaces.size(); j++) {

                double score_i = PlantivityScore.calculate(nearbyPlaces.get(i));

                double score_j = PlantivityScore.calculate(nearbyPlaces.get(j));

                if (score_j > score_i) {

                    Place temp = nearbyPlaces.get(i);

                    nearbyPlaces.set(i, nearbyPlaces.get(j));

                    nearbyPlaces.set(j, temp);
                }

            }
    }

    private void sortEventsByTime() {

        for (int i = 0; i < nearbyEvents.size(); i++)

            for (int j = i + 1; j < nearbyEvents.size(); j++) {

                Calendar date_i = nearbyEvents.get(i).date;

                Calendar date_j = nearbyEvents.get(j).date;

                if (date_j.getTimeInMillis() < date_i.getTimeInMillis()) {

                    CityEvent temp = nearbyEvents.get(i);

                    nearbyEvents.set(i, nearbyEvents.get(j));

                    nearbyEvents.set(j, temp);
                }

            }

    }

    private boolean placeVisited(Place place) {

        boolean visited = false;

        for (Place visitedPlace : visitedPlaces)
            if (place.equals(visitedPlace))
                visited = true;

        return visited;

    }


    private List<Place> convertGraphToList(Node placeGraphRoot) {

        ArrayList<Place> route = new ArrayList<>();

        Node head = placeGraphRoot;
        Node destination;

        while (true) {

            if (head.edgeList().size() < 1)
                return route;

            Edge edgeToNextNode = head.edgeList().get(0);

            destination = edgeToNextNode.getDestination();

            route.add(destination.getPlace());

            if (head.getArrivalTime() != null) {
                Date arrivalTime = addMinutes(head.getArrivalTime(), head.getCost().amount + edgeToNextNode.getCost());
                destination.setArrivalTime(arrivalTime);
                destination.getPlace().setArrivalTime(arrivalTime);
            }

            head = destination;

        }

    }

}
