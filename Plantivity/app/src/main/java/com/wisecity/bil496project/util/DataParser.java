package com.wisecity.bil496project.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DataParser {

    public double parseTimeFromDistanceMatrix(String json) {

        String text = "";

        try {
            JSONObject durationJSON = new JSONObject(json)
                    .getJSONArray("rows")
                    .getJSONObject(0)
                    .getJSONArray("elements")
                    .getJSONObject(0)
                    .getJSONObject("duration");

            text = durationJSON.getString("text");

        } catch (JSONException e) {
            Log.d("Error", "Cannot get duration from distance matrix json data");
            e.printStackTrace();
            return -1;
        }

        String regex = "([0-9]*[.])?[0-9]+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text.replace(',', '.'));

        boolean format_min = text.contains("minute") || text.contains("minutes")
                || text.contains("min");

        boolean format_hour = text.contains("hour") || text.contains("hours")
                || text.contains("hr");

        Float[] times = new Float[2];
        int index = 0;

        while (matcher.find()) {
            times[index++] = Float.parseFloat(matcher.group());
        }

        if (format_min && format_hour)
            return times[0] * 60 + times[1];

        if (format_hour)
            return times[0] * 60;

        if (format_min)
            return times[0];

        return 0;
    }
}