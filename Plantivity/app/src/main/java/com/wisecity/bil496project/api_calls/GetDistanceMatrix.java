package com.wisecity.bil496project.api_calls;

import android.content.Context;
import android.os.AsyncTask;

import com.google.maps.model.TravelMode;
import com.wisecity.bil496project.R;
import com.wisecity.bil496project.util.DownloadUrl;
import com.wisecity.bil496project.util.LatLngSerializable;

import java.io.IOException;
import java.math.BigDecimal;

public class GetDistanceMatrix extends AsyncTask {

    private Context context;
    private LatLngSerializable origin, destination;
    private BigDecimal departureTime;
    private TravelMode travelMode;
    private String distanceMatrix;

    @Override
    protected Object doInBackground(Object[] objects) {

        context = (Context) objects[0];
        origin = (LatLngSerializable) objects[1];
        destination = (LatLngSerializable) objects[2];

        StringBuilder url = new StringBuilder();
        url.append("https://maps.googleapis.com/maps/api/distancematrix/json?");
        url.append("origins=" + origin.latitude + "," + origin.longitude);
        url.append("&destinations=" + destination.latitude + "," + destination.longitude);
        url.append("&key=" + context.getString(R.string.google_maps_api));

        if (objects.length >= 4) {
            departureTime = new BigDecimal((long) objects[3]);
            url.append("&departure_time=" + departureTime.toPlainString());
        }

        if (objects.length == 5) {
            travelMode = (TravelMode) objects[4];
            url.append("&mode=" + travelMode.toString());
        }

        System.out.println(url.toString());

        DownloadUrl downloadUrl = new DownloadUrl();
        try {
            distanceMatrix = downloadUrl.urlToJsonString(url.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return distanceMatrix;

    }

}
