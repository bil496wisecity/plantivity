package com.wisecity.bil496project.util.DiscoverPlaces;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.wisecity.bil496project.DiscoverEventsActivity;
import com.wisecity.bil496project.DiscoverPlacesActivity;
import com.wisecity.bil496project.R;
import com.wisecity.bil496project.util.Place;

@Layout(R.layout.swipe_card_view)
public class SwipeCard {

    @View(R.id.placeImageView)
    private ImageView placeImageView;

    @View(R.id.placeName)
    private TextView placeName;

    @View(R.id.placeAddress)
    private TextView placeAddress;

    private Place mPlace;
    private String mPhotoURL;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    private boolean mEventsMode;

    public SwipeCard(Context context, Place place, String photoURL, SwipePlaceHolderView swipeView,
                     boolean eventsMode) {
        mContext = context;
        mPlace = place;
        mPhotoURL = photoURL;
        mSwipeView = swipeView;
        mEventsMode = eventsMode;
    }

    @Resolve
    private void onResolved() {
        Glide.with(mContext).load(mPhotoURL).into(placeImageView);
        String nameRatingDistance = mPlace.getName() + ",   " + mPlace.getRating() + ",    ";
        int km = 0;
        double meter = 0;
        if(mPlace.getDistance() > 1000){
            km = (int)Math.floor(mPlace.getDistance()) / 1000;
            nameRatingDistance += km + " km";
        }
        else{
            meter = round(mPlace.getDistance(), 2);
            nameRatingDistance += meter + " meter";
        }
        placeName.setText(nameRatingDistance);
        placeAddress.setText(mPlace.getVicinity());
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
        //mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn(){
        //TODO when the user add a place to chosen placen delete it from nearbyplaces
        if (mEventsMode)
            DiscoverEventsActivity.fillChosenEvents(mPlace);
        else
            DiscoverPlacesActivity.fillChosenPlaces(mPlace);
    }

    @SwipeInState
    private void onSwipeInState(){
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState(){
        Log.d("EVENT", "onSwipeOutState");
    }

}
