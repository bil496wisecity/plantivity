package com.wisecity.bil496project;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.Message;
import com.wisecity.bil496project.util.MessageListAdapter;
import com.wisecity.bil496project.util.MyAutoCompleteAdapter;
import com.wisecity.bil496project.util.MyAutoCompleteTextView;
import com.wisecity.bil496project.util.Notification;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class ChatActivity extends AppCompatActivity implements VolleyCallback {
    private static final String TAG = "ChatActivity";
    private static final String SECRET_KEY = "#hkb4_bphck&ass3!f52%7&t$u@*$eo)#&c5$hb+*6g=in#l)g";
    private static String roomName = "Istanbul";
    private static int selectedChannelId = 0;
    private String channelAddress = "ws://192.168.1.21:8000/ws/chat/";

    private OkHttpClient client;
    private MessageListAdapter mMessageAdapter;
    private Button sendBtn;
    private static EditText sendMessage;
    public static User onlineUser;
    ListView messageList;
    private WebSocket ws;
    private String lastMessage = "";
    private List<User> allUsers = new ArrayList<>();
    public static String prevText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        //mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        mMessageAdapter = new MessageListAdapter(this);
        //mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        messageList = (ListView) findViewById(R.id.messages_view);
        messageList.setAdapter(mMessageAdapter);
        client = new OkHttpClient();
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");
        sendBtn = (Button) findViewById(R.id.button_chatbox_send);
        WebServerManager.getAllUsers(this, this::onSuccessResponse, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
        startConnection();
        sendMessage = (EditText) findViewById(R.id.editText);

        sendMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                prevText = s.toString();
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });



        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sendMessage.getText().toString().length() == 0) return;
                String mentionedEmail = "";
                boolean isRealUser = false;
                User user = null;
                String inputText = sendMessage.getText().toString();
                String[] splitInputText = inputText.split("\\s+");
                for(int i = 0; i<splitInputText.length;i++){
                    if(splitInputText[i].charAt(0) == '@'){
                        mentionedEmail = splitInputText[i].substring(1);
                    }
                }
                for(int i = 0; i< allUsers.size();i++){
                    if(allUsers.get(i).getEmail().equals(mentionedEmail)){
                        isRealUser = true;
                        user = allUsers.get(i);
                    }
                }


                JSONObject obj = new JSONObject();
                try {
                    obj.put("message" , sendMessage.getText().toString() + SECRET_KEY + onlineUser.getEmail());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ws.send(obj.toString());
                sendMessage.getText().clear();

                if(isRealUser){
                    Notification notification = new Notification();
                    notification.setNotificationTitle("Plantivity");
                    notification.setNotificationBody(onlineUser.getEmail() + " is mentioning about you!");
                    WebServerManager.postNotification(ChatActivity.this, user, notification);
                }
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolbar.setTitle("Channels");
        //toolbar.setTitleTextColor(Color.RED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.channel_select_menu, menu);
        menu.getItem(selectedChannelId).setChecked(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //item.setChecked(true);
        Intent intent = getIntent();
        switch (item.getItemId()) {
            case R.id.oIstanbul:
                if(roomName.equals("Istanbul")) return true;
                lastMessage = "";
                roomName = "Istanbul";
                selectedChannelId = 0;
                intent = getIntent();
                finish();
                startActivity(intent);
                //startConnection();
                //Toast.makeText(this, "oIstanbul selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.oAnkara:
                if(roomName.equals("Ankara")) return true;
                lastMessage = "";
                roomName = "Ankara";
                selectedChannelId = 1;
                intent = getIntent();
                finish();
                startActivity(intent);
                //startConnection();
                //Toast.makeText(this, "oAnkara selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.oAntalya:
                if(roomName.equals("Antalya")) return true;
                lastMessage = "";
                roomName = "Antalya";
                selectedChannelId = 2;
                intent = getIntent();
                finish();
                startActivity(intent);
                //startConnection();
                return  true;
            /*
            case R.id.subitem1:
                Toast.makeText(this, "Sub Item 1 selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.subitem2:
                Toast.makeText(this, "Sub Item 2 selected", Toast.LENGTH_SHORT).show();
                return true;

             */
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    String randomSecrecStringGenerator() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

    void startConnection() {
        channelAddress += roomName + "/";
        Request request = new Request.Builder().url(channelAddress).build();
        ChatWebSocketListener listener = new ChatWebSocketListener();
        ws = client.newWebSocket(request, listener);
        client.dispatcher().executorService().shutdown();
    }

    private void output(final String txt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //eTUsername.setText(eTUsername.getText().toString() + "\n\n" + txt);
            }
        });
    }

    private final class ChatWebSocketListener extends WebSocketListener{
        private static final int NORMAL_CLOSURE_STATUS = 1000;
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            Log.d(TAG, "onOpen() is called.");
            JSONObject obj = new JSONObject();
            String welcomeMessage = "Welcome to " + roomName + " channel!";
            try {
                obj.put("message" , welcomeMessage + SECRET_KEY);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            webSocket.send(obj.toString());
        }


        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Log.d(TAG, "Receiving :" + text);

            try {
                JSONObject obj = new JSONObject(text);
                String strMessage = obj.isNull("message")?"":obj.optString("message");
                String []strMessages = strMessage.split(Pattern.quote(SECRET_KEY));
                if(strMessages[0].equals(lastMessage)) return;
                Message message = new Message();
                message.setMessage(strMessages[0]);
                if(strMessages.length == 1) message.setUserEmail("Administrator");
                else message.setUserEmail(strMessages[1]);

                if(message.getUserEmail().equals(onlineUser.getEmail()))
                    message.setMessageBelongToCurrentUser(true);
                else
                    message.setMessageBelongToCurrentUser(false);

                String hour = LocalDateTime.now(ZoneId.of("Turkey")).getHour()< 10? "0" + LocalDateTime.now(ZoneId.of("Turkey")).getHour() : ""+ LocalDateTime.now(ZoneId.of("Turkey")).getHour();
                String minutes = LocalDateTime.now(ZoneId.of("Turkey")).getMinute() < 10? "0" + LocalDateTime.now(ZoneId.of("Turkey")).getMinute() : ""+ LocalDateTime.now(ZoneId.of("Turkey")).getMinute();
                String seconds = LocalDateTime.now(ZoneId.of("Turkey")).getSecond() < 10? "0" + LocalDateTime.now(ZoneId.of("Turkey")).getSecond() : ""+ LocalDateTime.now(ZoneId.of("Turkey")).getSecond();
                String time = hour + ":" + minutes + ":" + seconds;
                message.setCreatedAt(time);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mMessageAdapter.add(message);
                        messageList.setSelection(messageList.getCount() - 1);
                    }
                });
                //obj.put("message" , message);
                lastMessage = strMessages[0];
                //webSocket.send(obj.toString());


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            Log.d(TAG, "onMessage() for ByteString is called.");
            output("Receiving bytes : " + bytes.hex());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            Log.d(TAG, "onClosing() is called.");
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            output("Closing : " + code + " / " + reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            Log.d(TAG, "onFailure() is called.");
            output("Error : " + t.getMessage());
        }
    }

    @Override
    public void onSuccessResponse(String result) {
        allUsers = JsonOperations.JsontoUsers(result);
        ArrayList<String> userEmails = new ArrayList<>();
        for(int i = 0; i< allUsers.size(); i++){
            userEmails.add(allUsers.get(i).getEmail());
        }
        MyAutoCompleteAdapter<String> adapter = new MyAutoCompleteAdapter<>(ChatActivity.this,
                userEmails);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.editText);
        textView.setThreshold(1);
        textView.setAdapter(adapter);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {

                textView.showDropDown();

            }
        });

        textView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public static void changeText(CharSequence s){

        String[] prevTexts = prevText.split("\\s+");
        String mainText = "";
        for(int i = 0; i< prevTexts.length ; i++){
            if(prevTexts[i].charAt(0) == '@'){
                mainText += "<font color='#0000ff'>" + "@" + s.toString() + "</font>" + " " ;
                continue;
            }
            mainText += prevTexts[i] + " ";
        }
        sendMessage.setText(Html.fromHtml(mainText));
        //sendMessage.setTextColor(Color.BLUE);
    }
}
