package com.wisecity.bil496project.util;

public interface VolleyCallback {
    void onSuccessResponse(String result);
}
