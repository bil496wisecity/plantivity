package com.wisecity.bil496project.util;

import java.io.Serializable;

public class LatLngSerializable implements Serializable {

    public double latitude;
    public double longitude;

    public LatLngSerializable(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LatLngSerializable(String latitude, String longitude) {
        this.latitude = Double.parseDouble(latitude);
        this.longitude = Double.parseDouble(longitude);
    }

}
