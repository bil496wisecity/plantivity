package com.wisecity.bil496project.api_calls;

import android.os.AsyncTask;
import android.util.Log;

import com.wisecity.bil496project.util.DownloadUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GetPopularHours extends AsyncTask<Object, String, String> {

    private String searchData, url;

    @Override
    protected String doInBackground(Object... objects) {

        url = (String) objects[0];

        JSONObject jsonObject = null;

        DownloadUrl downloadUrl = new DownloadUrl();
        try {
            searchData = downloadUrl.urlToJsonString(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        searchData = searchData.split("/\\*\"\"\\*/")[0];

        int bracketIndex = searchData.lastIndexOf("}");

        if (bracketIndex >= 0)
            searchData = searchData.substring(0, bracketIndex + 1);

        try {
            jsonObject = new JSONObject(searchData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String popularTimesData = jsonObject.get("d").toString().substring(4);
            JSONArray jsonArray = new JSONArray(popularTimesData);
            return getTimeTypicallySpent(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }


    @Override
    protected void onPostExecute(String s) {
    }


    private String getTimeTypicallySpent(JSONArray jsonArray) {

        String timeTypicallySpent = "";

        try {
            timeTypicallySpent = jsonArray.getJSONArray(0).getJSONArray(1).getJSONArray(0).getJSONArray(14).getJSONArray(117).getString(0);
        } catch (JSONException e) {
            Log.d("Error", "Time spent data not found.");
            return null;
        }

        String regex="([0-9]*[.])?[0-9]+";

        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(timeTypicallySpent.replace(',', '.'));

        boolean format_min = timeTypicallySpent.contains("min");
        boolean format_hour = timeTypicallySpent.contains("hour") || timeTypicallySpent.contains("hr");

        Float[] times = new Float[2];
        int index = 0;


        while(matcher.find())
        {
            times[index++] = Float.parseFloat(matcher.group());
        }

        if(format_min && format_hour)
            return times[0] + "," + times[1] * 60;

        if(format_hour)
            if(times[1] != null)
                return times[0] * 60 + "," + times[1] * 60;
            else
                return times[0] * 60 + "," + times[0] * 60;


        if(format_min)
            if(times[1] != null)
                return times[0] + "," + times[1];
            else
                return times[0] + "," + times[0];


        return null;

    }

}

