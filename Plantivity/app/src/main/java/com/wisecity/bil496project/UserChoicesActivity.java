package com.wisecity.bil496project;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.maps.model.TravelMode;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.UserData;
import com.wisecity.bil496project.util.UserInterest;
import com.wisecity.bil496project.util.UserTimeUnit;

public class UserChoicesActivity extends AppCompatActivity {

    private int userHourToSpent = 0; // As initially
    private int userMinuteToSpent = 0; // As initially

    private Toolbar toolbar;

    FloatingActionButton fab;
    Button btnChooseCost;
    TextView tVCost;
    Button btnChangeDate;
    Button btnChangeTime;
    Button btnChooseTravelMode;
    TextView tVTravelMode;
    EditText eTUserActivityTimeHour;
    EditText eTUserActivityTimeMinute;
    String[] listOfCosts;
    String[] listOfMods;
    User onlineUser;
    UserData userData;
    Calendar calendar;

    private int entertainmentInterest, cultureInterest, foodInterest, fashionInterest,
            natureInterest, floristInterest, personalCareInterest, religiousPlacesInterest,
            liquorStoreInterest, bookStoreInterest, hardwareStoreInterest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_choices);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        addItemsAndFunctionalityToDrawer();

        // Online User provided from login activity via main activity
        onlineUser = (User) getIntent().getSerializableExtra("OnlineUser");

        // User data provided from main activity
        userData = UserData.getInstance();

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {

            initializeUserHourAndMinuteSpent();
            userData.setMinutes(new UserTimeUnit(UserTimeUnit.TimeUnit.MINUTES, (userMinuteToSpent + (userHourToSpent * 60))));
            //initializeInterestedTypesOfUserData();
            Toast fabToast = Toast.makeText(getApplicationContext(), "User Activities Has Been Initialized", Toast.LENGTH_SHORT);
            fabToast.show();
            switchToMainActivity();
        });

        calendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        };

        btnChangeDate = findViewById(R.id.btnChangeDate);

        btnChangeDate.setOnClickListener(v -> new DatePickerDialog(this, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show());

        TimePickerDialog.OnTimeSetListener time = (view, hour, minute) -> {
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
        };

        btnChangeTime = findViewById(R.id.btnChangeHour);

        btnChangeTime.setOnClickListener(v -> new TimePickerDialog(this, time,
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show());


        final SeekBar entertainmentSeekbar = findViewById(R.id.entertainment_seekbar);

        entertainmentSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                entertainmentInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar cultureSeekbar = findViewById(R.id.culture_seekbar);

        cultureSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                cultureInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar foodSeekbar = findViewById(R.id.food_seekbar);

        foodSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                foodInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar fashionSeekbar = findViewById(R.id.fashion_seekbar);

        fashionSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                fashionInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar natureSeekbar = findViewById(R.id.nature_seekbar);

        natureSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                natureInterest = (int) (0.13 * progress);
            }


        });

        final SeekBar floristSeekbar = findViewById(R.id.florist_seekbar);

        floristSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                floristInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar personalCareSeekbar = findViewById(R.id.personal_care_seekbar);

        personalCareSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                personalCareInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar religiousPlacesSeekbar = findViewById(R.id.religious_places_seekbar);

        religiousPlacesSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                religiousPlacesInterest = (int) (0.13 * progress);
            }
        });

        final SeekBar bookStoreSeekbar = findViewById(R.id.book_store_seekbar);

        bookStoreSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                bookStoreInterest = (int) (0.13 * progress);
            }
        });

        eTUserActivityTimeHour = findViewById(R.id.eTUserActivityTimeHour);
        eTUserActivityTimeMinute = findViewById(R.id.eTUserActivityTimeMinute);

        btnChooseTravelMode = findViewById(R.id.btnChooseTransportation);
        tVTravelMode = findViewById(R.id.tVTransportation);

        btnChooseTravelMode.setOnClickListener(v -> {
            listOfMods = new String[]{"driving", "walking", "bicycling", "transit"};
            AlertDialog.Builder costAlertDialogBuilder = new AlertDialog.Builder(UserChoicesActivity.this);
            costAlertDialogBuilder.setTitle("Choose Travel Mode");
            costAlertDialogBuilder.setIcon(R.drawable.cost_list_icon);
            costAlertDialogBuilder.setSingleChoiceItems(listOfMods, -1, (dialog, which) -> {
                tVTravelMode.setText(listOfMods[which]);
                dialog.dismiss();
            });
            costAlertDialogBuilder.setNegativeButton("Cancel", (dialog, which) -> {
                // DO NOTHING
            });
            AlertDialog costAlertDialog = costAlertDialogBuilder.create();
            costAlertDialog.show();
        });
    }

    private void switchToMainActivity() {
        Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        mainActivityIntent.putExtra("OnlineUser", onlineUser);
        userData.setTravelDate(calendar.getTime());
        userData.getUserInterests().clear();
        userData.getUserInterests().add(new UserInterest(UserInterest.Type.tourist_attraction, 0));

        if (entertainmentInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.amusement_park, entertainmentInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.aquarium, entertainmentInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.bowling_alley, entertainmentInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.movie_theater, entertainmentInterest));
        }

        if (cultureInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.art_gallery, cultureInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.library, cultureInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.museum, cultureInterest));
        }

        if (foodInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.bakery, foodInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.meal_takeaway, foodInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.cafe, foodInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.restaurant, foodInterest));
        }

        if (fashionInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.clothing_store, fashionInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.shoe_store, fashionInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.jewelry_store, fashionInterest));
        }

        if (natureInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.campground, natureInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.park, natureInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.zoo, natureInterest));
        }

        if (floristInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.florist, floristInterest));
        }

        if (personalCareInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.beauty_salon, personalCareInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.hair_care, personalCareInterest));
        }

        if (bookStoreInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.book_store, bookStoreInterest));
        }

        if (liquorStoreInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.liquor_store, liquorStoreInterest));
        }

        if (religiousPlacesInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.mosque, religiousPlacesInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.synagogue, religiousPlacesInterest));
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.church, religiousPlacesInterest));
        }

        if (hardwareStoreInterest > 0) {
            userData.getUserInterests().add(new UserInterest(UserInterest.Type.hardware_store, hardwareStoreInterest));
        }

        if (tVTravelMode.equals("walking"))
            userData.setTravelMode(TravelMode.WALKING);
        else if (tVTravelMode.equals("bicycling"))
            userData.setTravelMode(TravelMode.BICYCLING);
        else if (tVTravelMode.equals("transit"))
            userData.setTravelMode(TravelMode.TRANSIT);
        else
            userData.setTravelMode(TravelMode.DRIVING);


        startActivity(mainActivityIntent);
    }

    private void initializeUserHourAndMinuteSpent() {
        if ((eTUserActivityTimeHour.getText().toString()).equals("") == false) {
            userHourToSpent = Integer.parseInt(eTUserActivityTimeHour.getText().toString());
        }
        if ((eTUserActivityTimeMinute.getText().toString().equals("")) == false) {
            userMinuteToSpent = Integer.parseInt(eTUserActivityTimeMinute.getText().toString());
        }
    }

    private void addItemsAndFunctionalityToDrawer() {

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.material_drawer_dark_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("Berk Utku Yenisey").withEmail("buy@gmail.com").withIcon(getResources().getDrawable(R.drawable.background_profile_routes))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_all_activities);
        PrimaryDrawerItem itemProfileActivity = new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_profile_activity);
        PrimaryDrawerItem itemMainActivity = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.drawer_item_main_Activity);
        PrimaryDrawerItem itemDiscoverPlacesActivity = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.drawer_item_discover_places_activity);
        PrimaryDrawerItem itemPlacePopularityActivity = new PrimaryDrawerItem().withIdentifier(5).withName(R.string.drawer_item_place_populartiy_activity);

        PrimaryDrawerItem itemLogout = new PrimaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_logout);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        itemProfileActivity,
                        itemMainActivity,
                        itemDiscoverPlacesActivity,
                        itemPlacePopularityActivity,

                        new DividerDrawerItem(),
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        switch ((int) drawerItem.getIdentifier()) {
                            case 2:
                                Toast.makeText(getApplicationContext(), "PROFILE ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToProfileActivity();
                                break;
                            case 3:
                                Toast.makeText(getApplicationContext(), "MAIN ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToMainActivity();
                                break;
                            case 4:
                                Toast.makeText(getApplicationContext(), "DISCOVER PLACES ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToDiscoverPlacesActivity();
                                break;
                            case 5:
                                Toast.makeText(getApplicationContext(), "PLACE POPULARITY ACTIVITY", Toast.LENGTH_SHORT).show();
                                switchToPlacePopularityActivity();
                                break;
                            case 6:
                                Toast.makeText(getApplicationContext(), "LOGGED OUT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                                logout();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true).withAccountHeader(headerResult).build();
    }

    private void switchToProfileActivity() {
        Intent profileActivityIntent = new Intent(getApplicationContext(), ProfileActivity.class);
        profileActivityIntent.putExtra("OnlineUser", onlineUser);
        profileActivityIntent.putExtra("UserData", userData);
        startActivity(profileActivityIntent);
    }

    private void switchToDiscoverPlacesActivity() {
        Intent discoverPlacesActivityIntent = new Intent(getApplicationContext(), DiscoverPlacesActivity.class);
        discoverPlacesActivityIntent.putExtra("UserData", userData);
        discoverPlacesActivityIntent.putExtra("OnlineUser", onlineUser);
        startActivity(discoverPlacesActivityIntent);
    }

    private void switchToPlacePopularityActivity() {
        Intent placePopularityActivityIntent = new Intent(getApplicationContext(), PlacePopularityActivity.class);
        placePopularityActivityIntent.putExtra("UserData", userData);
        placePopularityActivityIntent.putExtra("OnlineUser", onlineUser);
        startActivity(placePopularityActivityIntent);
    }

    private void logout() {
        // TO DO Switch To Login Activity and logout
    }

}


