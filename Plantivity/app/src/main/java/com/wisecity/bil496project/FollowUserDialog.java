package com.wisecity.bil496project;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.wisecity.bil496project.util.JsonOperations;
import com.wisecity.bil496project.util.ServerCallback;
import com.wisecity.bil496project.util.User;
import com.wisecity.bil496project.util.VolleyCallback;
import com.wisecity.bil496project.util.WebServerManager;

import java.util.ArrayList;

public class FollowUserDialog extends AppCompatDialogFragment implements VolleyCallback {

    private EditText eTFollowUserEmail;
    User onlineUser;
    Context contextOfParent;

    public FollowUserDialog(User onlineUser, Context contextOfParent) {
        this.onlineUser = onlineUser;
        this.contextOfParent = contextOfParent;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_follow_user, null);

        eTFollowUserEmail = view.findViewById(R.id.eTFollowUserEmail);

        builder.setView(view).setTitle("Follow Users").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // DISMISSES DIALOG do nothing.
            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(eTFollowUserEmail.getText().toString().equals("") == false) {
                    // TO DO Check if friend existed, if it does do nothing.
                    // TO DO Check the username in all of the members to the Plantivity, if exists add friend.
                    followUser();
                }
                else if(eTFollowUserEmail.getText().toString().equals("") == true) {
                    Toast.makeText(getActivity(), "Error, Please Enter A Username", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return builder.create();
    }

    private void followUser() {
        WebServerManager.getAllUsers(getContext(), this::onSuccessResponse, new ServerCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }

    @Override
    public void onSuccessResponse(String result) {
        ArrayList<User> listOfAllRegisteredUsers = new ArrayList<>(JsonOperations.JsontoUsers(result));
        for(User registeredUser : listOfAllRegisteredUsers) {
            if(eTFollowUserEmail.getText().toString().equals(registeredUser.getEmail())) {
                WebServerManager.putFriendFollow(contextOfParent, onlineUser.getEmail(), eTFollowUserEmail.getText().toString());
            }
        }
    }

}
