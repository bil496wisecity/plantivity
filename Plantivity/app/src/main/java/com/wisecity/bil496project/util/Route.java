package com.wisecity.bil496project.util;

import java.io.Serializable;
import java.util.ArrayList;

public class Route implements Serializable {

    private ArrayList<Place> places;
    private String userEmail;
    private String ID;
    private double usersRate;

    public Route(){
        this.ID = "";
        this.userEmail = "";
        this.usersRate = 1.0;
        this.places = new ArrayList<>();
    }

    public void setUserEmail(String userEmail){ this.userEmail = userEmail;}
    public void addPlace(Place place){ this.places.add(place);}
    public void setID(String Id){ this.ID = Id;}
    public void setUsersRate(double usersRate){ this.usersRate = usersRate;}


    public String getUserEmail(){ return this.userEmail;}
    public String getID(){ return this.ID;}
    public ArrayList<Place> getPlaces(){ return this.places;}
    public double getUsersRate(){ return this.usersRate;}
}
